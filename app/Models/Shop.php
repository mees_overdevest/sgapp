<?php

namespace App\Models;

use App\Helpers\FavouriteHelper;
use App\Helpers\LogHelper;
use Illuminate\Database\Eloquent\Model;

class Shop extends Model
{
    protected $table = "shops";

    protected $fillable = [
        'title',
        'description',
        'since',
        'city',
        'address'
    ];

    public function users()
    {
        return $this->hasMany('App\Models\Account\User','shop_user','shop_id','user_id');
    }

    public function products()
    {
        return $this->belongsToMany('App\Models\Product','shop_product','shop_id','product_id');
    }

    public function services()
    {
        return $this->belongsToMany('App\Models\Service','shop_service','shop_id','service_id');
    }

    public function areas()
    {
        return $this->belongsToMany('App\Models\Area','area_shop','shop_id','area_id');
    }

    public function owner()
    {
        return $this->belongsToMany('App\Models\Account\User','shop_owner', 'shop_id','user_id');
//        return $this->belongsTo('App\Models\Account\User','user_id', 'id');
    }

    public function pictures(){
        return $this->belongsToMany(Picture::class,'picture_shop','shop_id', 'picture_id');
    }

    public function categories()
    {
        return $this->belongsToMany(shopCategory::class,'shopCategory_shop','shop_id', 'shopCategory_id');
    }

    public function isFavourite(){
        return FavouriteHelper::isFavourite('shop', $this);
    }

    public function hasCategory($id){
        return $this->categories->contains($id);
    }

    public function getLog(){
        return LogHelper::getObjectLog($this->id, 'shop');
    }

    public function hasOwner(){
        if($this->owner != null){
            return true;
        } else {
            return false;
        }
    }

    public function getOwner(){
//        dd("agdds");
        return $this->owner->first();
    }

    public function getImageText($kind){
        switch ($kind){
            case 'detail':
                return 'bij de winkel ' .  $this->title;
                break;
            case 'edit':
                return 'van de winkel ' . $this->title;
                break;
            case 'addSuccess':
                return 'U heeft succesvol foto\'s toegevoegd aan de winkel: '. $this->title;
                break;
            case 'editSuccess':
                return "De afbeeldingen van de winkel: ". $this->title." zijn succesvol aangepast!";
                break;
            case 'deleteSuccess':
                return "Een afbeelding van de winkel: ". $this->title." is succesvol verwijderd!";
                break;
        }
    }
}
