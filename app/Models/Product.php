<?php

namespace App\Models;

use App\Helpers\FavouriteHelper;
use App\Helpers\LogHelper;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = "products";

    protected $fillable = [
        'title',
        'description'
    ];

    public function pictures()
    {
        return $this->belongsToMany(Picture::class,'picture_product','product_id', 'picture_id');
    }

    public function categories()
    {
//        return $this->belongsToMany(PCategory::class,'PCategory_product','PCategory_id', 'product_id');
        return $this->belongsToMany(PCategory::class,'PCategory_product','product_id', 'PCategory_id');
    }

    public function brand()
    {
        return $this->belongsToMany(Brand::class,'brand_product', 'product_id', 'brand_id');
    }

    public function shops()
    {
        return $this->belongsToMany(Shop::class,'shop_product','product_id','shop_id');
    }

    public function isFavourite(){
        return FavouriteHelper::isFavourite('product', $this);
    }

    public function getBrand()
    {
        return $this->brand()->first();
    }

    public function hasCategory($id){
        return $this->categories->contains($id);
    }

    public function getLog(){
        return LogHelper::getObjectLog($this->id, 'product');
    }

    public function getImageText($kind){
        switch ($kind){
            case 'detail':
                return 'bij het product ' .  $this->title;
                break;
            case 'edit':
                return 'van het product ' . $this->title;
                break;
            case 'addSuccess':
                return 'U heeft succesvol foto\'s toegevoegd aan het product: '. $this->title;
                break;
            case 'editSuccess':
                return "De afbeeldingen van het product: ". $this->title." zijn succesvol aangepast!";
                break;
            case 'deleteSuccess':
                return "Een afbeelding van het product: ". $this->title." is succesvol verwijderd!";
                break;
        }
    }
}
