<?php

namespace App\Models;

use App\Helpers\FavouriteHelper;
use App\Helpers\LogHelper;
use Illuminate\Database\Eloquent\Model;

class Area extends Model
{
    protected $table = "areas";

    protected $fillable = [
        'title',
        'city',
        'address',
        'description'
    ];

    public function shops()
    {
        return $this->belongsToMany('App\Models\Shop','area_shop','area_id', 'shop_id');
    }

    public function pictures(){
        return $this->belongsToMany(Picture::class,'picture_area','area_id', 'picture_id');
    }

    public function coordinates(){
        return $this->belongsToMany('App\Models\Coordinate', 'area_coordinates','area_id','coordinate_id')
            ->withPivot('order');
    }

    public function isFavourite(){
        return FavouriteHelper::isFavourite('brand', $this);
    }

    public function getLog(){
        return LogHelper::getObjectLog($this->id, 'area');
    }

    public function getImageText($kind){
        switch ($kind){
            case 'detail':
                return 'bij het winkelgebied ' .  $this->title . ' in ' . $this->city;
                break;
            case 'edit':
                return 'van het winkelgebied ' . $this->title . ' in ' . $this->city;
                break;
            case 'addSuccess':
                return 'U heeft succesvol foto\'s toegevoegd aan het winkelgebied: '. $this->title;
                break;
            case 'editSuccess':
                return "De afbeeldingen van het winkelgebied: ". $this->title." zijn succesvol aangepast!";
                break;
            case 'deleteSuccess':
                return "Een afbeelding van het winkelgebied: ". $this->title." is succesvol verwijderd!";
                break;
        }
    }
}
