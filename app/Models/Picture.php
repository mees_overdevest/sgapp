<?php

namespace App\Models;

use App\Helpers\LogHelper;
use Illuminate\Database\Eloquent\Model;

class Picture extends Model
{
    protected $table = "pictures";

    protected $fillable = [
        'image_name',
        'type',
        'image_extension',
        'description'
    ];

    public function product()
    {
        return $this->belongsTo('App\Models\Product', 'picture_product', 'picture_id','product_id');
    }

    public function area()
    {
        return $this->belongsTo('App\Models\Area', 'picture_area', 'picture_id','area_id');
    }

    public function service()
    {
        return $this->belongsTo('App\Models\Service', 'picture_service', 'picture_id','service_id');
    }

    public function shop()
    {
        return $this->belongsTo('App\Models\Shop', 'picture_shop', 'picture_id','shop_id');
    }

    public function brand()
    {
        return $this->belongsToMany('App\Models\Brand', 'picture_brand', 'picture_id','brand_id');
    }

    public function getLog(){
        return LogHelper::getObjectLog($this->id, 'shop');
    }
}
