<?php

namespace App\Models\Account;
//use App\Models\HasRoles;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
//    use HasRoles;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','language_id','code','active','gender','facebook_id','firstName','lastName'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    // Functions
    public function fullname()
    {
        if($this->firstName == null){
                return $this->name;
        }
        return $this->firstName . " ". $this->lastName;
    }

    /**
     * An user belongs to many roles
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function roles()
    {
        return $this->belongsToMany('App\Models\Role','role_user','user_id','role_id');
    }

    public function getRoles()
    {
        return $this->roles()->get();
    }

    public function shops(){
        return $this->belongsToMany('App\Models\Shop','shop_owner', 'user_id', 'shop_id');
    }

    public function getOwnedShop(){
        return $this->shops()->get();
    }

    public function assignRole($role){
        return $this->roles()->save(
            Role::whereName($role)->firstOrFail()
        );
    }

    public function hasRole($roles)
    {
        $this->have_role = $this->getUserRole();
        // Check if the user is a root account
//        if($this->have_role->name == 'Root') {
//            return true;
//        }
        if(is_array($roles)){
            foreach($roles as $need_role){
                if($this->checkIfUserHasRole($need_role)) {
                    return true;
                }
            }
        } else{
            if($this->checkIfUserHasRole($roles)) {
                return true;
            }
        }
        return false;
    }
    private function getUserRole()
    {
        return $this->roles()->getResults();
    }
    private function checkIfUserHasRole($need_role)
    {
        foreach($this->have_role as $role)
        {
            if($need_role == $role->name){
                return true;
            }
        }
        return false;
//        return (strtolower($need_role)==strtolower($this->have_role->label)) ? true : false;
    }

}
