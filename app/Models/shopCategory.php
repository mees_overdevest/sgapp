<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class shopCategory extends Model
{
    protected $table = "shopCategories";

    protected $fillable = [
        'title',
        'description',
        'parent_id'
    ];

    public function parent()
    {
        return $this->belongsTo('App\Models\shopCategory', 'parent_id');
    }

    public function children()
    {
        return $this->hasMany('App\Models\shopCategory', 'parent_id');
    }

    public function shops()
    {
        return $this->belongsToMany('App\Models\Shop', 'shopCategory_shop', 'shopCategory_id', 'shop_id');
    }
}
