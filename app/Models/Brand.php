<?php

namespace App\Models;

use App\Helpers\FavouriteHelper;
use App\Helpers\LogHelper;
use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    protected $table = "brands";

    protected $fillable = [
        'title',
        'description'
    ];

    public function products()
    {
        return $this->hasMany('App\Models\Product','brand_product','brand_id','product_id');
    }

    public function pictures(){
//        return $this->belongsToMany('App\Models\Picture','picture_brand', 'picture_id', 'brand_id');
        return $this->belongsToMany(Picture::class,'picture_brand','brand_id', 'picture_id');
    }

    public function isFavourite(){
        return FavouriteHelper::isFavourite('brand', $this);
    }

    public function getLog(){
        return LogHelper::getObjectLog($this->id, 'brand');
    }

    public function getImageText($kind){
        switch ($kind){
            case 'detail':
                return 'bij het merk ' .  $this->title;
                break;
            case 'edit':
                return 'van het merk ' . $this->title;
                break;
            case 'addSuccess':
                return 'U heeft succesvol foto\'s toegevoegd aan het merk: '. $this->title;
            break;
            case 'editSuccess':
                return "De afbeeldingen van het merk: ". $this->title." zijn succesvol aangepast!";
                break;
            case 'deleteSuccess':
                return "Een afbeelding van het merk: ". $this->title." is succesvol verwijderd!";
                break;
        }
    }
}
