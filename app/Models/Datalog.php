<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Datalog extends Model
{
    protected $table = "datalog";

    protected $fillable = [
        "type", "type_id", "user_id"
    ];

    public function user()
    {
        return $this->hasOne('App\Models\Account\User','id','user_id');
    }
}
