<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PCategory extends Model
{
    protected $table = "PCategories";

    protected $fillable = [
        'title',
        'description',
        'parent_id'
    ];

    public function parent()
    {
        return $this->belongsTo('App\Models\PCategory', 'parent_id');
    }

    public function children()
    {
        return $this->hasMany('App\Models\PCategory', 'parent_id');
    }

    public function products()
    {
        return $this->belongsToMany(Product::class, 'PCategory_product','PCategory_id', 'product_id');
    }

    public function services()
    {
        return $this->belongsToMany('App\Models\Service', 'PCategory_service', 'PCategory_id', 'service_id');
    }
}
