<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Favourite extends Model
{
    protected $table = "favourites";

    protected $fillable = [
        "type", "object_id", "user_id", "notifications"
    ];

    public function user()
    {
        return $this->hasOne('App\Models\Account\User','id','user_id');
    }

    public function object()
    {

        $className = 'App\\Models\\' . ucfirst($this->type);
        $object = $className::findOrFail($this->object_id);

        return $object;
    }
}
