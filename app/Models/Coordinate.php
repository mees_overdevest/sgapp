<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Coordinate extends Model
{
    protected $table = 'coordinates';

    protected $fillable = [
        'lat', 'lng'
    ];

    public function area(){
        return $this->belongsToMany('App\Models\Area','area_coordinates','coordinate_id','area_id')
            ->withPivot('where');
    }
}
