<?php

namespace App\Models;

use App\Helpers\FavouriteHelper;
use App\Helpers\LogHelper;
use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $table = "services";

    protected $fillable = [
        'title',
        'description'
    ];

    public function pictures()
    {
        return $this->belongsToMany(Picture::class,'picture_service','service_id', 'picture_id');
    }

    public function categories()
    {
        return $this->belongsToMany(PCategory::class,'PCategory_service','service_id', 'PCategory_id');
    }

    public function shops()
    {
        return $this->belongsToMany(Shop::class,'shop_service','service_id','shop_id');
    }

    public function isFavourite(){
        return FavouriteHelper::isFavourite('service', $this);
    }

    public function hasCategory($id){
        return $this->categories->contains($id);
    }

    public function getLog(){
        return LogHelper::getObjectLog($this->id, 'service');
    }

    public function getImageText($kind){
        switch ($kind){
            case 'detail':
                return 'bij de service ' .  $this->title;
                break;
            case 'edit':
                return 'van de service ' . $this->title;
                break;
            case 'addSuccess':
                return 'U heeft succesvol foto\'s toegevoegd aan de service: '. $this->title;
                break;
            case 'editSuccess':
                return "De afbeeldingen van de service: ". $this->title." zijn succesvol aangepast!";
                break;
            case 'deleteSuccess':
                return "Een afbeelding van de service: ". $this->title." is succesvol verwijderd!";
                break;
        }
    }
}
