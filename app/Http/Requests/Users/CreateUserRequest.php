<?php

namespace App\Http\Requests\Users;

use App\Http\Requests\Request;

class CreateUserRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if(auth()->guest())
        {
            return true;
        }
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email'         	=> 'required|email|unique:users,email',
            'password'      	=> 'required|min:6|confirmed',
            'firstName'     	=> 'required|min:2|max:200',
            'lastName'      	=> 'required|min:2|max:200',
        ];
    }
}
