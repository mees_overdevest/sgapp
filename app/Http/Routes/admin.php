<?php
/**
 * Created by PhpStorm.
 * User: mees
 * Date: 14-07-16
 * Time: 15:41
 */

Route::group(['prefix' => 'admin', 'namespace'=>'Admin', 'middleware' => 'roles', 'roles' => 'admin'], function () {

    # Users
    Route::resource('users', 'UsersController');
    Route::get('users/reset-password/{user_id}', ['as'=>'admin.users.resetpassword', 'uses'=>'UsersController@adminRequestChangePassword']);

    # Areas
    Route::resource('areas', 'AreaController');
    Route::get('areas/getCoordinates/{id}', ['as'=>'admin.areas.getCoordinates', 'uses'=>'AjaxController@getAreaCoordinates']);
    Route::get('areas/addPhotos/{area}', ['as'=>'admin.areas.addPhotos', 'uses'=>'AreaController@addPhotos']);
    Route::get('areas/editPhotos/{area}', ['as'=>'admin.areas.editPhotos', 'uses'=>'AreaController@editPhotos']);
    Route::get('areas/deletePhoto/{area}/{picture_id}', ['as'=>'admin.areas.deletePhoto', 'uses'=>'AreaController@deletePhoto']);
    Route::post('areas/addPhotos/{area}', ['as'=>'admin.areas.addPhotosPost', 'uses'=>'AreaController@addPhotosPost']);
    Route::post('areas/editPhotos/{brand}', ['as'=>'admin.areas.editPhotosPost', 'uses'=>'AreaController@editPhotosPost']);

    # Brands
    Route::resource('brands', 'BrandController');
//    Route::get('brands/addPhotos/{brand}', ['as'=>'admin.brands.addPhotos', 'uses'=>'BrandController@addPhotos']);
//    Route::get('brands/editPhotos/{brand}', ['as'=>'admin.brands.editPhotos', 'uses'=>'BrandController@editPhotos']);
//    Route::get('brands/deletePhoto/{brand}/{picture_id}', ['as'=>'admin.brands.deletePhoto', 'uses'=>'BrandController@deletePhoto']);
//    Route::post('brands/addPhotos/{brand}', ['as'=>'admin.brands.addPhotosPost', 'uses'=>'BrandController@addPhotosPost']);
//    Route::post('brands/editPhotos/{brand}', ['as'=>'admin.brands.editPhotosPost', 'uses'=>'BrandController@editPhotosPost']);

    # Product Categories
    Route::resource('PCategories', 'PCategoryController');
    Route::post('PCategories/uploadExcel', ['as'=>'admin.PCategories.uploadExcel', 'uses'=>'PCategories\UploadController@uploadExcel']);

    # Products
    Route::resource('products', 'ProductController');
    Route::get('products/addPhotos/{product}', ['as'=>'admin.products.addPhotos', 'uses'=>'ProductController@addPhotos']);
    Route::get('products/editPhotos/{product}', ['as'=>'admin.products.editPhotos', 'uses'=>'ProductController@editPhotos']);
    Route::get('products/deletePhoto/{product}/{picture_id}', ['as'=>'admin.products.deletePhoto', 'uses'=>'ProductController@deletePhoto']);
    Route::post('products/addPhotos/{product}', ['as'=>'admin.products.addPhotosPost', 'uses'=>'ProductController@addPhotosPost']);
    Route::post('products/editPhotos/{product}', ['as'=>'admin.products.editPhotosPost', 'uses'=>'ProductController@editPhotosPost']);

    # Services
    Route::resource('services', 'ServiceController');
    Route::get('services/addPhotos/{service}', ['as'=>'admin.services.addPhotos', 'uses'=>'ServiceController@addPhotos']);
    Route::get('services/editPhotos/{service}', ['as'=>'admin.services.editPhotos', 'uses'=>'ServiceController@editPhotos']);
    Route::get('services/deletePhoto/{service}/{picture_id}', ['as'=>'admin.services.deletePhoto', 'uses'=>'ServiceController@deletePhoto']);
    Route::post('services/addPhotos/{service}', ['as'=>'admin.services.addPhotosPost', 'uses'=>'ServiceController@addPhotosPost']);
    Route::post('services/editPhotos/{service}', ['as'=>'admin.services.editPhotosPost', 'uses'=>'ServiceController@editPhotosPost']);

    # Shops
    Route::resource('shops', 'ShopsController');
    Route::get('shops/addPhotos/{shop}', ['as'=>'admin.shops.addPhotos', 'uses'=>'ShopsController@addPhotos']);
    Route::get('shops/editPhotos/{shop}', ['as'=>'admin.shops.editPhotos', 'uses'=>'ShopsController@editPhotos']);
    Route::get('shops/deletePhoto/{shop}/{picture_id}', ['as'=>'admin.shops.deletePhoto', 'uses'=>'ShopsController@deletePhoto']);
    Route::post('shops/addPhotos/{shop}', ['as'=>'admin.shops.addPhotosPost', 'uses'=>'ShopsController@addPhotosPost']);
    Route::post('shops/editPhotos/{shop}', ['as'=>'admin.shops.editPhotosPost', 'uses'=>'ShopsController@editPhotosPost']);


    # Shop Categories
    Route::resource('shopCategory', 'ShopCategoryController');
    Route::resource('roles', 'RolesController');

    Route::get('logs', ['as'=>'admin.logs.index', 'uses'=>'LogsController@index']);
    Route::get('logs/{type}', ['as'=>'admin.logs.typeIndex', 'uses'=>'LogsController@typeIndex']);
    Route::get('logs/{type}/{id}', ['as'=>'admin.logs.objectTypeIndex', 'uses'=>'LogsController@objectTypeIndex']);

    Route::group(['prefix' => 'ajax'], function() {
        Route::get('products/getProducts/{search}', ['as' => 'admin.products.getProducts', 'uses' => 'AjaxController@getProducts']);
    });

});

Route::group(['middleware' => 'roles', 'roles' => 'admin'], function () {
    # Photos
    Route::get('addPhotos/{id}/{type}', ['as' => 'admin.addPhotos', 'uses' => 'PictureController@addPhotos']);
    Route::get('editPhotos/{id}/{type}', ['as'=>'admin.editPhotos', 'uses'=>'PictureController@editPhotos']);
    Route::get('deletePhoto/{id}/{picture_id}/{type}', ['as'=>'admin.deletePhoto', 'uses'=>'PictureController@deletePhoto']);
    Route::post('addPhotos/{id}/{type}', ['as' => 'admin.addPhotosPost', 'uses' => 'PictureController@addPhotosPost']);
    Route::post('editPhotos/{id}/{type}', ['as'=>'admin.editPhotosPost', 'uses'=>'PictureController@editPhotosPost']);

});