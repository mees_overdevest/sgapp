<?php
/**
 * Created by PhpStorm.
 * User: mees
 * Date: 04-08-16
 * Time: 13:50
 */

Route::group(['prefix' => 'auth', 'namespace'=>'Auth', 'middleware' => 'roles', 'roles' => 'user'], function () {

    Route::resource('areas', 'AreaController');
    Route::get('areas/getCoordinates/{id}', ['as'=>'auth.areas.getCoordinates', 'uses'=>'AjaxController@getAreaCoordinates']);
    Route::get('areas/favourite/{area}', ['as' => 'auth.areas.favourite', 'uses' => 'AreaController@favourite']);
    Route::get('areas/unFavourite/{area}', ['as' => 'auth.areas.unFavourite', 'uses' => 'AreaController@unFavourite']);

    # Shops
    Route::resource('shops', 'ShopController');
    Route::get('shops/favourite/{id}', ['as' => 'auth.shops.favourite', 'uses' => 'ShopController@favourite']);
    Route::get('shops/unFavourite/{id}', ['as' => 'auth.shops.unFavourite', 'uses' => 'ShopController@unFavourite']);

    # Brands
    Route::resource('brands', 'BrandController');
    Route::get('brands/favourite/{id}', ['as' => 'auth.brands.favourite', 'uses' => 'BrandController@favourite']);
    Route::get('brands/unFavourite/{id}', ['as' => 'auth.brands.unFavourite', 'uses' => 'BrandController@unFavourite']);

    # Services
    Route::resource('services', 'ServiceController');
    Route::get('services/favourite/{id}', ['as' => 'auth.services.favourite', 'uses' => 'ServiceController@favourite']);
    Route::get('services/unFavourite/{id}', ['as' => 'auth.services.unFavourite', 'uses' => 'ServiceController@unFavourite']);

    # Products
    Route::resource('products', 'ProductController');
    Route::get('products/favourite/{id}', ['as' => 'auth.products.favourite', 'uses' => 'ProductController@favourite']);
    Route::get('products/unFavourite/{id}', ['as' => 'auth.products.unFavourite', 'uses' => 'ProductController@unFavourite']);

    # Shop Categories
    Route::resource('shopCategory', 'ShopCategoryController');

    # Product Categories
    Route::resource('PCategories', 'PCategoryController');

    # Ajax Utilities
    Route::group(['prefix' => 'ajax'], function() {
        Route::get('areas/getAreas', ['as' => 'auth.areas.getAreas', 'uses' => 'AjaxController@getAreas']);
        Route::get('products/getProducts', ['as' => 'auth.products.getProducts', 'uses' => 'AjaxController@getProducts']);
        Route::post('areas/search', ['as' => 'auth.areas.search', 'uses' => 'AjaxController@search']);
        Route::post('areas/selectArea', ['as' => 'auth.areas.selectArea', 'uses' => 'AjaxController@selectArea']);
    });

    Route::group(['prefix'=>'my'], function()
    {
        Route::get('favourites', ['as'=>'auth.me.favourites.index', 'uses'=>'FavouriteController@index']);
    });
});

# Permissions: Logged in users only with shop
Route::group(['middleware'=>'auth', 'namespace'=>'Auth', 'prefix'=>'my'], function()
{
    Route::get('account', ['as'=>'auth.me', 'uses'=>'AccountController@index']);
    Route::post('account', ['as'=>'auth.me', 'uses'=>'AccountController@mePost']);

    Route::get('shop', ['as'=>'auth.me.shop', 'uses'=>'ShopController@myShop']);
    Route::get('shop/findMyArea/{id}', ['as'=>'auth.me.shop.findMyArea', 'uses'=>'ShopController@findMyArea']);
    Route::get('shop/addPhotos', ['as'=>'auth.me.shop.addPhotos', 'uses'=>'ShopController@addPhotos']);
    Route::get('shop/shopMarker/{id}', ['as'=>'auth.me.shop.addShopMarker', 'uses'=>'ShopController@addShopMarker']);
    Route::post('shop/shopMarker/{id}', ['as'=>'auth.me.shop.addShopMarkerPost', 'uses'=>'ShopController@addShopMarkerPost']);
    Route::post('shop', ['as'=>'auth.me.shop.update', 'uses'=>'ShopController@myShopPost']);
    Route::post('shop/addPhotos', ['as'=>'auth.me.shop.addPhotosPost', 'uses'=>'ShopController@addPhotosPost']);



});