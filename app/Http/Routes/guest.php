<?php

Route::get('wizards/intro', ['as'=>'wizards.intro', 'uses'=>'Guest\WizardController@intro']);

# Permissions: Guests only
Route::group(['middleware'=>'guest'], function(){

    Route::get('login', ['as'=>'auth.login', 'uses'=>'Auth\AuthController@login']);
    Route::get('register', ['as'=>'auth.register', 'uses'=>'Auth\AuthController@register']);
    Route::get('activate/{code}/{mail}', ['as'=>'auth.activate.account', 'uses'=>'Auth\AuthController@activate']);

    Route::post('register', ['as'=>'auth.register', 'uses'=>'Auth\AuthController@registerPost']);
    Route::post('login', ['as'=>'auth.login', 'uses'=>'Auth\AuthController@loginPost']);
    Route::get('password/email', ['as'=>'auth.forgotPasswordEmail', 'uses'=>'Auth\AuthController@forgotPassword']);
    Route::get('password/reset', ['as'=>'auth.forgotPassword.reset', 'uses'=>'Auth\AuthController@newPassword']);
    Route::post('password/reset', ['as'=>'auth.forgotPassword.reset', 'uses'=>'Auth\AuthController@newPasswordPost']);
    Route::post('password/email', ['as'=>'auth.forgotPasswordEmail', 'uses'=>'Auth\AuthController@forgotPasswordPost']);

    Route::get('login/facebook', 'Auth\AuthController@redirectToProvider');
    Route::get('login/facebook/callback', 'Auth\AuthController@handleProviderCallback');

});