<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

include "Routes/admin.php";
include "Routes/account.php";
include "Routes/guest.php";

Route::group(['prefix' => 'api', 'middleware' => 'cors'], function(){
	include "Routes/api.php";
});
// Route::get('/',['as'=> '/', 'uses'=>'HomeController@index']);

Route::get('/', ['as'=>'/','uses'=>'Guest\WizardController@intro']);

Route::post('search', ['as' => 'search', 'uses' => 'SearchController@search']);
//Route::auth();

Route::group(['prefix' => 'guest'], function(){
    Route::get('areas/show/{id}', ['as'=>'guest.areas.show', 'uses'=>'Auth\AreaController@show']);

    Route::get('brands/show/{id}', ['as'=>'guest.brands.show', 'uses'=>'Auth\BrandController@show']);

    Route::get('products/show/{id}', ['as'=>'guest.products.show', 'uses'=>'Auth\ProductController@show']);

    Route::get('services/show/{id}', ['as'=>'guest.services.show', 'uses'=>'Auth\ServiceController@show']);

    Route::get('PCategories/show/{id}', ['as'=>'guest.PCategories.show', 'uses'=>'Auth\PCategoryController@show']);

    Route::get('shopCategory/show/{id}', ['as'=>'guest.shopCategory.show', 'uses'=>'Auth\ShopCategoryController@show']);

    Route::get('shops/show/{id}', ['as'=>'guest.shops.show', 'uses'=>'Auth\ShopController@show']);
});


Route::get('logout', ['as'=>'auth.logout', 'uses'=>'Auth\AuthController@logout']);

// User
Route::group(['middleware' => 'auth'], function() {
    Route::get('user/reset-password', ['as' => 'user.resetpassword', 'uses' => 'Admin\UsersController@userRequestChangePassword']);
});
