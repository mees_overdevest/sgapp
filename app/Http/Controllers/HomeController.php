<?php

namespace App\Http\Controllers;

use App\Helpers\LogHelper;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Shop;
use App\Models\PCategory;
use App\Models\Area;
use App\Models\Product;
use App\Models\Service;
use App\Models\Brand;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    public function index(){
        $shops = Shop::take(3)->get();
        $PCategories = PCategory::take(3)->get();
        $areas = Area::take(3)->get();
        $products = Product::take(3)->get();
        $services = Service::take(3)->get();
        $brands = Brand::take(3)->get();

        if(Auth::check()){
            $log = new LogHelper();
            $log->makeLog('homepage', auth()->user(), 'frontpages');
        }

        return view('front.home', compact('shops','PCategories','areas','products','services', 'brands'));
    }
}
