<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\PCategory;
use App\Models\Service;
use App\Models\Shop;
use App\Models\Brand;
use App\Models\shopCategory;
use App\Models\Area;

use App\Http\Requests;

class SearchController extends Controller
{
    public function search(Request $request){
        $term = $request->get('term');

        $results = [];

        foreach(config('site.types') as $type){

            if($type == 'productCategory'){
                $type = 'PCategory';
            }

            $className = 'App\\Models\\' . ucfirst($type);

            if($type == 'PCategory'){
                $type = substr_replace($type, 'ies', -1);
            }

            $results[$type] = $className::where('title', 'LIKE', '%' . $term . '%')->get();

        }

        return view('front.search.index', compact('results', 'term'));
    }
}
