<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\PCategory;
use App\Http\Controllers\Controller;

class PCategoryController extends Controller
{
    public function getCategories(){

        $categories = PCategory::whereNull('parent_id')->get();

    	$res = [];
    	foreach ($categories as $category) {
    		$res[$category->id] = $category;
            $res[$category->id]['childs'] = $category->children;
            unset($res[$category->id]['children']);
    	}

    	return response()->json($res);
    }
}
