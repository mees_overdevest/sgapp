<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Product;
use App\Models\PCategory;
use App\Models\Service;
use App\Helpers\ImageManager;
use App\Models\Shop;
use App\Models\Brand;
use App\Models\shopCategory;
use App\Models\Area;
use App\Models\Picture;

class PictureController extends Controller
{
    public function deletePhoto($id, $picture_id, $type){
        $className = $this->classString($type);

        $object = $className::findOrFail($id);

        if($type == 'PCategory'){
            $type = substr_replace($type, 'ies', -1);
        }

        $picture = Picture::findOrFail($picture_id);
        $imageManager = new ImageManager();

        $object->pictures()->detach($picture->id);

        $imageManager->deleteImage($picture, $object->id);

        $picture->delete();

        return redirect()->route('admin.editPhotos', [$object->id, $type])->with('Success', $object->getImageText('deleteSuccess'));
    }

    public function addPhotos($id, $type){
        $className = $this->classString($type);

        $object = $className::findOrFail($id);

        return view('admin.photos.add', compact('object', 'type'));
    }

    public function addPhotosPost(Request $request, $id, $type)
    {
        $className = $this->classString($type);

        $object = $className::findOrFail($id);

        # Array with image ids
        $images = [];

        # Helper Class for Image Paths
        $imageManager = new ImageManager();

        $i = 0;
        # loop over the sent images
        foreach($request->file('image') as $file){

            # Check if submitted image has the right info
            if($request->get('image_name')[$i] != "" && $request->get('image_description')[$i] && $file != null) {

                $image = new Picture([
                    'image_name' => $request->get('image_name')[$i],
                    'image_extension' => $file->getClientOriginalExtension(),
                    'description' => $request->get('image_description')[$i],
                ]);

                // assigning the image paths to the model
                $image->type = $type;

                // Save image to DB
                $image->save();

                // Getting the parts of the image we need
                $images[] = $image->id;

                $i++;
                // Query the imageuploader to put the images on in the right places
                $imageManager->createImages($file, $image->image_name, $type, $object->id);
            }
        };

        $object->pictures()->attach($images);

        $route = $this->getTypeIndexRoute($type);

        return redirect()->route($route)->with('Success', $object->getImageText('addSuccess'));
    }

    public function editPhotos($id, $type){
        $className = $this->classString($type);

        $object = $className::findOrFail($id);
        return view('admin.photos.edit', compact('object', 'type'));
    }

    public function editPhotosPost(Request $request, $id, $type){
        $className = $this->classString($type);

        $object = $className::findOrFail($id);

        $new_descriptions = $request->get('image_description');

        # Helper Class for Image Paths
        $imageManager = new ImageManager();
//        dd($arr);
        foreach ($request->get('image_name') as $key => $value){
            $picture = Picture::findOrFail($key);
            $old_picture_name = $picture->image_name;

            $picture->image_name = $value;
            $picture->description = $new_descriptions[$key];
            $picture->save();

            $imageManager->renameImage($old_picture_name, $picture, $object->id);
        }

        $route = $this->getTypeIndexRoute($type);

        return redirect()->route($route)->with('Success', $object->getImageText('editSuccess'));
    }

    private function classString($str){
        return $className = 'App\\Models\\' . ucfirst($str);
    }

    private function getTypeIndexRoute($type){

        if($type == 'PCategory'){
            $type = substr_replace($type, 'ies', -1);
        } else {
            $type = $type . 's';
        }

        return 'admin.' . $type . '.index';
    }
}
