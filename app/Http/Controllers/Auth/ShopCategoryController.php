<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Models\shopCategory;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ShopCategoryController extends Controller
{
    public function index(){
        $shopCategories = shopCategory::whereNull('parent_id')->with('children')->get();

        return view('front.shopCategories.index', compact('shopCategories'));
    }

    public function show($id){
        $shopCategory = shopCategory::findOrFail($id);

        return view('front.shopCategories.show', compact('shopCategory'));
    }
}
