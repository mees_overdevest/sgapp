<?php

namespace App\Http\Controllers\Auth;

use App\Helpers\FavouriteHelper;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Helpers\LogHelper;
use Illuminate\Support\Facades\Auth;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{
    protected $type = "product";

    public function index(){
        $products = Product::paginate(30);
        return view('front.products.index', compact('products'));
    }

    public function show($id){
        $product = Product::findOrFail($id);

        if(Auth::check()){
            $log = new LogHelper();
            $log->makeLog($this->type, auth()->user(), $product);
        }

        return view('front.products.show', compact('product'));
    }

    public function favourite($id){
        $product = Product::findOrFail($id);
        $favourite = new FavouriteHelper();

        if($favourite->AddFavourite($this->type, $product) == true){
            return redirect()->back()->with('Success', 'Het product: ' . $product->name . ' is toegevoegd aan uw favorieten!');
        } else {
            return redirect()->back()->with('Error', 'Oops! Er ging iets fout. Het product: ' . $product->name . ' is niet toegevoegd aan uw favorieten!');
        }
    }

    public function unFavourite($id){
        $favourite = new FavouriteHelper();

        if($favourite->DeleteFavourite($id) == true){
            return redirect()->back()->with('Success', 'Het product is verwijderd van uw favorieten!');
        } else {
            return redirect()->back()->with('Error', 'Oops! Er ging iets fout. Het product is niet verwijderd van uw favorieten!');
        }
    }
}
