<?php

namespace App\Http\Controllers\Auth;

use App\Helpers\FavouriteHelper;
use App\Models\Favourite;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class FavouriteController extends Controller
{

    private $favouriteClient;

    public function __construct(){
        $this->favouriteClient = new FavouriteHelper();
    }


    public function index() {
        $favourites = $this->favouriteClient->getFavouritesPerType(auth()->user());

        return view('front.account.favourites.index', compact('favourites'));
    }
}
