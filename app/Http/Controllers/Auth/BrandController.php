<?php

namespace App\Http\Controllers\Auth;

use App\Helpers\FavouriteHelper;
use Illuminate\Http\Request;
use App\Models\Brand;
use App\Helpers\LogHelper;
use Illuminate\Support\Facades\Auth;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class BrandController extends Controller
{
    protected $type = "brand";

    public function index(){
        $brands = Brand::paginate(30);
        return view('front.brands.index', compact('brands'));
    }

    public function show($id){
        $brand = Brand::findOrFail($id);
        if(Auth::check()){
            $log = new LogHelper();
            $log->makeLog($this->type, auth()->user(), $brand);
        }

        return view('front.brands.show', compact('brand'));
    }

    public function favourite($id){
        $brand = Brand::findOrFail($id);
        $favourite = new FavouriteHelper();

        if($favourite->AddFavourite($this->type, $brand) == true){
            return redirect()->back()->with('Success', 'Het merk: ' . $brand->name . ' is toegevoegd aan uw favorieten!');
        } else {
            return redirect()->back()->with('Error', 'Oops! Er ging iets fout. Het merk: ' . $brand->name . ' is niet toegevoegd aan uw favorieten!');
        }
    }

    public function unFavourite($id){
        $favourite = new FavouriteHelper();

        if($favourite->DeleteFavourite($id) == true){
            return redirect()->back()->with('Success', 'Het merk is verwijderd van uw favorieten!');
        } else {
            return redirect()->back()->with('Error', 'Oops! Er ging iets fout. Het merk is niet verwijderd van uw favorieten!');
        }
    }
}
