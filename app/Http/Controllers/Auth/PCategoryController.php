<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Models\PCategory;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class PCategoryController extends Controller
{
    public function index(){
        $PCategories = PCategory::whereNull('parent_id')->with('children')->get();

        return view('front.PCategories.index', compact('PCategories'));
    }

    public function show($id){
        $PCategory = PCategory::findOrFail($id);

        return view('front.PCategories.show', compact('PCategory'));
    }
}
