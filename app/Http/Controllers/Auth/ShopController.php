<?php

namespace App\Http\Controllers\Auth;

use App\Helpers\FavouriteHelper;
use App\Helpers\LogHelper;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Shop;
use App\Models\shopCategory;
use App\Helpers\ImageManager;
use App\Models\Picture;

class ShopController extends Controller
{
    protected $type = "shop";

    public function index(){
        $shops = Shop::all();

        return view('front.shops.index',compact('shops'));
    }

    public function show($id){
        $shop = Shop::findOrFail($id);

        if(Auth::check()){
            $log = new LogHelper();
            $log->makeLog($this->type, auth()->user(), $shop);
        }

        return view('front.shops.show',compact('shop'));
    }

    public function myShop(){
        $shop = auth()->user()->getOwnedShop()->first();

        $cats = shopCategory::whereNull('parent_id')->with('children')->get();

        $categories = [];
        foreach($cats as $cat){
            $cat->title = "Hoofdcategorie: " . $cat->title;
            $categories[] = array($cat->id => $cat->title);
            foreach($cat->children as $child){
                $child->title = "Subcategorie: " . $child->title;
                $categories[] = array($child->id => $child->title);
            }
        }

        return view('front.shops.my', compact('shop', 'categories'));
    }

    public function myShopPost(Request $request){
        $shop = Shop::findOrFail($request->get('id'));
//dd($request->input('since'));
//        dd(strtotime($request->input('since')));
        $shop->title = $request->input('title');
        $shop->description = $request->input('description');
        $shop->since = strtotime($request->input('since'));
        $shop->city = $request->input('city');
        $shop->address = $request->input('address');
        $shop->update();

        $cats = [];

        foreach($request->get('category_id') as $key => $value){
            $cats[] = $value;
        }

        $shop->categories()->sync($cats);

        return redirect()->route('auth.me.shop');
    }

    public function addPhotos(){
        $shop = auth()->user()->getOwnedShop()->first();
        return view('front.shops.ShopAddPhotos', compact('shop'));
    }

    public function addPhotosPost(Request $request)
    {
        $shop = auth()->user()->getOwnedShop()->first();
        # Array with image ids
        $images = [];

        # Helper Class for Image Paths
        $uploader = new ImageManager();

        $i = 0;
        # loop over the sent images
        foreach($request->file('image') as $img){

            # Check if submitted image has the right info
            if($request->get('image_name')[$i] != "" && $request->get('image_description')[$i] && $img != null) {

                $image = new Picture([
                    'image_name' => $request->get('image_name')[$i],
                    'image_extension' => $img->getClientOriginalExtension(),
                    'description' => $request->get('image_description')[$i],
                ]);

                // assigning the image paths to the model
                $image->type = $this->type;

                // Save image to DB
                $image->save();

                // Getting the parts of the image we need
                $images[] = $image->id;

                $i++;
                // Query the imageuploader to put the images on in the right places
                $uploader->createImages($img, $image->image_name, $this->type, $shop->id);
            }
        };

        $shop->pictures()->attach($images);

        return redirect()->route('auth.me.shop')->with('Success', trans('shops/wizard.addPhotos.success', ['name' => $shop->name]));
    }

    public function addShopMarker($id){
        $shop = Shop::findOrFail($id);
        return view('front.shops.shopMarker', compact('shop'));
    }

    public function addShopMarkerPost($id, Request $request){
        $shop = Shop::findOrFail($id);
        $shop->places_id = $request->get('places_id');
        $shop->save();

        return redirect()->route('auth.me.shop')->with('Success', trans('shops/wizard.addMarker.success', ['name' => $shop->name]));
    }

    public function findMyArea($id){
        $shop = Shop::findOrFail($id);
        return view('front.shops.findMyArea', compact('shop'));
    }

    public function favourite($id){
        $shop = Shop::findOrFail($id);
        $favourite = new FavouriteHelper();

        if($favourite->AddFavourite($this->type, $shop) == true){
            return redirect()->back()->with('Success', 'De winkel: ' . $shop->name . ' is toegevoegd aan uw favorieten!');
        } else {
            return redirect()->back()->with('Error', 'Oops! Er ging iets fout. Het winkelgebied: ' . $shop->name . ' is niet toegevoegd aan uw favorieten!');
        }
    }

    public function unFavourite($id){
        $favourite = new FavouriteHelper();

        if($favourite->DeleteFavourite($id) == true){
            return redirect()->back()->with('Success', 'De winkel is verwijderd van uw favorieten!');
        } else {
            return redirect()->back()->with('Error', 'Oops! Er ging iets fout. De winkel is niet verwijderd van uw favorieten!');
        }
    }
}
