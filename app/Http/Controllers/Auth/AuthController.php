<?php

namespace App\Http\Controllers\Auth;

use App\Models\Account\User;
use App\Models\Account\Language;
use Validator;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;
use App\Helpers\Generators;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

//    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
//    protected $redirectTo = '/';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    /**
     * Login Attempt
     *
     * @param Request $request
     * @return mixed
     */
    public function loginPost(Request $request)
    {
        $data = [
            'email'         	=> $request->get('email'),
            'password'      	=> $request->get('password'),
            'active'        	=> 1,
        ];

        $validator = Validator::make($request->all(), [
            'email' => 'required|max:255',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }

        $remember = $request->get('remember');

        if(Auth::attempt($data, ($remember != null) ? true : false))
        {
            return Redirect::route('/')->with('Success', 'You\'ve been logged in successfully.');
//            Session::put('Login', Lang::get('auth.login.success'));
//            return new JsonResponse(['redirect'=>route('app.index')]);
        }
        else
        {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput()
                ->with('Info', 'Er is iets fout gegaan bij uw inlogpoging. Probeer het later opnieuw.');
        }
    }


    /**
     * Register View
     * @return \Illuminate\View\View
     */
    public function register()
    {
        $title = trans('auth.register.title');
        $languages  = Language::lists('name', 'id');

        return view('auth.register', compact('title', 'languages'));
    }

    /**
     * Register View
     * @return \Illuminate\View\View
     */
    public function login()
    {
        $title = trans('auth.login.title');

        return view('auth.login', compact('title'));
    }

    /**
     * Register POST Request and send mail after
     *
     * @param Requests\Users\CreateUserRequest $request
     * @return mixed
     */
    public function registerPost(Requests\Users\CreateUserRequest $request)
    {
        $user = new User();
        $user->email = $request->get('email');
        $user->language_id = $request->get('language_id');
        $user->gender = $request->get('gender');
        $user->firstName = $request->get('firstName');
        $user->lastName = $request->get('lastName');
        $user->password = Hash::make($request->get('password'));

        $code = Generators::uniqueCode();

        $user->code = $code;
        $user->active = 0;
        $user->save();

        // Send mail with the comments
        $data = [
            'mail'         => $user->email,
            'name'          => $user->fullName(),
            'code'             => $code
        ];

        Mail::send('emails.auth.activation', $data, function($message) use ($user)
        {
            $message->from(config('site.auth.no-reply.mail'), config('site.auth.no-reply.name'));
            $message->to($user['email'], $user['firstName'] . ' '. $user['lastName'])->subject(trans('admin/users.messages.welcome'));
        });

        return Redirect::route('/')->with('Success', 'You\'ve been registered successfully. Activate your account with the link inside the e-mail.');

//        return Redirect::route('auth.register')->with('Success', Lang::get('auth.verification.send'));
    }

    /**
     * Activate my account
     */
    public function activate($code, $mail) {
        $user = User::where('code', $code)->where('email', $mail)->first();
        $user->active = 1;
        $user->code = 0;
        $user->save();

        Auth::login($user);

        return Redirect::route('/')->with('Success', 'You\'ve been logged in successfully.');
    }

    public function forgotPassword(){
        return view('auth.passwords.email');
    }

    public function newPassword(){
        return view('auth.passwords.reset');
    }

    /**
     * Password forgot Post Request
     * @param Requests\Users\CreatePasswordForgotRequest $request
     * @return mixed
     */
    public function forgotPasswordPost(Request $request)
    {
//        dd($request->get('email'));
        $user = User::where('email', $request->get('email'))->first();

        $code = Generators::uniqueCode();
        $user->code = $code;
        $user->save();

        $data = [
            'name'      => $user->fullName(),
            'code'      => $code,
            'mail'     => $request->get('email'),
        ];

        Mail::send('emails.auth.activation', $data, function($message) use ($data)
        {
            $message->from(config('site.auth.no-reply.mail'), config('site.auth.no-reply.name'));
            $message->to($data['mail'], $data['name'])->subject(trans('account.password.submit'));
        });
        return redirect()->route('auth.forgotPassword.reset')->with('Success', 'An e-mail with your password reset code has been sent to your inbox!');
        //return new JsonResponse(['success'=>trans('auth.password.success')], 200);
    }

    /**
     * New password Post Request
     * @param Requests\Users\CreateNewPasswordRequest $request
     * @return mixed
     */
    public function newPasswordPost(Request $request)
    {
        $user = User::where('email', $request->get('email'))->where('code', $request->get('code'))->first();

        if($user != null)
        {
            $user->code = 0;
            $user->password = Hash::make($request->get('password'));
            $user->save();

//            return new JsonResponse(['success'=>Lang::get('auth.newPassword.success')], 200);
            return redirect()->route('auth.login')->with('Success', 'Your new password has been set!');
        }
        else
        {
//            return new JsonResponse([Lang::get('auth.newPassword.null')], 422);
            return redirect()->route('auth.login')->with('Error', 'Your code did not work, if this happens more often contact us!');
        }
    }

    /**
     * Redirect the user to the Facebook authentication page.
     *
     * @return Response
     */
    public function redirectToProvider()
    {
        return Socialite::driver('facebook')->redirect();
    }

    /**
     * Obtain the user information from Facebook.
     *
     * @return Response
     */
    public function handleProviderCallback()
    {
        try {
            $user = Socialite::driver('facebook')->user();
        } catch (Exception $e) {
            return redirect('auth/facebook');
        }

        $authUser = $this->findOrCreateUser($user);

        Auth::login($authUser, true);

        return redirect()->route('/')->with('Success', 'You\'ve been logged in successfully with Facebook.');
    }

    /**
     * Return user if exists; create and return if doesn't
     *
     * @param $facebookUser
     * @return User
     */
    private function findOrCreateUser($facebookUser)
    {
        $authUser = User::where('facebook_id', $facebookUser->id)->first();

        if ($authUser){
            return $authUser;
        }

        return User::create([
            'name' => $facebookUser->name,
            'email' => $facebookUser->email,
            'facebook_id' => $facebookUser->id,
//            'avatar' => $facebookUser->avatar,
            'language_id' => 1
        ]);
    }

    /**
     * Logout
     * @return mixed
     */
    public function logout()
    {
        Auth::logout();
        return Redirect::route('/')->with('Success', 'You have logged out!');
    }
}
