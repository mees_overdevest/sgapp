<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Models\Account\Language;
use App\Models\Account\User;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class AccountController extends Controller
{
    /**
     * Changing standard account settings
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $title = trans('account.actions.edit');
        $languages = Language::lists('name','id');
        $user = auth()->user();

        return view('front.account.index', compact('title','user','languages'));
    }

    /**
     * (POST) Changing standard account settings
     * @param Requests\Users\CreateAccountRequest $request
     * @return mixed
     */
    public function mePost(Request $request)
    {
        $user = User::find(auth()->user()->id)->update($request->all());

        return redirect()->route('auth.me')->with('Success', trans('account.messages.success'));
    }
}
