<?php

namespace App\Http\Controllers\Auth;

use App\Models\Area;
use App\Models\Product;
use App\Models\Shop;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class AjaxController extends Controller
{
    public function getAreas(){
        $areas = Area::with('coordinates')->get();

        $mapAreas = [];
        foreach($areas as $area) {

            $mapArea = [];

            $i = 0;

            $info = [
                'title' => $area->title,
                'city' => $area->city,
                'id' => $area->id
            ];

            $mapArea['info'] = $info;

            foreach ($area->coordinates as $coordinate) {
                $mapArea[$i]['lat'] = $coordinate->lat;
                $mapArea[$i]['lng'] = $coordinate->lng;

                $i += 1;
            }
            $mapAreas[] = $mapArea;
        }
//    dd($coordinates);
        return new JsonResponse($mapAreas,200);
    }

    public function getProducts($input)
    {
        $products = Product::where('title', '?' . $input . '?')->take(5);
        return new JsonResponse($products, 200);
    }

    public function getAreaCoordinates($id){
        $area = Area::findOrFail($id);

        $coordinates = [];
        $i = 0;
        foreach($area->coordinates as $coordinate){
            $coordinates[$i]['lat'] = $coordinate->lat;
            $coordinates[$i]['lng'] = $coordinate->lng;

            $i += 1;
        }

        return new JsonResponse($coordinates,200);
    }

    public function search(Request $request){
        $queryName = $request->get('city');

        if($request->exists('city')){
            $areas = Area::where('city',  'LIKE', '%' .$request->get('city') . '%')->get();
            return new JsonResponse($areas,200);
        } elseif($request->exists('title')){
            $areas = Area::where('title',  'LIKE', '%' .$request->get('title') . '%')->get();
            return new JsonResponse($areas,200);
        }
        return $queryName;
//        return new JsonResponse($queryName);
    }

    public function selectArea(Request $request){
        $shopId = $request->get('shopId');
        $areaId = $request->get('areaId');

        $shop = Shop::findOrFail($shopId);
        $shop->areas()->sync([$areaId]);

        return new JsonResponse('success',200);
    }

}
