<?php

namespace App\Http\Controllers\Auth;

use App\Helpers\FavouriteHelper;
use Illuminate\Http\Request;
use App\Models\Service;
use App\Helpers\LogHelper;
use Illuminate\Support\Facades\Auth;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ServiceController extends Controller
{
    protected $type = "service";

    public function index(){
        $services = Service::paginate(30);
        return view('front.services.index', compact('services'));
    }

    public function show($id){
        $service = Service::findOrFail($id);

        if(Auth::check()){
            $log = new LogHelper();
            $log->makeLog($this->type, auth()->user(), $service);
        }

        return view('front.services.show', compact('service'));
    }

    public function favourite($id){
        $service = Service::findOrFail($id);
        $favourite = new FavouriteHelper();

        if($favourite->AddFavourite($this->type, $service) == true){
            return redirect()->back()->with('Success', 'De service: ' . $service->name . ' is toegevoegd aan uw favorieten!');
        } else {
            return redirect()->back()->with('Error', 'Oops! Er ging iets fout. De service: ' . $service->name . ' is niet toegevoegd aan uw favorieten!');
        }
    }

    public function unFavourite($id){
        $favourite = new FavouriteHelper();

        if($favourite->DeleteFavourite($id) == true){
            return redirect()->back()->with('Success', 'De service is verwijderd van uw favorieten!');
        } else {
            return redirect()->back()->with('Error', 'Oops! Er ging iets fout. De service is niet verwijderd van uw favorieten!');
        }
    }
}
