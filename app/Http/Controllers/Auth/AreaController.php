<?php

namespace App\Http\Controllers\Auth;

use App\Helpers\FavouriteHelper;
use App\Helpers\LogHelper;
use App\Models\Area;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AreaController extends Controller
{
    protected $type = "area";

    public function index(){
//        $areas = Area::with('coordinates')->get();
//        dd($areas);
        return view('front.areas.index');
    }

    public function show($id){
        $area = Area::findOrFail($id);

        if(Auth::check()){
            $log = new LogHelper();
            $log->makeLog($this->type, auth()->user(), $area);
        }

        return view('front.areas.show', compact('area'));
    }

    public function favourite($id){
        $area = Area::findOrFail($id);
        $favourite = new FavouriteHelper();

        if($favourite->AddFavourite($this->type, $area) == true){
            return redirect()->back()->with('Success', 'Het winkelgebied: ' . $area->name . ' is toegevoegd aan uw favorieten!');
        } else {
            return redirect()->back()->with('Error', 'Oops! Er ging iets fout. Het winkelgebied: ' . $area->name . ' is niet toegevoegd aan uw favorieten!');
        }
    }

    public function unFavourite($id){
        $favourite = new FavouriteHelper();

        if($favourite->DeleteFavourite($id) == true){
            return redirect()->back()->with('Success', 'Het winkelgebied is verwijderd van uw favorieten!');
        } else {
            return redirect()->back()->with('Error', 'Oops! Er ging iets fout. Het winkelgebied is niet verwijderd van uw favorieten!');
        }
    }
}
