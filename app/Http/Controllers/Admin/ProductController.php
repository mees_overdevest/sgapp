<?php

namespace App\Http\Controllers\Admin;

use App\Models\Brand;
use App\Models\Product;
use App\Models\Picture;
use App\Models\PCategory;
use Illuminate\Http\Request;

use App\Helpers\ImageManager;
use Illuminate\Support\Facades\Input;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{
    protected $type = "product";

    public function index(){
        $products = Product::paginate(30);
        return view('admin.products.index', compact('products'));
    }

    public function show($id){
        $product = Product::findOrFail($id);

        return view('admin.products.show', compact('product'));
    }

    public function create(){
        $cats = PCategory::whereNull('parent_id')->with('children')->get();
        $brands = Brand::lists('name','id');

        $categories = [];
        foreach($cats as $cat){
            $cat->title = "Hoofdcategorie: " . $cat->title;
            $categories[] = array($cat->id => $cat->title);
            foreach($cat->children as $child){
                $child->title = "Subcategorie: " . $child->title;
                $categories[] = array($child->id => $child->title);
            }
        }
        return view('admin.products.create', compact('categories','brands'));
    }

    public function edit($id){
        $product = Product::findOrFail($id);
        $cats = PCategory::whereNull('parent_id')->with('children')->get();

        $categories = [];
        foreach($cats as $cat){
            $cat->title = "Hoofdcategorie: " . $cat->title;
            $categories[] = array($cat->id => $cat->title);
            foreach($cat->children as $child){
                $child->title = "Subcategorie: " . $child->title;
                $categories[] = array($child->id => $child->title);
            }
        }
        return view('admin.products.edit', compact('product', 'categories'));
    }

    public function update(Request $request, $id){
        $product = Product::findOrFail($id);
        $product->title = $request->get('title');
        $product->description = $request->get('description');
        $product->update();

        $categories = [];
        foreach($request->input('category_id') as $key => $value){
            $categories[] = $value;
        }
        $product->categories()->sync($categories);

        return redirect()->route('admin.products.index')->with('Success', trans('products/product.update.success', ['name' => $product->title]));
    }

    public function store(Requests\Admin\Product\CreateRequest $request){
        $product = Product::create($request->all());

        $categories = [];
        foreach($request->input('category_id') as $key => $value){
            $categories[] = $value;
        }
        $product->categories()->attach($categories);
        $product->brand()->attach([$request->input('brand_id')]);

        return redirect()->route('admin.products.index')->with('Success', trans('products/product.create.success', ['name' => $product->title]));
    }

    public function destroy($id){
        $product = Product::findOrFail($id);
        $pictures = $product->pictures()->get();
        $imageManager = new ImageManager();

        foreach ($pictures as $picture){
            $product->pictures()->detach($picture->id);
            $imageManager->deleteImage($picture, $product->id);
            $picture->delete();
        }

        $product->delete();

        return redirect()->route('admin.products.index')->with('Success', "Het product ". $product->title ." is succesvol verwijderd.");
    }

    public function addPhotos(Product $product){
        return view('admin.products.addPhotos', compact('product'));
    }

    public function addPhotosPost(Request $request, Product $product)
    {
        # Array with image ids
        $images = [];

        # Helper Class for Image Paths
        $uploader = new ImageManager();

        $i = 0;
        # loop over the sent images
        foreach($request->file('image') as $img){

            # Check if submitted image has the right info
            if($request->get('image_name')[$i] != "" && $request->get('image_description')[$i] && $img != null) {

                $image = new Picture([
                    'image_name' => $request->get('image_name')[$i],
                    'image_extension' => $img->getClientOriginalExtension(),
                    'description' => $request->get('image_description')[$i],
                ]);

                // assigning the image paths to the model
                $image->type = $this->type;

                // Save image to DB
                $image->save();

                // Getting the parts of the image we need
                $images[] = $image->id;

                $i++;
                // Query the imageuploader to put the images on in the right places
                $uploader->createImages($img, $image->image_name, $this->type, $product->id);
            }
        };

        $product->pictures()->attach($images);

        return redirect()->route('admin.products.index')->with('Success', trans('products/product.addPhotos.success', ['name' => $product->title]));
    }

    public function deletePhoto(Product $product, $picture_id){
        $picture = Picture::findOrFail($picture_id);
        $imageManager = new ImageManager();

        $product->pictures()->detach($picture->id);

        $imageManager->deleteImage($picture, $product->id);

        $picture->delete();

        return redirect()->route('admin.products.editPhotos', $product->id)->with('Success', "De afbeelding is succesvol verwijderd van het product: ". $product->title."!");
    }

    public function editPhotos(Product $product){
        return view('admin.products.editPhotos', compact('product'));
    }

    public function editPhotosPost(Request $request, Product $product){

        $new_descriptions = $request->get('image_description');

        # Helper Class for Image Paths
        $imageManager = new ImageManager();
//        dd($arr);
        foreach ($request->get('image_name') as $key => $value){
            $picture = Picture::findOrFail($key);
            $old_picture_name = $picture->image_name;

            $picture->image_name = $value;
            $picture->description = $new_descriptions[$key];
            $picture->save();

            $imageManager->renameImage($old_picture_name, $picture, $product->id);
        }

        return redirect()->route('admin.products.editPhotos', $product->id)->with('Success', "De afbeelding van het product: ". $product->title." zijn succesvol aangepast!");
    }
}
