<?php

namespace App\Http\Controllers\Admin;

use App\Models\Area;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Models\Product;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class AjaxController extends Controller
{
    public function getAreaCoordinates($id){
        $area = Area::findOrFail($id);

        $coordinates = [];
        $i = 0;
        foreach($area->coordinates as $coordinate){
            $coordinates[$i]['lat'] = $coordinate->lat;
            $coordinates[$i]['lng'] = $coordinate->lng;

            $i += 1;
        }

        return new JsonResponse($coordinates,200);
    }

    public function getProducts($search)
    {
//        dd($search);
        $products = Product::where('title', 'LIKE', '%' . $search . '%')->lists('title','id');
//        dd($request->get('search'));
        $result = [];
        foreach ($products as $key => $value){
            $result[] = [
                'id' => $key,
                'name' => $value
            ];
        }
//        foreach ()
//        $products = $request->get('search');

        return new JsonResponse($products, 200);
        // http://fareez.info/blog/create-your-own-autocomplete-using-vuejs/
    }
}
