<?php

namespace App\Http\Controllers\Admin;

use App\Models\Brand;
use App\Models\Picture;
use Illuminate\Http\Request;

use App\Helpers\ImageManager;
use Illuminate\Support\Facades\Input;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class BrandController extends Controller
{
    protected $type = "brand";

    public function index(){
        $brands = Brand::paginate(30);
        return view('admin.brands.index', compact('brands'));
    }

    public function create(){
        return view('admin.brands.create');
    }

    public function show($id){
        $brand = Brand::findOrFail($id);
        return view('admin.brands.show', compact('brand'));
    }

    public function edit($id){
        $brand = Brand::findOrFail($id);

        return view('admin.brands.edit', compact('brand'));
    }

    public function update(Request $request, $id){
        $brand = Brand::findOrFail($id);
        $brand->title = $request->get('title');
        $brand->description = $request->get('description');
        $brand->update();

        return redirect()->route('admin.brands.index')->with('Success', 'Het merk ' . $brand->title . ' is succesvol bijgewerkt.');
    }

    public function store(Requests\Admin\Brand\CreateRequest $request){
        $brand = new Brand();
        $brand->title = $request->input('title');
        $brand->description = $request->input('description');
        $brand->save();

        return redirect()->route('admin.brands.index')->with('Success', trans('brands/brand.create.success', ['name' => $brand->title]));
    }

    public function destroy($id){
        $brand = Brand::findOrFail($id);
        $pictures = $brand->pictures()->get();
        $imageManager = new ImageManager();

        $brand->pictures()->detach($pictures);

        foreach ($pictures as $picture){
            $brand->pictures()->detach($picture->id);
            $imageManager->deleteImage($picture, $brand->id);
            $picture->delete();
        }

        $brand->delete();

        return redirect()->route('admin.brands.index')->with('Success', trans('brands/brand.deleted', ['name' => $brand->name]));
    }

    public function deletePhoto(Brand $brand, $picture_id){
        $picture = Picture::findOrFail($picture_id);
        $imageManager = new ImageManager();

        $brand->pictures()->detach($picture->id);

        $imageManager->deleteImage($picture, $brand->id);

        $picture->delete();

        return redirect()->route('admin.brands.editPhotos', $brand->id)->with('Success', "De afbeelding is succesvol verwijderd van het merk: ". $brand->name."!");
    }

    public function addPhotos(Brand $brand){
        return view('admin.brands.addPhotos', compact('brand'));
    }

    public function addPhotosPost(Request $request, Brand $brand)
    {
        # Array with image ids
        $images = [];

        # Helper Class for Image Paths
        $imageManager = new ImageManager();

        $i = 0;
        # loop over the sent images
        foreach($request->file('image') as $file){

            # Check if submitted image has the right info
            if($request->get('image_name')[$i] != "" && $request->get('image_description')[$i] && $file != null) {

                $image = new Picture([
                    'image_name' => $request->get('image_name')[$i],
                    'image_extension' => $file->getClientOriginalExtension(),
                    'description' => $request->get('image_description')[$i],
                ]);

                // assigning the image paths to the model
                $image->type = $this->type;

                // Save image to DB
                $image->save();

                // Getting the parts of the image we need
                $images[] = $image->id;

                $i++;
                // Query the imageuploader to put the images on in the right places
                $imageManager->createImages($file, $image->image_name, $this->type, $brand->id);
            }
        };

        $brand->pictures()->attach($images);

        return redirect()->route('admin.brands.index')->with('Success', trans('brands/brand.addPhotos.success', ['name' => $brand->name]));
    }

    public function editPhotos(Brand $brand){
        return view('admin.brands.editPhotos', compact('brand'));
    }

    public function editPhotosPost(Request $request, Brand $brand){

        $new_descriptions = $request->get('image_description');

        # Helper Class for Image Paths
        $imageManager = new ImageManager();
//        dd($arr);
        foreach ($request->get('image_name') as $key => $value){
            $picture = Picture::findOrFail($key);
            $old_picture_name = $picture->image_name;

            $picture->image_name = $value;
            $picture->description = $new_descriptions[$key];
            $picture->save();

            $imageManager->renameImage($old_picture_name, $picture, $brand->id);
        }

        return redirect()->route('admin.brands.editPhotos', $brand->id)->with('Success', "De afbeelding van het merk: ". $brand->name." zijn succesvol aangepast!");
    }
}
