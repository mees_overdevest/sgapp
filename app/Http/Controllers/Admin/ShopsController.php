<?php

namespace App\Http\Controllers\Admin;

use App\Models\Shop;
use App\Models\Picture;

use App\Models\shopCategory;
use Illuminate\Http\Request;
use App\Helpers\ImageManager;
use Illuminate\Support\Facades\Input;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class ShopsController extends Controller
{
    protected $type = "shop";

    public function index(){
        $shops = Shop::all();
        return view('admin.shops.index', compact('shops'));
    }

    public function show($id){
        $shop= Shop::findOrFail($id);
        return view('admin.shops.show', compact('shop'));
    }

    public function create(){
        $cats = shopCategory::whereNull('parent_id')->with('children')->get();

        $categories = [];
        foreach($cats as $cat){
            $cat->title = "Hoofdcategorie: " . $cat->title;
            $categories[] = array($cat->id => $cat->title);
            foreach($cat->children as $child){
                $child->title = "Subcategorie: " . $child->title;
                $categories[] = array($child->id => $child->title);
            }
        }

//        dd(auth()->user());
        // todo:
            // Retrieve brands
            // Retrieve ShopCategories
            // Retrieve Areas
        return view('admin.shops.create', compact('categories'));
    }

    public function edit($id){
        $shop = Shop::findOrFail($id);

        return view('admin.shops.edit', compact('shop'));
    }

    public function update(Request $request, $id){
        $shop = Shop::findOrFail($id);
        $shop->title = $request->get('title');
        $shop->description = $request->get('description');
        $shop->since = $request->get('since');
        $shop->city = $request->get('city');
        $shop->address = $request->get('address');
        $shop->update();

        return redirect()->route('admin.shops.index')->with('Success', 'De winkel ' . $shop->title . ' is succesvol bijgewerkt.');
    }

    public function store(Requests\Admin\Shop\CreateShopRequest $request){
        $shop = new Shop();
        $shop->title = $request->input('title');
        $shop->description = $request->input('description');
        $shop->since = $request->input('since');
        $shop->city = $request->input('city');
        $shop->address = $request->input('address');
        $shop->save();

        $shop->categories()->attach([$request->get('category_id')]);

        return redirect()->route('admin.shops.index')->with('Success', trans('shops/wizard.create.success', ['name' => $shop->name]));
    }

    public function destroy($id){
        $shop = Shop::findOrFail($id);
        $pictures = $shop->pictures()->get();
        $imageManager = new ImageManager();

        foreach ($pictures as $picture){
            $shop->pictures()->detach($picture->id);
            $imageManager->deleteImage($picture, $shop->id);
            $picture->delete();
        }

        $shop->delete();

        return redirect()->route('admin.shops.index')->with('Success', "De winkel ". $shop->title ." is succesvol verwijderd.");
    }

    public function addPhotos(Shop $shop){
        return view('admin.shops.addPhotos', compact('shop'));
    }

    public function addPhotosPost(Request $request, Shop $shop)
    {
        # Array with image ids
        $images = [];

        # Helper Class for Image Paths
        $uploader = new ImageManager();

        $i = 0;
        # loop over the sent images
        foreach($request->file('image') as $img){

            # Check if submitted image has the right info
            if($request->get('image_name')[$i] != "" && $request->get('image_description')[$i] && $img != null) {

                $image = new Picture([
                    'image_name' => $request->get('image_name')[$i],
                    'image_extension' => $img->getClientOriginalExtension(),
                    'description' => $request->get('image_description')[$i],
                ]);

                // assigning the image paths to the model
                $image->type = $this->type;

                // Save image to DB
                $image->save();

                // Getting the parts of the image we need
                $images[] = $image->id;

                $i++;
                // Query the imageuploader to put the images on in the right places
                $uploader->createImages($img, $image->image_name, $this->type, $shop->id);
            }
        };

        $shop->pictures()->attach($images);

        return redirect()->route('admin.shops.index')->with('Success', trans('shops/wizard.addPhotos.success', ['name' => $shop->name]));
    }

    public function deletePhoto(Shop $shop, $picture_id){
        $picture = Picture::findOrFail($picture_id);
        $imageManager = new ImageManager();

        $shop->pictures()->detach($picture->id);

        $imageManager->deleteImage($picture, $shop->id);

        $picture->delete();

        return redirect()->route('admin.shops.editPhotos', $shop->id)->with('Success', "De afbeelding is succesvol verwijderd van winkel: ". $shop->name."!");
    }

    public function editPhotos(Shop $shop){
        return view('admin.shops.editPhotos', compact('shop'));
    }

    public function editPhotosPost(Request $request, Shop $shop){

        $new_descriptions = $request->get('image_description');

        # Helper Class for Image Paths
        $imageManager = new ImageManager();

        foreach ($request->get('image_name') as $key => $value){
            $picture = Picture::findOrFail($key);
            $old_picture_name = $picture->image_name;

            $picture->image_name = $value;
            $picture->description = $new_descriptions[$key];
            $picture->save();

            $imageManager->renameImage($old_picture_name, $picture, $shop->id);
        }

        return redirect()->route('admin.shops.editPhotos', $shop->id)->with('Success', "De afbeelding van de winkel: ". $shop->name." zijn succesvol aangepast!");
    }
}
