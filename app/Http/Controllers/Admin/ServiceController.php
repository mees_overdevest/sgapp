<?php

namespace App\Http\Controllers\Admin;

use App\Models\Picture;
use App\Models\Service;
use App\Models\PCategory;
use Illuminate\Http\Request;

use App\Helpers\ImageManager;
use Illuminate\Support\Facades\Input;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class ServiceController extends Controller
{
    protected $type = "service";

    public function index(){
        $services = Service::paginate(30);
        return view('admin.services.index', compact('services'));
    }

    public function show($id){
        $service = Service::findOrFail($id);

        return view('admin.services.show', compact('service'));
    }

    public function create(){
        $cats = PCategory::whereNull('parent_id')->with('children')->get();

        $categories = [];
        foreach($cats as $cat){
            $cat->title = "Hoofdcategorie: " . $cat->title;
            $categories[] = array($cat->id => $cat->title);
            foreach($cat->children as $child){
                $child->title = "Subcategorie: " . $child->title;
                $categories[] = array($child->id => $child->title);
            }
        }
        return view('admin.services.create', compact('categories'));
    }

    public function edit($id){
        $service = Service::findOrFail($id);
        $cats = PCategory::whereNull('parent_id')->with('children')->get();

        $categories = [];
        foreach($cats as $cat){
            $cat->title = "Hoofdcategorie: " . $cat->title;
            $categories[] = array($cat->id => $cat->title);
            foreach($cat->children as $child){
                $child->title = "Subcategorie: " . $child->title;
                $categories[] = array($child->id => $child->title);
            }
        }
        return view('admin.services.edit', compact('service', 'categories'));
    }

    public function update(Request $request, $id){
        $service = Service::findOrFail($id);
        $service->title = $request->get('title');
        $service->description = $request->get('description');
        $service->update();

        $categories = [];
        foreach($request->input('category_id') as $key => $value){
            $categories[] = $value;
        }
        $service->categories()->sync($categories);

        return redirect()->route('admin.services.index')->with('Success', trans('services/service.update.success', ['name' => $service->title]));
    }

    public function store(Requests\Admin\Service\CreateRequest $request){
        $service = Service::create($request->all());
        $service->save();

        $categories = [];
        foreach($request->input('category_id') as $key => $value){
            $categories[] = $value;
        }
        $service->categories()->attach($categories);

        return redirect()->route('admin.services.index')->with('Success', trans('services/service.create.success', ['name' => $service->title]));
    }


    public function destroy($id){
        $service = Service::findOrFail($id);
        $pictures = $service->pictures()->get();
        $imageManager = new ImageManager();

        foreach ($pictures as $picture){
            $service->pictures()->detach($picture->id);
            $imageManager->deleteImage($picture, $service->id);
            $picture->delete();
        }

        $service->delete();

        return redirect()->route('admin.services.index')->with('Success', "De Service ". $service->title ." is succesvol verwijderd.");
    }

    public function addPhotos(Service $service){
        return view('admin.services.addPhotos', compact('service'));
    }

    public function addPhotosPost(Request $request, Service $service)
    {
        # Array with image ids
        $images = [];

        # Helper Class for Image Paths
        $uploader = new ImageManager();

        $i = 0;
        # loop over the sent images
        foreach($request->file('image') as $img){

            # Check if submitted image has the right info
            if($request->get('image_name')[$i] != "" && $request->get('image_description')[$i] && $img != null) {

                $image = new Picture([
                    'image_name' => $request->get('image_name')[$i],
                    'image_extension' => $img->getClientOriginalExtension(),
                    'description' => $request->get('image_description')[$i],
                ]);

                // assigning the image paths to the model
                $image->type = $this->type;

                // Save image to DB
                $image->save();

                // Getting the parts of the image we need
                $images[] = $image->id;

                $i++;
                // Query the imageuploader to put the images on in the right places
                $uploader->createImages($img, $image->image_name, $this->type, $service->id);
            }
        };

        $service->pictures()->attach($images);

        return redirect()->route('admin.services.index')->with('Success', trans('services/service.addPhotos.success', ['name' => $service->title]));
    }


    public function deletePhoto(Service $service, $picture_id){
        $picture = Picture::findOrFail($picture_id);
        $imageManager = new ImageManager();

        $service->pictures()->detach($picture->id);

        $imageManager->deleteImage($picture, $service->id);

        $picture->delete();

        return redirect()->route('admin.services.editPhotos', $service->id)->with('Success', "De afbeelding is succesvol verwijderd van de Service: ". $service->title."!");
    }

    public function editPhotos(Service $service){
        return view('admin.services.editPhotos', compact('service'));
    }

    public function editPhotosPost(Request $request, Service $service){

        $new_descriptions = $request->get('image_description');

        # Helper Class for Image Paths
        $imageManager = new ImageManager();
//        dd($arr);
        foreach ($request->get('image_name') as $key => $value){
            $picture = Picture::findOrFail($key);
            $old_picture_name = $picture->image_name;

            $picture->image_name = $value;
            $picture->description = $new_descriptions[$key];
            $picture->save();

            $imageManager->renameImage($old_picture_name, $picture, $service->id);
        }

        return redirect()->route('admin.services.editPhotos', $service->id)->with('Success', "De afbeelding van de Service: ". $service->title." zijn succesvol aangepast!");
    }
}
