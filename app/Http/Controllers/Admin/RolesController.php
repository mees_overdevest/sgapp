<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Role;
use App\Permission;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class RolesController extends Controller
{
    public function index(){
        $roles = Role::all();

        return view('roles.index', compact('roles'));
    }

    public function rolesCreate(){
        return view('roles.create');
    }

    public function permissionsCreate(){
        $roles = Role::lists('label','id');

        return view('permissions.create', compact('roles'));
    }

    public function permissionPost(Requests\Admin\Permission\CreatePermissionRequest $request){
//        dd($request);

        $permission = new Permission();
        $permission->name = $request->get('name');
        $permission->label = $request->get('label');
        $permission->save();

        $role = Role::findOrFail($request->get('parent'));
        $role->givePermissionTo($permission);
        $role->save();

        return redirect()->route('admin.roles.index')->with('Success', trans('roles/permission.create.success', ['name' => $permission->name]));
    }

    public function rolePost(Requests\Admin\Role\CreateRoleRequest $request){
//        dd($request);
        $role = new Role();
        $role->name = $request->get('name');
        $role->label = $request->get('label');
        $role->save();

        return redirect()->route('admin.roles.index')->with('Success', trans('roles/role.create.success', ['name' => $role->name]));
    }
}
