<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\PCategory;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class PCategoryController extends Controller
{
    public function index(){
        $PCategories = PCategory::whereNull('parent_id')->with('children')->get();

        return view('admin.PCategories.index', compact('PCategories'));
    }

    public function show($id){
        $PCategory = PCategory::findOrFail($id);

        return view('admin.PCategories.show', compact('PCategory'));
    }

    public function create(){
        $PCategories = PCategory::whereNull('parent_id')->with('children')->lists('title', 'id');
        return view('admin.PCategories.create', compact('PCategories'));
    }

    public function store(Request $request){
        $PCategory = new PCategory();

        $PCategory->title = $request->input('title');
        $PCategory->description = $request->input('description');

        if($request->input('subcat') == null){

        } else {
            $PCategory->parent_id = $request->input('parent_category');
        }

        $PCategory->save();

        return redirect()->route('admin.PCategories.index')->with('Success', trans('PCategories/PCategory.create.success',['name' => $PCategory->title]));
    }

    public function edit($id){
        $PCategory = PCategory::findOrFail($id);
        $PCategories = PCategory::whereNull('parent_id')->with('children')->lists('title', 'id');

        return view('admin.PCategories.edit', compact('PCategory', 'PCategories'));
    }

    public function update(Request $request, $id){
        $PCategory = PCategory::findOrFail($id);
        $PCategory->title = $request->input('title');
        $PCategory->description = $request->input('description');

        if($request->input('headcat') == null){
            $PCategory->parent_id = $request->input('parent_category');
        } else {
            $PCategory->parent_id = null;
        }

        $PCategory->save();

        return redirect()->route('admin.PCategories.index')->with('Success', trans('PCategories/PCategory.update.success',['name' => $PCategory->title]));
    }
}
