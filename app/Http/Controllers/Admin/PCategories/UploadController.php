<?php

namespace App\Http\Controllers\Admin\PCategories;

use App\Models\PCategory;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;

class UploadController extends Controller
{
    public function uploadExcel(Request $request){
        Excel::load($request->file('excel'), function($reader){
            $categories = [];

            $reader->each(function($row) use (&$categories){
                if($row->parent == null){

                    $categories[$row->id] =
                        [
                            'id' => (int)$row->id,
                            'title' => $row->category,
                            'description' => $row->description,
                            'parent_id' => null
                        ];
                } else{
                    if(array_key_exists((int)$row->parent, $categories)){

                        $categories[$row->parent]['children'][$row->id] =
                            [
                                'id' => (int)$row->id,
                                'title' => $row->category,
                                'description' => $row->description,
                                'parent_id' => (int)$row->parent
                            ];
                    } elseif(!array_key_exists((int)$row->parent, $categories)){
                        foreach ($categories as $cat){
                            if(array_key_exists((int)$row->parent, $cat['children'])){
                                foreach ($cat['children'] as $child){
                                    if($child['id'] == (int)$row->parent){

                                        $categories[$cat['id']]['children'][$child['id']]['children'][$row->id] =
                                            [
                                                'id' => (int)$row->id,
                                                'title' => $row->category,
                                                'description' => $row->description,
                                                'parent_id' => (int)$row->parent
                                            ];
                                    }
                                }
                            }
                        }
                    }
                }
            });

            foreach ($categories as $category){
                $newCat = new PCategory();
                $newCat->title = $category['title'];
                $newCat->description = $category['description'];
                $newCat->save();

                if(isset($category['children']) && count($category['children']) > 0){
                    foreach ($category['children'] as $child){
                        $newChild = new PCategory();
                        $newChild->title = $child['title'];
                        $newChild->description = $child['description'];
                        $newChild->parent_id = $newCat->id;
                        $newChild->save();

                        if(isset($child['children']) && count($child['children']) > 0) {
                            foreach ($child['children'] as $subChild) {
                                $newSubChild = new PCategory();
                                $newSubChild->title = $subChild['title'];
                                $newSubChild->description = $subChild['description'];
                                $newSubChild->parent_id = $newChild->id;
                                $newSubChild->save();

                            }
                        }
                    }
                }
            }
        });
    }
}
