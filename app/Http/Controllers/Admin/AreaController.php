<?php

namespace App\Http\Controllers\Admin;

use App\Models\Area;
use App\Models\Coordinate;
use App\Models\Picture;
use Illuminate\Http\Request;

use App\Helpers\CoordinateHelper;
use App\Helpers\ImageManager;
use Illuminate\Support\Facades\Input;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class AreaController extends Controller
{
    protected $type = "area";

    public function index(){
        $areas = Area::all();
        return view('admin.areas.index', compact('areas'));
    }

    public function show($id){
        $area = Area::findOrFail($id);

        return view('admin.areas.show', compact('area'));
    }

    public function create(){
        return view('admin.areas.create');
    }

    public function edit($id){
        $area = Area::findOrFail($id);

        return view('admin.areas.edit', compact('area'));
    }

    public function update(Request $request, $id){
        $area = Area::findOrFail($id);
        $area->title = $request->get('title');
        $area->description = $request->get('description');
        $area->update();

        return redirect()->route('admin.areas.index')->with('Success', 'Het Winkelgebied ' . $area->title . ' is succesvol bijgewerkt.');
    }

    public function store(Request $request){
        $coordinates = new CoordinateHelper();
        $coordinates = $coordinates->extractCoordinates($request->get('coordinates'));

        $area = new Area();
        $area->title = $request->get('title');
        $area->city = $request->get('city');
        $area->address = $request->get('address');
        $area->save();

        $order = 0;
        foreach($coordinates as $coordinate){
            $newCoordinate = new Coordinate();
            $newCoordinate->lat = $coordinate["lat"];
            $newCoordinate->lng = $coordinate["lng"];

            $order += 1;

            $newCoordinate->save();

            $newCoordinate->area()->attach([$area->id => ['order'=>$order]]);
        }

        return redirect()->route('admin.areas.index')->with('Success', "yes we did it");
    }

    public function destroy($id){
        $area = Area::findOrFail($id);
        $pictures = $area->pictures()->get();
        $imageManager = new ImageManager();

        foreach ($pictures as $picture){
            $area->pictures()->detach($picture->id);
            $imageManager->deleteImage($picture, $area->id);
            $picture->delete();
        }

        foreach($area->coordinates as $coordinate){
            $coordinate->area()->detach($area->id);
            $coordinate->delete();
        }

        $area->delete();

        return redirect()->route('admin.areas.index')->with('Success', trans('areas/area.deleted', ['name' => $area->title]));
    }

    public function deletePhoto(Area $area, $picture_id){
        $picture = Picture::findOrFail($picture_id);
        $imageManager = new ImageManager();

        $area->pictures()->detach($picture->id);

        $imageManager->deleteImage($picture, $area->id);

        $picture->delete();

        return redirect()->route('admin.areas.editPhotos', $area->id)->with('Success', "De afbeelding is succesvol verwijderd van het gebied: ". $area->title."!");
    }

    public function addPhotos(Area $area){
        return view('admin.areas.addPhotos', compact('area'));
    }

    public function addPhotosPost(Request $request, Area $area)
    {
        # Array with image ids
        $images = [];

        # Helper Class for Image Paths
        $uploader = new ImageManager();

        $i = 0;
        # loop over the sent images
        foreach($request->file('image') as $img){

            # Check if submitted image has the right info
            if($request->get('image_name')[$i] != "" && $request->get('image_description')[$i] && $img != null) {

                $image = new Picture([
                    'image_name' => $request->get('image_name')[$i],
                    'image_extension' => $img->getClientOriginalExtension(),
                    'description' => $request->get('image_description')[$i],
                ]);

                // assigning the image paths to the model
                $image->type = $this->type;

                // Save image to DB
                $image->save();

                // Getting the parts of the image we need
                $images[] = $image->id;

                $i++;
                // Query the imageuploader to put the images on in the right places
                $uploader->createImages($img, $image->image_name, $this->type, $area->id);
            }
        };

        $area->pictures()->attach($images);

        return redirect()->route('admin.areas.index')->with('Success', trans('areas/area.addPhotos.success', ['name' => $area->title]));
    }

    public function editPhotos(Area $area){
        return view('admin.areas.editPhotos', compact('area'));
    }

    public function editPhotosPost(Request $request, Area $area){

        $new_descriptions = $request->get('image_description');

        # Helper Class for Image Paths
        $imageManager = new ImageManager();
//        dd($arr);
        foreach ($request->get('image_name') as $key => $value){
            $picture = Picture::findOrFail($key);
            $old_picture_name = $picture->image_name;

            $picture->image_name = $value;
            $picture->description = $new_descriptions[$key];
            $picture->save();

            $imageManager->renameImage($old_picture_name, $picture, $area->id);
        }

        return redirect()->route('admin.areas.editPhotos', $area->id)->with('Success', "De afbeelding van het gebied: ". $area->title." zijn succesvol aangepast!");
    }

    
}
