<?php

namespace App\Http\Controllers\Admin;

use App\Models\Account\Language;
use App\Models\Account\User;
use App\Models\Role;
use App\Models\Shop;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Helpers\Generators;
use Illuminate\Support\Facades\Mail;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class UsersController extends Controller
{
    public function index() {
        $title = trans('general.actions.index') . " " . trans('admin/users.page.namePlural');
        $users = User::all();

        return view('admin.users.index', compact('title', 'users'));
    }

    public function create() {
        $title = trans('general.actions.create') . " " . trans('admin/users.page.name');
        $languages  = Language::lists('name', 'id');
        $roles  = Role::lists('name', 'id');

        return view('admin.users.create', compact('title', 'languages','roles'));
    }

    public function store(\App\Http\Requests\CreateUserRequest $request) {
        $data = $request->all();
        $user = User::create($data);

        $this->SendPasswordCode($user->id, 1);

        return redirect::route('admin.users.index')->with("Success", trans('admin/users.messages.userCreated'));
    }

    public function show($id) {
        $title = trans('general.actions.show') . " " . trans('admin/users.page.namePlural');
        $user = User::findOrFail($id);

        return view('admin.users.show', compact('title', 'user'));
    }

    public function edit($id) {
        $title = trans('general.actions.edit') . " " . trans('admin/users.page.name');
        $languages  = Language::lists('name', 'id');
        $user = User::findOrFail($id);
        $roles = Role::all();
        $shops  = Shop::lists('name', 'id');
//        $roles = $user->roles()->lists('name','id');
//        dd($id);
        $states = [
            '0'     => ucfirst(trans('admin/users.form.inactive')),
            '1'     => ucfirst(trans('admin/users.form.active'))
        ];

        return view('admin.users.edit', compact('title', 'shops', 'user', 'roles', 'languages', 'states'));
    }

    public function update(Request $request, $id) {
        $user = User::findOrFail($id);
        $user->update($request->all());
        $syncRoles = [];
        foreach($request->get('role') as $key => $value){
            $syncRoles[] = [$user->id => $value];
        }

        $user->roles()->sync($syncRoles[0]);
        $user->shops()->sync([$request->get('shop_id')]);

        return redirect()->route('admin.users.index', $id)->with("Success", trans('admin/users.messages.userUpdated',['name' => $user->fullName()]));
    }

    public function destroy(User $user) {
        $user->delete();

        return redirect::route('admin.users.index')->with("Success", trans('admin/users.messages.userDeleted'));
    }

    public function userRequestChangePassword() {
        $this->SendPasswordCode(Auth::user()->id, 2);

        return redirect::route('admin.users.index')->with("Success", trans('admin/users.messages.passwordRestMailSend'));
    }

    public function adminRequestChangePassword($user_id) {
        $this->SendPasswordCode($user_id, 3);

        return redirect::route('admin.users.index')->with("Success", trans('admin/users.messages.passwordRestMailSend'));
    }

    private function SendPasswordCode($user_id, $type) {
        if($type == 1) {
            $emailTemplate = 'emails.auth.users.newAccount';
            $subject = trans('passwordMail.subjectNewAccount');
        } elseif($type == 2) {
            $emailTemplate = 'emails.auth.users.userRequestChangePassword';
            $subject = "Wachtwoord resetten";
        } elseif ($type == 3) {
            $emailTemplate = 'emails.auth.users.adminRequestChangePassword';
            $subject = "Wachtwoord resetten";
        } else {
            throw new Exception('Type must be betweeen 1 and 3.');
        }

        $code = Generators::uniqueCode();
        $user = User::findOrFail($user_id);
        $user->code = $code;
        $user->save();

        $data = [
            'name'  => $user->fullname(),
            'code'  => $code,
            'mail' => $user->email
        ];

        Mail::send($emailTemplate, $data, function($message) use ($data, $subject)
        {
            $message->from(config('site.auth.no-reply.mail'), config('site.auth.no-reply.name'));
            $message->to($data['mail'])->subject($subject);
        });
    }
}
