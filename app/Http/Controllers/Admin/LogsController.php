<?php

namespace App\Http\Controllers\Admin;

use App\Models\Datalog;
use Illuminate\Http\Request;
use App\Models\Shop;
use App\Models\Brand;
use App\Models\Service;
use App\Models\Product;
use App\Models\Area;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class LogsController extends Controller
{
    public function index(){
        $title = 'All';
        $logs = Datalog::all();

        return view('admin.logs.index', compact('logs', 'title'));
    }

    public function typeIndex($type){
        $title = ucfirst($type);

        $className = 'App\\Models\\' . $title;

        $logs = $className::all();

        return view('admin.logs.typeLog', compact('logs', 'title', 'type'));
    }

    public function objectTypeIndex($type, $id){
        $className = 'App\\Models\\' . ucfirst($type);
        $object = $className::findOrFail($id);
        $logs = $object->getLog();

        return view('admin.logs.objectLog', compact('object', 'logs'));
    }
}
