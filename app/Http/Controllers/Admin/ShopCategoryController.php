<?php

namespace App\Http\Controllers\Admin;

use App\Models\shopCategory;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ShopCategoryController extends Controller
{
    public function index(){
        $shopCategories = shopCategory::whereNull('parent_id')->with('children')->get();

        return view('admin.shopCategories.index', compact('shopCategories'));
    }

    public function show($id){
        $shopCategory = shopCategory::findOrFail($id);

        return view('admin.shopCategories.show', compact('shopCategory'));
    }

    public function create(){
        $shopCategories = shopCategory::whereNull('parent_id')->with('children')->lists('title', 'id');
        return view('admin.shopCategories.create',compact('shopCategories'));
    }

    public function store(Requests\Admin\Shop\CreateCategoryRequest $request){
        $shopCategory = new shopCategory();

        $shopCategory->title = $request->input('title');
        $shopCategory->description = $request->input('description');

        if($request->input('subcat') == null){

        } else {
            $shopCategory->parent_id = $request->input('parent_category');
        }

        $shopCategory->save();

        return redirect()->route('admin.shopCategory.index')->with('Success', trans('shops/category.create.success',['name' => $shopCategory->title]));
    }

    public function edit($id){
        $shopCategory = shopCategory::findOrFail($id);
        $shopCategories = shopCategory::whereNull('parent_id')->with('children')->lists('title', 'id');

        return view('admin.shopCategories.edit', compact('shopCategory', 'shopCategories'));
    }

    public function update(Request $request, $id){
        $shopCategory = shopCategory::findOrFail($id);
        $shopCategory->title = $request->input('title');
        $shopCategory->description = $request->input('description');

        if($request->input('parent_category') != $id){
            $shopCategory->parent_id = $request->input('parent_category');
        } else {
            $shopCategory->parent_id = null;
        }

        $shopCategory->update();

        return redirect()->route('admin.shopCategory.index')->with('Success', trans('shops/category.update.success',['name' => $shopCategory->title]));
    }
}
