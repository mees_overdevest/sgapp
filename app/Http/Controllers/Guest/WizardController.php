<?php

namespace App\Http\Controllers\Guest;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class WizardController extends Controller
{
    public function intro(){
        return view('front.wizards.intro.wizard');
    }
}
