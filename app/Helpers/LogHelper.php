<?php namespace App\Helpers;

use App\Models\Area;
use App\Models\Shop;
use App\Models\Product;
use App\Models\Service;
use App\Models\Brand;
use App\Models\Datalog;

class LogHelper{

    /**
     *
     * Convert an object to an array
     *
     * @type  string    $type           The object type of the log
     * @user  object    $user           The user object of the log
     * @type_object     object||string  $type_object This can be an object that can be visited
     *                                  or is a string that points to an ID in the site config in app/config/site.php
     *
     * @return      void
     *
     */
    public function makeLog($type, $user, $type_object)
    {
        $log = new Datalog();

        $pages = config('site.log_types');

        $log->type = $type;
        $log->user_id = $user->id;

        if(is_string($type_object)){
            if(in_array($type_object,$pages)){
                $log->type_id = array_search($type_object,$pages);
            }
        } else {
            $log->type_id = $type_object->id;
        }

        $log->save();
    }

    /**
     *
     * Convert an object to an array
     *
     * @id    integer   $id The object id to retrieve logs from
     * @type  string    $type The object type of the log
     * @return      Collection
     *
     */
    public static function getObjectLog($id, $type){
        return Datalog::where('type_id', $id)->where('type', $type)->get();
    }

}