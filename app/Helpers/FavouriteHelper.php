<?php namespace App\Helpers;

use App\Models\Favourite;

class FavouriteHelper{

    // Create a favourite
    public function AddFavourite($type, $object){
        $favourite = new Favourite();

        $favourite->type = $type;
        $favourite->user_id = auth()->user()->id;
        $favourite->object_id = $object->id;

        if($favourite->save()){
            return true;
        } else{
            return false;
        }
    }

    // Delete a favourite
    // $id is the object_id of the favourite to delete
    public function DeleteFavourite($id){
        if(Favourite::where( 'object_id', $id )->where( 'user_id', auth()->user()->id )->delete()){
            return true;
        } else {
            return false;
        }
    }

    // Set notification interval on how many times to receive info about saved Favourite
    public function SetNotificationInterval($id, $interval){
        $favourite = Favourite::findOrFail($id);

        $favourite->notifications = $interval;

        if($favourite->save()){
            return true;
        } else{
            return false;
        }
    }

    public static function isFavourite($type, $object){
        $favourite = Favourite::where('object_id', $object->id)
            ->where('user_id', auth()->user()->id)
            ->where('type', $type)->count();

        if($favourite > 0){
            return true;
        } else {
            return false;
        }
    }

    public function getFavouritesPerType($user){
        $favourites = [];
        foreach(config('site.types') as $type){
            $favourites[$type] = Favourite::where('type', $type)->where('user_id', $user->id)->get();
        }

        return $favourites;
    }

}