<?php namespace App\Helpers;

use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Input;

class ImageManager
{
    public static function createImages($file, $imageName, $type, $id){
//        dd(self::getImageDir($type)['base']);
        if(!File::exists(public_path(self::getImageDir($type)['base']))){
//            mkdir('public' . self::getImageDir($type)['base'], 0777);
//            dd(public_path());

            File::makeDirectory(public_path().self::getImageDir($type)['base'], $mode = 0777, true, true);
            File::makeDirectory(public_path().self::getImageDir($type)['destinationFolder'], $mode = 0777, true, true);
            File::makeDirectory(public_path().self::getImageDir($type)['destinationThumbnail'], $mode = 0777, true, true);
            File::makeDirectory(public_path().self::getImageDir($type)['destinationMobile'], $mode = 0777, true, true);
//            mkdir(self::removeLastChar($this->getImageDir($type)['base']));
//            mkdir(self::removeLastChar($this->getImageDir($type)['destinationFolder']));
//            mkdir(self::removeLastChar($this->getImageDir($type)['destinationThumbnail']));
//            mkdir(self::removeLastChar($this->getImageDir($type)['destinationMobile']));
        }

        if(!File::exists(public_path().self::getImageDir($type)['destinationFolder'] . $id . '/')){
            File::makeDirectory(public_path().self::getImageDir($type)['base'], $mode = 0777, true, true);
            File::makeDirectory(public_path().self::getImageDir($type)['destinationFolder']. $id . '/', $mode = 0777, true, true);
            File::makeDirectory(public_path().self::getImageDir($type)['destinationThumbnail']. $id . '/', $mode = 0777, true, true);
            File::makeDirectory(public_path().self::getImageDir($type)['destinationMobile']. $id . '/', $mode = 0777, true, true);
        }

        $extension = $file->getClientOriginalExtension();

        // Put '_' in the whitespace
       $imageName = str_replace(' ', '_', $imageName);

        //Original image
        Image::make($file->getRealPath())->save(public_path().self::getImageDir($type)['destinationFolder'] . $id . '/'. $imageName . '.'.$extension);

        // Medium image
        $medium = Image::make($file);
        $medium->resize(800, null, function ($constraint) {
            $constraint->aspectRatio();
        });
        $medium->save(public_path().self::getImageDir($type)['destinationMobile'] .  $id . '/'. $imageName . '.'.$extension);

        // Thumbnail image
        $thumb = Image::make($file);
        $thumb->resize(350, null, function ($constraint) {
            $constraint->aspectRatio();
        });
        $thumb->save(public_path().self::getImageDir($type)['destinationThumbnail'] .  $id . '/'. $imageName . '.'.$extension);

        return true;
    }

    // A function to delete a single image
    public static function deleteImage($picture, $owner_id){
        $imageName = str_replace(' ', '_', $picture->image_name);

        unlink(public_path().self::getImageDir($picture->type)['destinationFolder'] . $owner_id . '/'. $imageName . '.'.$picture->image_extension);
        unlink(public_path().self::getImageDir($picture->type)['destinationMobile'] .  $owner_id . '/'. $imageName . '.'.$picture->image_extension);
        unlink(public_path().self::getImageDir($picture->type)['destinationThumbnail'] .  $owner_id . '/'. $imageName . '.'.$picture->image_extension);

        if(count(glob(public_path().self::getImageDir($picture->type)['destinationFolder'] . $owner_id ."/*")) === 0){
            rmdir(public_path().self::getImageDir($picture->type)['destinationFolder']. $owner_id . '/');
            rmdir(public_path().self::getImageDir($picture->type)['destinationMobile']. $owner_id . '/');
            rmdir(public_path().self::getImageDir($picture->type)['destinationThumbnail']. $owner_id . '/');
        }

    }

    public static function renameImage($old_picture_name, $new_picture, $owner_id){
        // Put '_' in the whitespace
        $newName = str_replace(' ', '_', $new_picture->image_name);

        rename(public_path().self::getImageDir($new_picture->type)['destinationFolder'] . $owner_id . '/'. $old_picture_name . '.'.$new_picture->image_extension,public_path().self::getImageDir($new_picture->type)['destinationFolder'] . $owner_id . '/'. $newName . '.'.$new_picture->image_extension);
        rename(public_path().self::getImageDir($new_picture->type)['destinationMobile'] . $owner_id . '/'. $old_picture_name . '.'.$new_picture->image_extension,public_path().self::getImageDir($new_picture->type)['destinationMobile'] . $owner_id . '/'. $newName . '.'.$new_picture->image_extension);
        rename(public_path().self::getImageDir($new_picture->type)['destinationThumbnail'] . $owner_id . '/'. $old_picture_name . '.'.$new_picture->image_extension,public_path().self::getImageDir($new_picture->type)['destinationThumbnail'] . $owner_id . '/'. $newName . '.'.$new_picture->image_extension);

    }

    public static function getImageDir($type)
    {
        switch ($type) {
            case "area":
                return [
                    'base' => '/img/area/',
                    'destinationFolder' => '/img/area/original/',
                    'destinationThumbnail' => '/img/area/thumbnails/',
                    'destinationMobile' => '/img/area/mobile/',
                ];
            case "shop":
                return [
                    'base' => '/img/shop/',
                    'destinationFolder' => '/img/shop/original/',
                    'destinationThumbnail' => '/img/shop/thumbnails/',
                    'destinationMobile' => '/img/shop/mobile/',
                ];
            case "product":
                return [
                    'base' => '/img/product/',
                    'destinationFolder' => '/img/product/original/',
                    'destinationThumbnail' => '/img/product/thumbnails/',
                    'destinationMobile' => '/img/product/mobile/',
                ];
            case "service":
                return [
                    'base' => '/img/service/',
                    'destinationFolder' => '/img/service/original/',
                    'destinationThumbnail' => '/img/service/thumbnails/',
                    'destinationMobile' => '/img/service/mobile/',
                ];
            case "brand":
                return [
                    'base' => '/img/brand/',
                    'destinationFolder' => '/img/brand/original/',
                    'destinationThumbnail' => '/img/brand/thumbnails/',
                    'destinationMobile' => '/img/brand/mobile/',
                ];
        }

    }

    private static function removeLastChar($str) {
        return rtrim($str, "/");
    }

    public static function getMobilePath($id, $image){

        $imageName = str_replace(' ', '_', $image->image_name);
//echo str_replace(' ', '_', $imageName);
        $dir = self::getImageDir($image->type)['destinationMobile'] . $id . '/' . $imageName . '.' . $image->image_extension;

        return $dir;
    }

}
