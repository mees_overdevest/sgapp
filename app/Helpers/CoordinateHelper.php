<?php namespace App\Helpers;

class CoordinateHelper {

    // function that extract coordinates form string
    public function extractCoordinates($coordinateString){
        $i = 0;
        $coordinateCount = 0;
        $array = [];

        $coordinateString = explode(",",$coordinateString);

        foreach($coordinateString as $coordinate){
//            dd($coordinate);
            $i += 1;
            switch (self::isEven($i)){
                case true:
                    $coordinate = explode(")",$coordinate);
                    $coordinate = explode(" ",$coordinate[0]);
//                    dd($coordinate);
                    $array[$coordinateCount]['lng'] = $coordinate[1];

                    $coordinateCount += 1;
                    break;
                case false:
                    $coordinate = explode("(",$coordinate);
//                    $coordinate = explode(" ",$coordinate[0]);

                    $array[$coordinateCount]['lat'] = $coordinate[1];
                break;
            };
        }

        return $array;
    }

    public static function isEven($number){
        return $number % 2 == 0;
    }
}