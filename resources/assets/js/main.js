/**
 * Created by mees on 22-09-16.
 */
// var Vue = require('vue');
import Vue from 'vue'

import ShopOwnerView from './components/shop/OwnerView.vue'

import VueResource from 'vue-resource'
Vue.use(VueResource)

Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#token').getAttribute('content');

new Vue({
    el: 'body',

    components: {
        ShopOwnerView
    }

})