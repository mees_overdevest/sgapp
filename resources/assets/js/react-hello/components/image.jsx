
import React, { PropTypes } from 'react'

const Image = ({image, extension, imgException = false}) => {
    let url;
    let	img;

    if(imgException) {
        img = <img src={url} className="quickscan-img-exception" />
    } else {
        img = <img src={url} />
    }

    return (
        url ? (img) : <span />
    )
}

export default Image
