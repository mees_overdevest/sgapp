import React, { PropTypes } from 'react'

const Button = ({color, text, onClick}) => {
    let cls = ['btn'];
    
    if (color !== undefined) {
        cls.push('btn-' + color);
    }

    cls = cls.join(' ');

    return (<button
        className={cls}
        onClick={e => {
            e.preventDefault()
            onClick()
        }}
    >
        {text}
    </button>)
}

export default Button
