import React, { PropTypes } from 'react'

const QuestionWrapper = ({title, children, onClick, btnText, options, subTitle = false}) => {
    let subTitleText            = undefined;

    const btn = (<button
        className="footer__link footer__link--main"
        onClick={e => {
            e.preventDefault()
            onClick()
        }}
    >
        {btnText}
    </button>);


    if(subTitle === true){
        subTitleText = (<p className="quickscan-question-subtitle"><i>U kunt meerdere antwoorden kiezen.</i></p>)
    }

    return (
        <div className="quickscan-question-wrapper cf">
            <div className="quickscan-question">
                <h3 className="quickscan-question-title">{title}</h3>
                {(subTitle === true) ? (subTitleText) : []}
                {children}
            </div>
        </div>
    )
}

export default QuestionWrapper
