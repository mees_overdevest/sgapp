import React from 'react'
import {Icon} from 'react-fa'

const Loader = () => {

  return (
    <Icon spin name="spinner" size="3x" />
  )
}

export default Loader