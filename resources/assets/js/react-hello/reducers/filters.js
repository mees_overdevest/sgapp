
import { SET_FILTER_VALUE } from '../action_types/filters'

function filter_values(state = {}, action) {
    switch (action.type) {
        case SET_FILTER_VALUE:
            return Object.assign({}, state, {[action.name]: action.value})
        default:
            return state;
    }
}

export { filter_values };
