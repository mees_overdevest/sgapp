
import { FIRST } from '../services/pages'

import { SET_PAGE } from '../action_types/page'

function page(state = FIRST, action) {
    switch (action.type) {
        case SET_PAGE:
            return action.page;
        default:
            return state;
    }
}

export { page };
