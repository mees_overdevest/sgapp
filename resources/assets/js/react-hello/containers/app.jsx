import { connect } from 'react-redux'
import React, { PropTypes } from 'react'

import { get_page } from '../services/pages'

let App = ({page}) => (
    <div>
        {React.createElement(get_page(page))}
    </div>
)

const mapStateToProps = (state) => {
    return {
        page: state.page
    }
}

App = connect(
    mapStateToProps
)(App)

export default App