import { connect } from 'react-redux'
import React, { PropTypes } from 'react'

import PageButton from '../../containers/page_button'
import { get_categories } from '../../action_creators/content'
import { set_filter_value } from '../../action_creators/filters'
import Loader from '../../components/loading'
import CategoryButton from '../category_button'

import { FIRST, THIRD } from '../../services/pages'

let SecondPage = ({getCategories, gender_filled, loading, categories, name = "categories", chosen_values}) => {
    let cats = [];
    // Call API
    if(gender_filled !== undefined && categories === undefined || Object.keys(categories).length < 1){
        getCategories()        
    }

    // Serialize categories
    if(categories !== undefined) {
        for(var key in categories){
            cats[key] = categories[key];
        }
    }

    return (<div className="quickscan-inner">
        {loading ? (<Loader />) : ''}
        <div className="quickscan-block quickscan--intro">
            <div className="quickscan-block-inner">
                <p>TWEjjjjjjEDE</p>
                <p>De quickscan geeft u inzicht in uw vertrekpunt voor traceerbaarheid. Door het invullen van alle onderdelen van de quickscan krijgt u op hoofdlijnen een beeld van de mogelijke besparingen, welke processen en afdelingen betrokken zijn en welke stappen nog ondernomen moeten worden om traceerbaarheid te realiseren.</p>
                <p>De onderdelen leiden u door: de status van patiëntveiligheid, de inrichting van de logistieke processen, de status van barcodering van de producten en de software die u in huis heeft. Het resultaat is een samenvattend rapport dat u een handvat biedt om aan de slag te gaan!</p>
            </div>
            <div>
            {categories !== undefined ? (cats.map(function(c){
                let childs = [];
                // Map Childs
                c.childs.map(function(k){
                    childs.push(
                        <CategoryButton key={k.id} name={name} value={k.id} text={'Title: ' + k.title + ' Bescrijving: ' + k.description} chosen_values={chosen_values} />
                        )
                });
                
                return(
                    <div key={c.id} className='bg-primary'>
                    <h2>Hoofdcategorie: {c.title}</h2>
                    <p>Beschrijving: {c.description}</p>
                    <CategoryButton key={c.id} name={name} value={c.id} text={'Title: ' + c.title + ' Bescrijving: ' + c.description} chosen_values={chosen_values} />
              
                    <div>
                    <h2>Subcategorieën</h2>
                    {childs}
                    
                    </div>
                </div>)
            })) : '' }
            </div>
        </div>
    </div>)    
    
}

const mapStateToProps = (state) => {
    return {
        gender_filled: state.filter_values['gender'],
        loading: state.loading,
        categories: state.content.categories,
        chosen_values: (state.filter_values.categories !== undefined ? state.filter_values.categories : [])
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        getCategories: () => dispatch(get_categories())
    }
}

SecondPage = connect(
    mapStateToProps,
    mapDispatchToProps
)(SecondPage)

SecondPage.nextButton = (state) => {
    if (state.filter_values['categories'] !== undefined && state.filter_values['categories'].length !== 0) {
        return (<PageButton color="blue" direction="next" page={THIRD} text="Verder" />)
    } else {
        return false
    }
}

SecondPage.prevButton = (state) => {
    return (<PageButton color="blue" direction="next" page={FIRST} text="Terug" />)
}

export default SecondPage
