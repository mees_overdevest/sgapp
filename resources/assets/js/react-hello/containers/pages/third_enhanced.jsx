import { connect } from 'react-redux'
import React, { PropTypes } from 'react'

import PageButton from '../../containers/page_button'

import { SECOND } from '../../services/pages'

import RadiosQuestion from '../radios_question'
import * as filters from '../../services/filters'

import { withGoogleMap,  GoogleMap, Marker, SearchBox } from 'react-google-maps'



const SimpleMapExampleGoogleMap = withGoogleMap(props => (
  <GoogleMap
    ref={props.onMapMounted}
    defaultZoom={8}
    defaultCenter={{ lat: -34.397, lng: 150.644 }}
    onBoundsChanged={props.onBoundsChanged}
 />
));



let ThirdPage = ({ mapOptions, bounds = null, center = { lat: 47.6205588, lng: -122.3212725 }, markers = []}) => {
// () => {
    // = ({gender_filled}) =>
   
    let handleMapMounted = (map) => {
        console.log('mounting')
        mapOptions._map = map;
        console.log('mounted')
        console.log(mapOptions._map)
    }

    let handleBoundsChanged = () => {
        this.setState({
          bounds: mapOptions._map.getBounds(),
          center: mapOptions._map.getCenter(),
        });
    }

    // let handleSearchBoxMounted = (searchBox) => {

    //     mapOptions._searchBox = searchBox;
    // }


    return (
      <SimpleMapExampleGoogleMap
        containerElement={
          <div style={{ height: `300px`, width: `400px` }} />
        }
        mapElement={
          <div style={{ height: `300px`, width: `400px` }} />
        }
        onMapMounted={handleMapMounted}
        onBoundsChanged={handleBoundsChanged}
        onSearchBoxMounted={handleSearchBoxMounted}
        bounds={bounds}
      />)

}

const mapStateToProps = (state) => {
    return {
        mapOptions: {
            _map: undefined,
            _searchBox: undefined
        }
    }
}

ThirdPage = connect(
    mapStateToProps
)(ThirdPage)

ThirdPage.nextButton = (state) => {
    if (state.filter_values['gender'] !== undefined && state.filter_values['gender'].length !== 0) {
        return(<PageButton color="blue" direction="next" page={SECOND} text="Verder" />)
    }
    return false
}

ThirdPage.prevButton = (state) => {
    return (<PageButton color="blue" direction="next" page={SECOND} text="Terug" />)
}

export default ThirdPage



    
