import { connect } from 'react-redux'
import React, { PropTypes } from 'react'

import PageButton from '../../containers/page_button'

import { SECOND } from '../../services/pages'

import RadiosQuestion from '../radios_question'
import * as filters from '../../services/filters'


let FirstPage = ({gender_filled}) => {
    // = ({gender_filled}) =>


    return (
    <div className="quickscan-inner">
        <div className="quickscan-block quickscan--intro">
            <div className="quickscan-block-inner">
                <p>De quickscan geeft jjoijjiou inzicht in uw vertrekpunt voor traceerbaarheid. Door het invullen van alle onderdelen van de quickscan krijgt u op hoofdlijnen een beeld van de mogelijke besparingen, welke processen en afdelingen betrokken zijn en welke stappen nog ondernomen moeten worden om traceerbaarheid te realiseren.</p>
                <p>De onderdelen leiden u door: de status van patiëntveiligheid, de inrichting van de logistieke processen, de status van barcodering van de producten en de software die u in huis heeft. Het resultaat is een samenvattend rapport dat u een handvat biedt om aan de slag te gaan!</p>
            </div>
            <div>
                <RadiosQuestion name="gender" title={filters.gender.label} options={filters.gender.options} />
            </div>
        </div>
    </div>)
}

const mapStateToProps = (state) => {
    return {
        filters: filters,
        gender_filled: (state.filter_values['gender'] !== undefined)
    }
}

FirstPage = connect(
    mapStateToProps
)(FirstPage)

FirstPage.nextButton = (state) => {
    if (state.filter_values['gender'] !== undefined && state.filter_values['gender'].length !== 0) {
        return(<PageButton color="blue" direction="next" page={SECOND} text="Verder" />)
    }
    return false
}

export default FirstPage
