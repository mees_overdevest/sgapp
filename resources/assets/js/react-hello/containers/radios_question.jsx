
import { connect } from 'react-redux'
import React, { PropTypes } from 'react'

import QuestionWrapper from '../components/question_wrapper';
import {RadioGroup, Radio} from 'react-radio-group';
import Image from '../components/image';

import { set_filter_value } from '../action_creators/filters';

let RadiosQuestion = ({title, name, value, onChange, options, horizontal = false}) => {
    let current_explanation = undefined;
    for (let {key, explanation} of options) {
        if (key == value && explanation !== undefined) {
            current_explanation = explanation
        }
    }

    return (<QuestionWrapper title={title} explanation={current_explanation}>
        <RadioGroup name={name} selectedValue={value} onChange={onChange}>
            {options.map((o) => (
                <div className={horizontal ? 'horizontal-radio' : 'quickscan-radio'} key={o.key}>
                    <Radio value={o.key} id={name + "_" + o.key} />
                    <label htmlFor={name + "_" + o.key}>{(o.label)}</label>
                    {o.image ? (
                        // <div className="radio-img">
                        <label htmlFor={name + "_" + o.key}>
                            <Image image={o.image} extension={o.extension} />
                        </label>
                    ) : []}
                </div>
            ))}
        </RadioGroup>
    </QuestionWrapper>)
};

const mapStateToProps = (state, {name}) => {
    return {
        value: (state.filter_values[name] !== undefined) ? state.filter_values[name] : ''
    }
}

const mapDispatchToProps = (dispatch, {name}) => {
    return {
        onChange: (value) => dispatch(set_filter_value(name, value))
    };
}

RadiosQuestion = connect(
    mapStateToProps,
    mapDispatchToProps
)(RadiosQuestion)

export default RadiosQuestion
