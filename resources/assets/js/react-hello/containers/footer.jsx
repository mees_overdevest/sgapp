import { connect } from 'react-redux'
import React, { PropTypes } from 'react'

import { get_page } from '../services/pages'

let Footer = ({prev_button, next_button, print_button}) => (
    <div className="quickscan-inner quickscan-inner--footer">
        {prev_button ? prev_button : []}
        {next_button ? next_button : []}
    </div>
)

const mapStateToProps = (state) => {
    let page_class = get_page(state.page);
    let next_button = page_class.nextButton ? page_class.nextButton(state) : false;
    let prev_button = page_class.prevButton ? page_class.prevButton(state) : false;

    return { next_button, prev_button }
}

Footer = connect(
    mapStateToProps
)(Footer)

export default Footer