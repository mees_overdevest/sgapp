
import { connect } from 'react-redux'
import React, { PropTypes } from 'react'

import Button from '../components/button'

import { set_filter_value } from '../action_creators/filters'

const mapStateToProps = (state, {name, value, chosen_values}) => {
  return {
    name: name,
    value: value,
    color: 'success',
    chosen_values: chosen_values
  }
}

const mapDispatchToProps = (dispatch, {name, value, chosen_values}) => {
  return {
    onClick: () => {
    	// Perform Category toggle
    	if(!chosen_values.includes(value)){
    		chosen_values.push(value)
    	} else {
    		chosen_values.pop(value)
    	}
    	dispatch(set_filter_value(name, chosen_values))	
    }
  };
}

const CategoryButton = connect(
  mapStateToProps,
  mapDispatchToProps
)(Button)

export default CategoryButton