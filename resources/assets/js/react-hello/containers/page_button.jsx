
import { connect } from 'react-redux'
import React, { PropTypes } from 'react'

import Button from '../components/button'

import { set_page } from '../action_creators/page'

const mapStateToProps = (state, {page}) => {
    return {
        active: false
    }
}

const mapDispatchToProps = (dispatch, {page}) => {
    return {
        onClick: () => dispatch(set_page(page))
    };
}

const PageButton = connect(
    mapStateToProps,
    mapDispatchToProps
)(Button)

export default PageButton