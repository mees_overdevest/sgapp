import React from 'react'
import { render } from 'react-dom'

import { Provider } from 'react-redux'
import App from './containers/app'
import Footer from './containers/footer'
import reducer from './reducer'
import factory from './services/store'

var store = factory(reducer);

render(
    <Provider store={store}>
      <App />
    </Provider>,
    document.querySelector('#react-canvas')
    // document.getElementById('react-canvas')
)

render(
    <Provider store={store}>
        <Footer />
    </Provider>,
    document.querySelector('#react-footer')
    // document.getElementById('react-footer')
)
