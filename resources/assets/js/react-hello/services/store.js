import { applyMiddleware, createStore, compose } from 'redux';
import thunk from 'redux-thunk';
// import dataService from './data-service'

const factory = (reducer) => {  
  if (process.env.NODE_ENV == 'production') {
    return createStore(
      reducer
    );
  } else {
    var createLogger = require('redux-logger');
    var logger = createLogger();

    const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

    return createStore(
      reducer,
      composeEnhancers(applyMiddleware(logger, thunk))
    );
  }
};

export default factory;
