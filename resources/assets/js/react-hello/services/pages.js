import FirstPage from '../containers/pages/first';
import SecondPage from '../containers/pages/second';
import ThirdPage from '../containers/pages/third';

export const FIRST = 'FIRST'
export const SECOND = 'SECOND'
export const THIRD = 'THIRD'

let map = {
    [FIRST]: FirstPage,
    [SECOND]: SecondPage,
    [THIRD]: ThirdPage
};

let get_page = (key) => (map[key]);

export { get_page };