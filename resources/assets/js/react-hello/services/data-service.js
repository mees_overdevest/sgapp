
import request from 'superagent'

const getApiGenerator = next => (route, name) => request
    .get(route)
    .end((err, res) => {
    if (err) {
        return next({
            type: 'GET_CATEGORIES_DATA_ERROR',
            err
        })
    }
    const data = JSON.parse(res.text)

    next({
        type: 'GET_CATEGORIES_DATA_RECEIVED',
        data
    })
})

const dataService = store => next => action => {

    next(action)
    const getApi = getApiGenerator(next)
    switch(action.type) {
        case 'GET_CATEGORIES_DATA':
            getApi('http://sg.dev/api/categories', 'GET_CATEGORIES_DATA')
            // return {
            // 	type: 'LOADING',
            // 	true
            // }

            // fetch('sg.dev/api/categories')
            // 	.then(data => console.log(data))
            // 	.then(data => next({
            // 		type: 'GET_CATEGORIES_DATA_RECEIVED',
            // 		data
            // 	}))
            // 	.catch(err => next({
            // 		type: 'GET_CATEGORIES_DATA_ERROR',
            // 		err
            // 	}));
            break
        default:
            break
    }
};

export default dataService