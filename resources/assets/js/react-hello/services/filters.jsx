import React from 'react'

export const gender = {
    label: 'Uw geslacht',
    options: [
        {
            key: 'a',
            label: 'Man'
        },
        {
            key: 'b',
            label: 'Vrouw'
        },
        {
            key: 'c',
            label: 'Anders'
        }
    ]
}