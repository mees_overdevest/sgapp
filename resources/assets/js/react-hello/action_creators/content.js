
import { GET_CATEGORIES, CONTENT_SUCCESS, CONTENT_ERROR } from '../action_types/content';

import { START_LOADING, ERROR_LOADING, DONE_LOADING } from '../action_types/loading';

export const get_categories = () => {
	return (dispatch) => {		
		dispatch({type: START_LOADING})
		return fetch('http://sg.dev/api/categories')
			.then(response =>	response.json())
			.then(response => dispatch(fetch_content_success(response, dispatch, CONTENT_SUCCESS, 'categories')))
			.catch(err => dispatch(fetch_content_error(err, dispatch, CONTENT_ERROR,'categories')));
	}
}

function fetch_content_success(data, dispatch, action_type, kind){
	dispatch({type: DONE_LOADING})
	return {
		type: action_type,
		data: data,
		kind: kind
	}
}

function fetch_content_error(data, dispatch, action_type, kind){
	dispatch({type: ERROR_LOADING})
	return {
		type: action_type,
		data: data,
		kind: kind
	}
}