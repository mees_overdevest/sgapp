
import { SET_FILTER_VALUE } from '../action_types/filters';

export const set_filter_value = (name, value) => ({type: SET_FILTER_VALUE, name, value});
