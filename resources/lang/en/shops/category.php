<?php
/**
 * Created by PhpStorm.
 * User: mees
 * Date: 31-07-16
 * Time: 03:07
 */
return [
    'actions' => [
        'edit' => 'Edit Shop Category',
        'create' => 'Create Shop Category',
        'save' => 'Save Shop Category',
        'update' => 'Update Shop Category',
    ],
    'create' => [
        'success' => 'The Shop Category :name has been successfully created!'
    ],
    'update' => [
        'success' => 'The Shop Category :name has been successfully updated!'
    ],
    'category' => 'Shop Category',
    'categories' => 'Shop Categories',
    'description'   => 'Shop Description',
    'parent' => 'Parent Shop Category'
];