<?php
return [
	'create' => [
		'title' => 'Create a new shop',
		'submit' => 'Save new shop',
		'success' => 'The new shop :name has been created!',
	],
	'edit' => [
		'title' => 'Edit shop',
        'save'  => 'Save new shop info',
	],
	'addPhotos'	=> [
		'success' => 'Successfully added new Photos to Shop :name !'
	],
    'addMarker' => [
        'success' => 'Succesfully added a marker to Shop :name !'
    ]
];