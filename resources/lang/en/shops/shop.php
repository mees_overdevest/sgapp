<?php
return [
	'name' => 'shop name',
	'description' => 'shop description',
	'since' 	=> 'since',
	'owner'		=> 'shop owner',
    'addPhotos' => [
        'success' => 'Successfully added new photos to the shop :name !',
        'title' => 'Add photos to this shop'
    ],
    'deleted' => 'Successfully deleted shop: :name !'
];