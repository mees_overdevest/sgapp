<?php

return [
	'nav' => [
		'title' => 'Users',
		'create' => 'Create user',
	],
	'form' => [
		'firstName' => 'Firstname',
		'lastName' => 'Lastname',
		'role'	=> 'role',
		'language' => 'language',
		'save'		=> 'save user',
		'active'	=> 'active',
		'inactive'	=> 'inactive',
		'update'		=> 'update user',
		'gender'	=> 'gender',
		'email'		=> 'email',
		'language'	=> 'language'
	],
	'messages' => [
		'userUpdated' => 'You have successfully updated the User :name !'
	],
];