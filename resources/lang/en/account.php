<?php
/**
 * Created by PhpStorm.
 * User: mees
 * Date: 04-08-16
 * Time: 15:52
 */
return [
    'messages' => [
        'success'   => 'You have successfully updated your account.',
    ],
    'actions' => [
        'edit'  => 'Edit Account',
    ],
];