<?php
/**
 * Created by PhpStorm.
 * User: mees
 * Date: 14-07-16
 * Time: 16:04
 */
return [
    'create' => [
        'create' => 'Create a Role',
        'success' => 'The Role :name has been created',
    ],
];