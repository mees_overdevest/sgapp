<?php
/**
 * Created by PhpStorm.
 * User: mees
 * Date: 14-07-16
 * Time: 08:55
 */
return [
    'create' => [
        'create' => 'Create a Permission',
        'success' => 'The Permission :name has been created',
    ],
];