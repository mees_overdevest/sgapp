<?php
/**
 * Created by PhpStorm.
 * User: mees
 * Date: 25-07-16
 * Time: 10:06
 */

return [
    'area' => 'area',
    'create' => [
        'title' => 'Create area',

    ],
    'addPhotos'	=> [
        'success' => 'Successfully added new Photos to Area :name !',
        'title' => 'Add photos to this area'
    ],
    'deleted' => 'Successfully deleted Area: :name !'
];