<?php
/**
 * Created by PhpStorm.
 * User: mees
 * Date: 30-07-16
 * Time: 16:42
 */

return [
    'create' => [
        'new' => 'Create Service',
        'submit' => 'Save Service',
        'success' => 'Successfully created the Service :name !'
    ],
    'update' => [
        'submit' => 'Update Service',
        'success' => 'Successfully updated the Service :name !',
    ],
    'service'  => 'service',
    'services' => 'services',
    'name' => 'Service name',
    'description' => 'Service Description',

    'addPhotos'	=> [
        'success' => 'Successfully added new Photos to Service :name !',
        'title' => 'Add new photos to this service'
    ]
];