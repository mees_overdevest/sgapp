<?php
return [
	'address' => 'address',
	'city' => 'city',
	'email' => 'email',
	'name'	=> 'name',
	'actions' => [
		'edit' => 'edit',
		'delete' => 'delete',
		'reset-password' => 'reset password',
		'show' => 'show',
		'save' => 'save',
	],
	'mr' => 'Mr.',
	'mrs' => 'Mrs.'
];