<?php
/**
 * Created by PhpStorm.
 * User: mees
 * Date: 30-07-16
 * Time: 16:42
 */

return [
    'create' => [
        'new' => 'Create Brand',
        'submit' => 'Save Brand',
        'success' => 'Successfully created the Brand :name !'
    ],
    'brand'     => 'brand',
    'brands'    => 'brands',
    'name'      => 'brand name',
    'description'=> 'brand description',
    'deleted'   => 'You have successfulle deleted brand: :name and all of its photos',

    'addPhotos'	=> [
        'title' => 'Add photos to brand',
        'success' => 'Successfully added new Photos to Brand :name !'
    ]
];