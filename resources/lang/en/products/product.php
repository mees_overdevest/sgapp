<?php
/**
 * Created by PhpStorm.
 * User: mees
 * Date: 30-07-16
 * Time: 16:42
 */

return [
    'create' => [
        'new' => 'Create Product',
        'submit' => 'Save Product',
        'success' => 'Successfully created the Product :name !'
    ],
    'update' => [
        'submit' => 'Update Product',
        'success' => 'Successfully updated the Product :name !',
    ],
    'product'  => 'product',
    'products' => 'products',
    'name' => 'Product name',
    'description' => 'Product Description',
    'addPhotos'	=> [
        'success' => 'Successfully added new Photos to Product :name !',
        'title'     => 'Add photos to this product'
    ]
];