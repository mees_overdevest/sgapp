<?php
/**
 * Created by PhpStorm.
 * User: mees
 * Date: 30-07-16
 * Time: 16:42
 */

return [
    'create' => [
        'new' => 'Create Product Category',
        'submit' => 'Save Product Category',
        'success' => 'Successfully created the Product Category :name !'
    ],
    'PCategory'  => 'product category',
    'PCategories' => 'product categories',

    'addPhotos'	=> [
        'success' => 'Successfully added new Photos to Product Category :name !'
    ]
];