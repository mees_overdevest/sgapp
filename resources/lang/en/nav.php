<?php
return [
	'shops' => [
		'title' => 'shops',
	],
	'brands' => [
		'title' => 'brands',
	],
	'areas' => [
		'title' => 'areas',
	],
	'PCategories' => [
		'title' => 'product categories',
	],
	'shopCategories' => [
		'title' => 'shop categories',
	],
	'users' => [
		'title' => 'users',
	],
	'products' => [
		'title' => 'products',
	],
	'services' => [
		'title' => 'services',
	],
];