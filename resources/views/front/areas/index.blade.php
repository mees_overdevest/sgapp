@extends('layouts.app')

@section('content')
    {{--Voor de Google Maps voor winkelgebieden--}}
    {{--//http://codepen.io/jhawes/pen/ujdgK--}}
    {{--http://stackoverflow.com/questions/5072059/polygon-drawing-and-getting-coordinates-with-google-map-api-v3--}}
    <div class="container">

        <h1>{{ ucfirst(trans('areas/area.create.title')) }}</h1>



        <div class="btn-group">
            <button type="button" class="btn btn-default" onclick="initMap()">Open map</button>
        </div>

        <div id="map-canvas"></div>



    </div>

@stop

@section('javascript')
    {{--<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>--}}
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB8if5Pn4291xGseJe2OydAz35tf9xua_Y&libraries=drawing" async defer></script>
    <script>
        //var myPolygon;
        var points;
        var coordinates;
        function initMap(){
            var map = new google.maps.Map(document.getElementById('map-canvas'), {
                center: new google.maps.LatLng(0, 0),
                zoom: 0
            });

            var Areas = [];
            var info = [];
            var bounds = new google.maps.LatLngBounds();
            // Using the core $.ajax() method
            $.ajax({

                        // The URL for the request
                        url: "/auth/ajax/areas/getAreas",

                        // Whether this is a POST or GET request
                        type: "GET",

                        // The type of data we expect back
                        dataType : "json",
                    })
                    // Code to run if the request succeeds (is done);
                    // The response is passed to the function
                    .done(function( json ) {
//                        console.log(json);
                        for(var i = 0; i <= json.length - 1; i++){
                            var area = [];
                            var tempCoords = [];
                            for(var obj in json[i]){
                                if(obj === 'info'){
                                    area.push(json[i][obj]);
                                    console.log(json[i][obj]);
                                } else {
                                    var coord = new google.maps.LatLng(json[i][obj].lat, json[i][obj].lng);
                                    tempCoords.push(coord);
                                    bounds.extend(coord);
                                }


                            }
                            area.push(tempCoords);
//                            console.log(areaCoordinates);
                            Areas.push(area);

                            tempCoords = [];
                            area = [];

                        }

//                        console.log(bounds);
                        map.setCenter(bounds.getCenter());
                        map.fitBounds(bounds);
                        new google.maps.Rectangle({
                            bounds: bounds,
                            map: map,
                            fillColor: "#000000",
                            fillOpacity: 0.2,
                            strokeWeight: 0,
                            zoom: 4
                        });

                    })
                    // Code to run if the request fails; the raw request and
                    // status codes are passed to the function
                    .fail(function( xhr, status, errorThrown ) {
                        alert( "Sorry, there was a problem!" );
                        console.log( "Error: " + errorThrown );
                        console.log( "Status: " + status );
                        console.dir( xhr );
                    })
                    // Code to run regardless of success or failure;
                    .always(function( xhr, status ) {
                        var info = [];
    console.log(Areas);
                        for(var i = 0; i <= Areas.length; i++){

                            info[i] = Areas[i].splice(0,1).pop();

                            makePolygon(Areas[i], info[i], map);


                        }
                    });



//            map.fitBounds(bounds);





        }

        function makePolygon(coords, info, map){
            polygon = new google.maps.Polygon({
                paths: coords,
                strokeColor: '#FF0000',
                strokeOpacity: 0.8,
                strokeWeight: 3,
                fillColor: '#FF0000',
                fillOpacity: 0.35
            });
            polygon.setMap(map);
//                    console.log(info);

            google.maps.event.addListener(polygon, 'click', function(event) {
                var infoWindow = new google.maps.InfoWindow();
                infoWindow.setContent("<h3>" + info.title + "</h3><br>"+
                        "<a class='btn btn-default' href='" + BASE + "/auth/areas/" + info.id +"'>Bekijk " + info.city + "</a>"+
                        "<p>Beschrijving</p>");

                // 'laty,lngy' is the location of one point in PGpoints, which can be chosen as you wish
                infoWindow.setPosition(event.latLng);
                infoWindow.open(map);
            });

        }








    </script>
@stop