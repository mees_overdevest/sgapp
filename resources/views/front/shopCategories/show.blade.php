@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>Categorie {{ $shopCategory->title }}</h1>
        <h3>Links die horen bij deze categorie</h3>
        <p>{{ $shopCategory->description }}</p>

        @if($shopCategory->shops->count() > 0)
            <div class="col-md-6">
                <h2>Producten</h2>
                <p>Alle producten die tot deze categorie toe behoren.</p>
                @foreach($shopCategory->shops as $shop)
                    <h3><a href="{{ route('auth.shops.show', $shop->id) }}">{{ $shop->name }} in {{ $shop->city }}</a></h3>
                    <p>{{ $shop->description }}</p>
                @endforeach
            </div>
        @endif
    </div>

@stop

@section('footer')

@stop
