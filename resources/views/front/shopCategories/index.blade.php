@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>{{ trans('shops/category.categories') }}</h1>

        <table class="table table-bordered table-hover">
            <thead>
            <tr>
                <th>{{ trans('shops/category.category') }}</th>
                <th>{{ trans('shops/category.description') }}</th>
                <th>{{ trans('shops/category.parent') }}</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($shopCategories as $category)
                <tr>
                    <td><a href='{{ route('auth.shopCategory.show', [$category->id]) }}'>{{ $category->title }}</a></td>
                    <td>{{ $category->description }}</td>
                    <td><p class="text-danger">Hoofdcategorie</p></td>
                @foreach ($category->children as $children)
                    <tr>
                        <td><a href='{{ route('auth.shopCategory.show', [$children->id]) }}'>{{ $children->title }}</a></td>
                        <td>{{ $children->description }}</td>
                        <td>{{ $children->parent->title }}</td>
                    </tr>
                    @endforeach
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

    <!-- Test -->
@stop

@section('javascript')

@stop
