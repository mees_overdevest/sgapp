@extends('layouts.app')

@section('styles')
    <link rel="stylesheet" type="text/css" href="{{ Request::root() }}/assets/plugins/owl-carousel/owl.carousel.css">
    <link rel="stylesheet" type="text/css" href="{{ Request::root() }}/assets/plugins/owl-carousel/owl.theme.css">
@stop

@section('content')
    <div class="container">
        <div class="col-md-12 col-lg-6">
            <div class="carrousel-wrap">
                <h2>Populaire winkels</h2>
                @include('includes.carrousels.homecarrousel', ['objects' => $shops, 'favouriteRoute' => 'shops'])
            </div>
        </div>

        <div class="col-md-6">
            <h2>Popular Product Categories</h2>
            <ul class="list-group">
                @foreach($PCategories as $cat)
                    <li class="list-group-item">{{ $cat->title }}</li>
                @endforeach
            </ul>
        </div>
        <div class="col-md-6">
            <h2>Popular Products</h2>
            <ul class="list-group">
                @include('includes.carrousels.homecarrousel', ['objects' => $products, 'favouriteRoute' => 'products'])
            </ul>
        </div>
        <div class="col-md-6">
            <h2>Popular Services</h2>
            <ul class="list-group">
                @include('includes.carrousels.homecarrousel', ['objects' => $services, 'favouriteRoute' => 'services'])
            </ul>
        </div>
        <div class="col-md-6">
            <h2>Popular Areas</h2>
            <ul class="list-group">
                @include('includes.carrousels.homecarrousel', ['objects' => $areas, 'favouriteRoute' => 'areas'])
            </ul>
        </div>
        <div class="col-md-6">
            <h2>Popular Brands</h2>
            <ul class="list-group">
                @include('includes.carrousels.homecarrousel', ['objects' => $brands, 'favouriteRoute' => 'brands'])
            </ul>
        </div>


    </div>
@endsection

@section('javascript')
    <script src="{{ Request::root() }}/assets/plugins/owl-carousel/owl.carousel.js"></script>
    <script>
        $(document).ready(function() {
//            $(".homecarrousel").owlCarousel({
//                singleItem: true,
//                pagination: true
//            });

            //Sort random function
            function random(owlSelector) {
                owlSelector.children().sort(function () {
                    return Math.round(Math.random()) - 0.5;
                }).each(function () {
                    $(this).appendTo(owlSelector);
                });
            }

            $(".homecarrousel").owlCarousel({
                singleItem: true,
                navigation: true,
                pagination:false,
                navigationText: [
                    "<i class='fa fa-chevron-left' aria-hidden='true'></i> Vorige winkel ",
                    "Volgende winkel <i class='fa fa-chevron-right' aria-hidden='true'></i>"
                ],
                beforeInit: function (elem) {
                    //Parameter elem pointing to $("#owl-demo")
                    random(elem);
                }

            });

            $(".carrousel").owlCarousel({
                pagination:false,
                navigation: true,
                singleItem: true,
                navigationText: [
                    "<i class='fa fa-chevron-left' aria-hidden='true'></i>",
                    "<i class='fa fa-chevron-right' aria-hidden='true'></i>"
                ],
                beforeInit: function (elem) {
                    //Parameter elem pointing to $("#owl-demo")
                    random(elem);
                }
            });
        });
    </script>
@stop
