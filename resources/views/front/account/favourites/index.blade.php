@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-lg-6">
                <div class="carrousel-wrap">
                    <h1>Mijn favorieten</h1>
    {{--                @include('includes.carrousels.homecarrousel', ['objects' => $shops, 'favouriteRoute' => 'shops'])--}}
                </div>
            </div>
        </div>

        <div class="row">
            @foreach($favourites as $key => $favGroup)
                @if(count($favGroup) != 0)
                    <div class="col-md-6">
                        <h2>{{ ucfirst($key . 's') }}</h2>
                        <div class="list-group">
                            @foreach($favGroup as $favourite)
                                <?php $favObject = $favourite->object() ?>
                                <div class="list-group-item">
                                    <div class="pull-right">
                                        @include('includes.buttons.favourite', ['object' => $favObject, 'routeName' => $key . 's'])
                                    </div>
                                    <h4 class="list-group-item-heading"><a href="{{ route('auth.' . $key . 's.show', $favObject->id) }}">{{ $favObject->title }}</a></h4>
                                    <p class="list-group-item-text">{{ $favObject->description }}</p>
                                </div>

                            @endforeach
                        </div>
                    </div>
                @endif
            @endforeach
        </div>

    </div>
@endsection

@section('javascript')

@stop
