@extends('layouts.app')

@section('content')
    <div class="container">
    	<h1>{{ $title }}</h1>

        {!! Form::open([]) !!}
        <div class="form-group">
            {!! Form::label(ucfirst(trans('admin/users.form.email'))) !!}
            {!! Form::email('email', $user->email, ['class'=>'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label(ucfirst(trans('admin/users.form.gender'))) !!}
            {!! Form::select('gender', [0=>Lang::get('general.mr'), 1=>Lang::get('general.mrs')], $user->gender, ['class'=>'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label(ucfirst(trans('admin/users.form.firstName'))) !!}
            {!! Form::text('firstName', $user->firstName, ['class'=>'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label(ucfirst(trans('admin/users.form.lastName'))) !!}
            {!! Form::text('lastName', $user->lastName, ['class'=>'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('language', ucfirst(trans('admin/users.form.language'))) !!}
            {!! Form::select('language', $languages, $user->language_id, ['class'=>'form-control select']) !!}
        </div>
        <div class="form-group">
            <p></p>
            {!! Form::submit(ucfirst(trans('general.actions.save')), ['class'=>'btn btn-info']) !!}
        </div>
        {!! Form::close() !!}

    </div>
@endsection
