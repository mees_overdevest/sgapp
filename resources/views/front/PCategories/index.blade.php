@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>Product Categorieën</h1>

        <table class="table table-bordered table-hover">
            <thead>
            <tr>
                <th>Categorieën</th>
                <th>Beschrijving</th>
                <th>Overkoepelende categorie</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($PCategories as $category)
                <tr>
                    <td><a href='{{ route('auth.PCategories.show', [$category->id]) }}'>{{ $category->title }}</a></td>
                    <td>{{ $category->description }}</td>
                    <td><p class="text-danger">Hoofdcategorie</p></td>
                @foreach ($category->children as $children)
                    <tr>
                        <td><a href='{{ route('auth.PCategories.show', [$children->id]) }}'>{{ $children->title }}</a></td>
                        <td>{{ $children->description }}</td>
                        <td>{{ $children->parent->title }}</td>
                    </tr>
                    @endforeach
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

    <!-- Test -->
@stop

@section('javascript')

@stop
