@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>Categorie {{ $PCategory->title }}</h1>
        <p>Links die horen bij deze categorie</p>
        <p>{{ $PCategory->description }}</p>

        @if($PCategory->products->count() > 0)
            <div class="col-md-6">
                <h2>Producten</h2>
                <p>Alle producten die tot deze categorie toe behoren.</p>
                @foreach($PCategory->products as $product)
                    <h3><a href="{{ route('auth.products.show', $product->id) }}">{{ $product->title }}</a></h3>
                    <p>{{ $product->description }}</p>
                @endforeach
            </div>
        @endif

        @if($PCategory->services->count() > 0)
            <div class="col-md-6">
                <h2>Diensten</h2>
                <p>Alle diensten die tot deze categorie toe behoren.</p>

                @foreach($PCategory->services as $service)
                    <h3><a href="{{ route('auth.services.show', $service->id) }}">{{ $service->title }}</a></h3>
                    <p>{{ $service->description }}</p>
                @endforeach
            </div>
        @endif
    </div>

@stop

@section('footer')

@stop
