@extends('layouts.app')

@section('styles')
    <link rel="stylesheet" type="text/css" href="{{ Request::root() }}/assets/plugins/owl-carousel/owl.carousel.css">
    <link rel="stylesheet" type="text/css" href="{{ Request::root() }}/assets/plugins/owl-carousel/owl.theme.css">
@stop

@section('content')
    <div class="container">
        <h1>Dienst: {{ $service->title }}</h1>

        <div class="col-sm-6 col-md-6 col-lg-6">
            <h3>Beschrijving</h3>
            <p>{{ $service->description }}</p>
        </div>

        @if($service->pictures)
            <div class="col-sm-6 col-md-6 col-lg-6">
                <div id="carrousel owl-carousel owl-theme">

                    @foreach($service->pictures as $image)
                        <div class="item"><img src="{{ \App\Helpers\ImageManager::getMobilePath($service->id, $image) }}" alt="{{ $image->image_name }}"></div>

                    @endforeach
                </div>
            </div>
        @endif

        @if($service->categories->count() > 0)
            <div class="col-sm-6 col-md-6 col-lg-6">
                <h3>Service Categorieën</h3>
                <table id="tableProducts" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <td>Titel</td>
                        <td>Beschrijving</td>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($service->categories as $PCategory)
                        <tr>
                            <td><a href="{{ route('auth.PCategories.show',$PCategory->id) }}">{{ $PCategory->title }}</a></td>
                            <td>{{ $PCategory->description }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        @endif

        @if($service->shops->count() > 0)
            <div class="col-sm-6 col-md-6 col-lg-6">
                <h3>Winkels</h3>
                <p>Alle winkels die deze dienst aanbieden</p>
                <table id="tableProducts" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <td>Titel</td>
                        <td>Beschrijving</td>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($service->shops as $shop)
                        <tr>
                            <td><a href="{{ route('auth.shops.show',$shop->id) }}">{{ $shop->name }} in {{ $shop->city }}</a></td>
                            <td>{{ $shop->description }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        @endif


    </div>
@stop

@section('javascript')
    <script src="{{ Request::root() }}/assets/plugins/owl-carousel/owl.carousel.js"></script>
    <script>
        $("#carrousel").owlCarousel({
            autoPlay: true,
            singleItem: true,
            pagination: true
        });
    </script>
@stop

