@extends('layouts.app')

@section('styles')
    <link rel="stylesheet" type="text/css" href="{{ Request::root() }}/assets/plugins/owl-carousel/owl.carousel.css">
    <link rel="stylesheet" type="text/css" href="{{ Request::root() }}/assets/plugins/owl-carousel/owl.theme.css">
    <link rel="stylesheet" type="text/css" href="{{ Request::root() }}/assets/css/homepage.css">
@stop

@section('content')
    <div class="container">
        <h1>{{  ucfirst(trans('products/product.products')) }}</h1>

        @foreach($products as $product)
            <div class="col-md-6">
                <h2><a href="{{ route('auth.products.show', $product->id) }}">{{ $product->title }}</a></h2>
                <p class="lead">{{ $product->description }}</p>
                <hr>
                @if($product->pictures)
                    <div class="carrousel" class="owl-carousel owl-theme">
                        @foreach($product->pictures as $image)
                            <div class="item"><img src="{{ \App\Helpers\ImageManager::getMobilePath($product->id, $image) }}" alt="{{ $image->image_name }}"></div>
                        @endforeach
                    </div>
                @endif

                @if($product->categories->count() > 0)
                    <h3>Product Categorieën</h3>
                    <table id="tableProducts" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <td>Titel</td>
                                <td>Beschrijving</td>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($product->categories as $PCategory)
                            <tr>
                                <td><a href="{{ route('auth.PCategories.show',$PCategory->id) }}">{{ $PCategory->title }}</a></td>
                                <td>{{ $PCategory->description }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                @endif

            </div>
        @endforeach


    </div>
@stop

@section('javascript')
    <script src="{{ Request::root() }}/assets/plugins/owl-carousel/owl.carousel.js"></script>
    <script>
        $(".carrousel").owlCarousel({
            autoPlay: true,
            singleItem: true,
            pagination: true
        });
    </script>
@stop
