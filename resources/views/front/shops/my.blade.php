@extends('layouts.app')

@section('content')
    <div class="container">


        <h1>{{ trans('shops/wizard.edit.title') }}</h1>

        <a href="{{ route('auth.me.shop.addShopMarker', $shop->id) }}" class="btn btn-info">Voeg een Google locatie toe aan mijn winkel.</a>
        <a href="{{ route('auth.me.shop.findMyArea', $shop->id) }}" class="btn btn-info">Voeg een winkelgebied toe aan mijn winkel.</a>

        {!! Form::model($shop, ['method' => 'POST', 'route' => ['auth.me.shop.update', $shop->id]]) !!}
        <div class="form-group">
            {!! Form::label('title', trans('shops/shop.name')) !!}
            {!! Form::text('title', $shop->title, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('description', trans('shops/shop.description')) !!}
            {!! Form::text('description', $shop->description, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            @foreach ($categories as $category)
                @foreach ($category as $key => $value)
                    {!! Form::Label('category_id', ucfirst($value)) !!}
                    @if($shop->hasCategory($key))
                        {!! Form::checkbox('category_id[]', $key, true, ['class' => 'form-control', 'checked' => 'true']) !!}
                    @else
                        {!! Form::checkbox('category_id[]', $key, false, ['class' => 'form-control']) !!}
                    @endif
                @endforeach
            @endforeach
        </div>

        <div class="form-group">
            {!! Form::label('since', ucfirst(trans('shops/shop.since'))) !!}
            {!! Form::date('since', date("d-m-Y", $shop->since), ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('city', ucfirst(trans('general.city'))) !!}
            {!! Form::text('city', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('address', ucfirst(trans('general.address'))) !!}
            {!! Form::text('address', null, ['class' => 'form-control']) !!}
        </div>

        {{ Form::hidden('id', $shop->id) }}

        <div class="form-group">
            {!! Form::submit(trans('shops/wizard.edit.save'), ['class' => 'btn btn-primary form-control']) !!}
        </div>
        {!! Form::close() !!}
    </div>

@stop

@section('javascript')

@stop