@extends('layouts.app')

@section('styles')
    <link rel="stylesheet" type="text/css" href="{{ Request::root() }}/assets/plugins/owl-carousel/owl.carousel.css">
    <link rel="stylesheet" type="text/css" href="{{ Request::root() }}/assets/plugins/owl-carousel/owl.theme.css">
@stop


@section('content')
    <div class="container">
        <div class="row text-center">
            <h1>Winkels</h1>
        </div>

        @foreach($shops as $shop)
            <div class="col-md-6">
                @if($shop->pictures)
                    <div class="carrousel-index owl-carousel owl-theme">

                        @foreach($shop->pictures as $image)
                            <div class="item">
                                <div class="textoverlay">
                                    <h3>{{ $shop->title }}</h3>
                                    <p>{{ $image->description }}</p>
                                    <a class="btn btn-info" href="{{ route('auth.shops.show', $shop->id) }}">Bekijk {{ $shop->title }}</a>
                                </div>
                                <img src="{{ \App\Helpers\ImageManager::getMobilePath($shop->id, $image) }}" alt="{{ $image->image_name }}">
                            </div>

                        @endforeach
                    </div>
                @endif
            </div>
        @endforeach



        {{--<table id="tableShops" class="table table-striped table-bordered" cellspacing="0" width="100%">--}}
            {{--<thead>--}}
            {{--<tr>--}}
                {{--<td>{{  ucfirst(trans('shops/shop.name')) }}</td>--}}
                {{--<td>{{  ucfirst(trans('general.actions.edit')) }}</td>--}}
                {{--<td>{{  ucfirst(trans('general.actions.delete')) }}</td>--}}
                {{--<td>Afbeeldingen</td>--}}
            {{--</tr>--}}
            {{--</thead>--}}
            {{--<tbody>--}}
            {{--@foreach($shops as $shop)--}}
                {{--<tr>--}}
                    {{--<td>{{ $shop->id }}</td>--}}
                    {{--<td><a href="{{ route('admin.shops.show', $shop->id) }}">{{ $shop->name }}</a></td>--}}
                    {{--<td><a href="{{ route('admin.shops.edit', $shop->id) }}"><i class="fa fa-pencil-square-o"></i></a></td>--}}
                    {{--<td>--}}
                        {{--{!! Form::open(array('route' => array('admin.shops.destroy', $shop->id), 'method' => 'DELETE')) !!}--}}
                        {{--<button class="btn btn-danger" data-confirm="Are you sure?">Verwijder <i class="fa fa-trash-o"></i></button>--}}
                        {{--{!! Form::close() !!}--}}
                    {{--</td>--}}
                    {{--<td>--}}
                        {{--<a href="{{route('admin.shops.addPhotos',$shop->id)}}" class="btn btn-warning">Voeg foto's toe aan winkel <i class="fa fa-plus" aria-hidden="true"></i></a>--}}
                        {{--<a href="{{route('admin.shops.editPhotos',$shop->id)}}" class="btn btn-warning">Pas foto's aan <i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>--}}
                    {{--</td>--}}
                {{--</tr>--}}
            {{--@endforeach--}}
            {{--</tbody>--}}
        {{--</table>--}}

    </div>

@stop

@section('javascript')
    <script src="{{ Request::root() }}/assets/plugins/owl-carousel/owl.carousel.js"></script>
    <script>
        $(".carrousel-index").owlCarousel({
            autoPlay: true,
            singleItem: true,
            pagination: true
        });
    </script>
@stop
