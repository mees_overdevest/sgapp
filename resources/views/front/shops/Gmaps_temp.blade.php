@extends('layouts.app')

@section('content')
    {{--Voor de Google Maps voor winkelgebieden--}}
    {{--//http://codepen.io/jhawes/pen/ujdgK--}}
    {{--http://stackoverflow.com/questions/5072059/polygon-drawing-and-getting-coordinates-with-google-map-api-v3--}}
    <div class="container">

        <h1>{{ ucfirst(trans('areas/area.create.title')) }}</h1>



        <div class="btn-group">
            <button type="button" class="btn btn-default" onclick="initMap({{ $area->id }})">Open map</button>
        </div>

        <div id="map-canvas"></div>



    </div>

@stop

@section('javascript')
    {{--<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>--}}
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB8if5Pn4291xGseJe2OydAz35tf9xua_Y&libraries=drawing" async defer></script>
    <script>
        //var myPolygon;
        var points;
        var coordinates;
        function initMap(id){
            var map = new google.maps.Map(document.getElementById('map-canvas'), {
                center: {lat: 52.370216, lng: 4.895168},
                zoom: 8
            });

            var areaCoordinates = [];

            // Using the core $.ajax() method
            $.ajax({

                        // The URL for the request
                        url: "/admin/areas/getCoordinates/" + id,

                        // The data to send (will be converted to a query string)
//                data: {
//                    id: 123
//                },

                        // Whether this is a POST or GET request
                        type: "GET",

                        // The type of data we expect back
                        dataType : "json",
                    })
                    // Code to run if the request succeeds (is done);
                    // The response is passed to the function
                    .done(function( json ) {
//                console.log(json);

                        for(var obj in json){
//                    console.log(json[obj]);
                            areaCoordinates.push({lat: json[obj].lat, lng: json[obj].lng});
                        }
                        console.log(areaCoordinates);
                    })
                    // Code to run if the request fails; the raw request and
                    // status codes are passed to the function
                    .fail(function( xhr, status, errorThrown ) {
                        alert( "Sorry, there was a problem!" );
                        console.log( "Error: " + errorThrown );
                        console.log( "Status: " + status );
                        console.dir( xhr );
                    })
                    // Code to run regardless of success or failure;
                    .always(function( xhr, status ) {
                        alert( "The request is complete!" );
                        // Construct the polygon.
                        var areaPolygon = new google.maps.Polygon({
                            paths: areaCoordinates,
                            strokeColor: '#FF0000',
                            strokeOpacity: 0.8,
                            strokeWeight: 3,
                            fillColor: '#FF0000',
                            fillOpacity: 0.35
                        });
                        areaPolygon.setMap(map);
                    });






        }




    </script>
@stop