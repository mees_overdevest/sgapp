@extends('layouts.app')

@section('content')
    <div class="container" id="myArea">
        <div class="col-md-12">
            <div class="col-md-4 col-sm-12">
                <h3>Voeg het winkelgebied van uw winkel toe</h3>
            </div>
            <div class="col-md-2 col-sm-4">
                <label for="city">Stad</label>
                <input type="text" name="city" v-model="query_city">
                <button type="button" v-on:click="searchByCity">Zoek</button>
            </div>
            <div class="col-md-2 col-sm-4">
                <label for="title">Titel</label>
                <input type="text" name="title" v-model="query_title">
                <button type="button" v-on:click="searchByTitle">Zoek</button>
            </div>
        </div>
        <div v-show="showAlert"><p>Success</p></div>
        <table class="table table-bordered table-hover">
        	<thead>
        		<tr>
        			<th>Stad</th>
                    <th>Winkelgebied</th>
                    <th>Adres</th>
                    <th>Actie</th>
        		</tr>
        	</thead>
        	<tbody>
        		<tr v-for="area in areas">
        			<td>@{{ area.city }}</td>
                    <td>@{{ area.title }}</td>
                    <td>@{{ area.address }}</td>
                    <td>
                        <button type="button" v-on:click="selectArea(area.id)">Kies winkelgebied</button>
                    </td>
        		</tr>
        	</tbody>
        </table>
    </div>

    {{--<input v-on:keyUp="calculatePrices" v-model="order.amount"--}}
           {{--type="text" value="@{{ order.amount }}" name="amount[]">--}}
    {{--</td>--}}
    {{--<td>--}}
        {{--&euro;<input type="text" name="price[]" class="fieldHidden" readonly value="@{{ order.price }}">--}}
    {{--</td>--}}
    {{--<td>--}}
        {{--<a href="#" v-on:click="removeComponent(order)">--}}
            {{--Remove--}}
        {{--</a>--}}
        {{--<div class="newComponent" v-show="showNewComponent">--}}


            {{--toggleNewComponent: function() {--}}
            {{--if(this.showCopyComponent)--}}
            {{--this.toggleCopyComponent();--}}
            {{--this.showNewComponent = !this.showNewComponent;--}}
            {{--},--}}
            {{--toggleCopyComponent: function() {--}}
            {{--if(this.showNewComponent)--}}
            {{--this.toggleNewComponent();--}}
            {{--this.showCopyComponent = !this.showCopyComponent;--}}
            {{--},--}}
@stop

@section('javascript')
    <script src="/js/plugins/vue.js"></script>
    <script src="https://cdn.jsdelivr.net/vue.resource/0.9.3/vue-resource.min.js"></script>
    <script>

    new Vue({
        el: '#myArea',
        data: {
            areas: [],
            query_city: '',
            query_title: '',
            shopId: '{!! $shop->id !!}',
            alert: false
        },
        ready: function(){
            Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#token').getAttribute('content');
        },
        methods:{
            searchByCity: function(){
                this.emptyResults();
                var data = {
                    'city' : this.query_city
                };
                this.$http.post(BASE + '/auth/ajax/areas/search', data).then(function(response) {
                    response.data.map(this.addToAreas);
                    this.emptyForm();
                });
            },
            searchByTitle: function(){
                this.emptyResults();
                var data = {
                    'title' : this.query_title
                };
                this.$http.post(BASE + '/auth/ajax/areas/search', data).then(function(response) {
                    response.data.map(this.addToAreas);
                    this.emptyForm();
                });
            },
            emptyForm: function(){
                this.query_title = "";
                this.query_city = "";
            },
            addToAreas: function(area){
                this.areas.push(area);
            },
            emptyResults: function(){
                this.areas = [];
            },
            selectArea: function(id){
                var data = {
                    'areaId' : id,
                    'shopId' : this.shopId
                }

                this.$http.post(BASE + '/auth/ajax/areas/selectArea', data).then(function(response) {
                    console.log(response.data);
                    this.alert = !this.alert;
//                    response.data.map(this.addToAreas);
                    this.emptyForm();
                });
            }
        },
        computed: {
            showAlert: function(){
                if(this.alert){

                }
                return this.alert;
            }
        }
    })
    </script>
@stop
