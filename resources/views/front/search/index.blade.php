@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-lg-6">
                <div class="carrousel-wrap">
                    <h1>Zoekresultaten</h1>
                    <p>Resultaten voor {{ $term }}</p>
                    {{--                @include('includes.carrousels.homecarrousel', ['objects' => $shops, 'favouriteRoute' => 'shops'])--}}
                </div>
            </div>
        </div>

        <div class="row">
            @foreach($results as $key => $resultGroup)
                @if(count($resultGroup) != 0)
                    <div class="col-md-6">
                        <h2>Gevonden {{ ucfirst($key . 's') }}</h2>
                        <div class="list-group">
                            @foreach($resultGroup as $result)
                                @if($key == "shopCategory" || $key == 'PCategories')
                                <div class="list-group-item">
                                    <div class="pull-right">
                                        @include('includes.buttons.favourite', ['object' => $result, 'routeName' => $key])
                                    </div>
                                    <h4 class="list-group-item-heading"><a href="{{ route('guest.' . $key . '.show', $result->id) }}">{{ $result->title }}</a></h4>
                                    <p class="list-group-item-text">{{ $result->description }}</p>
                                </div>
                                @else

                                <div class="list-group-item">
                                    <div class="pull-right">
                                        @include('includes.buttons.favourite', ['object' => $result, 'routeName' => $key . 's'])
                                    </div>
                                    <h4 class="list-group-item-heading">
                                        <a href="{{ route('guest.' . $key . 's.show', $result->id) }}">
                                            {{ $result->title }}
                                        </a>
                                        @if($key == 'shop' || $key == 'area')
                                            in {{ $result->city }}
                                        @endif
                                    </h4>
                                    <p class="list-group-item-text">{{ $result->description }}</p>
                                </div>
                                @endif
                            @endforeach
                        </div>
                    </div>
                @endif
            @endforeach
        </div>

    </div>
@endsection

@section('javascript')

@stop
