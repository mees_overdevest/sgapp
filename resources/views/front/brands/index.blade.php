@extends('layouts.app')

@section('styles')
    <link rel="stylesheet" type="text/css" href="{{ Request::root() }}/assets/plugins/owl-carousel/owl.carousel.css">
    <link rel="stylesheet" type="text/css" href="{{ Request::root() }}/assets/plugins/owl-carousel/owl.theme.css">
@stop


@section('content')
    <div class="container">
        <h1>{{  ucfirst(trans('brands/brand.brands')) }}</h1>

        @foreach($brands as $brand)
            <div class="col-md-6">
                <h2>{{ $brand->name }}</h2>
                <a class="btn btn-info pull-right" href="{{ route('auth.brands.show', $brand->id) }}">Bekijk {{ $brand->title }}</a>
                <p class="lead">{{ $brand->description }}</p>
                @if($brand->pictures)
                    <div class="carrousel" class="owl-carousel owl-theme">
                        @foreach($brand->pictures as $image)
                            <div class="item"><img src="{{ \App\Helpers\ImageManager::getMobilePath($brand->id, $image) }}" alt="{{ $image->image_name }}"></div>
                        @endforeach
                    </div>
                @endif
            </div>
        @endforeach

    </div>
@stop


@section('javascript')
    <script src="{{ Request::root() }}/assets/plugins/owl-carousel/owl.carousel.js"></script>
    <script>
        $(".carrousel").owlCarousel({
            autoPlay: true,
            singleItem: true,
            pagination: true
        });
    </script>
@stop

