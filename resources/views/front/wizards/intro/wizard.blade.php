@extends('layouts.app')

@section('style')
    <script src="{{ Request::root() }}/js/bundle.js"></script>
@stop

@section('content')
    <div class="container">
        <div id="react-canvas"></div>
        
        <div id="react-footer"></div>
    </div>
@stop

@section('javascript')
		<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB8if5Pn4291xGseJe2OydAz35tf9xua_Y" async defer></script>
    <script src="{{ Request::root() }}/js/bundle.js"></script>
@stop