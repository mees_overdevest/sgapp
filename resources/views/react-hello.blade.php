@extends('layouts.app')

@section('content')
	<div id="react-canvas"></div>
@stop

@section('javascript')
    <script src="{{ Request::root() }}/js/components/public/bundle.js"></script>

@stop