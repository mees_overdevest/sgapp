@extends('layouts.email')

@section('content')
	<p>{{ trans('passwordMail.title') . ' ' . $name }},</p>
	
	<p>{{ trans('passwordMail.contentRegistration') }}</p>

	@include('emails.partials.button', [
		'link'		=> route('auth.activate.account', ['code'=>$code, 'mail'=>$mail]),
		'text'		=> trans('passwordMail.activateAccount')
	])

	@include('emails.partials.blockquote_dark', [
		'text' => trans('passwordMail.buttonBroken') . ' <a href="'.route('auth.activate.account', ['code'=>$code, 'mail'=>$mail]).'">'.
		route('auth.activate.account', ['code'=>$code, 'mail'=>$mail]) . '</a>' 
	])
@endsection