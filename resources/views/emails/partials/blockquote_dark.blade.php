<table cellpadding="0" cellspacing="0" border="0" style="background: #e9e9e9;padding: 20px;margin-bottom: 30px;width: 100%;">
	<tr>
		<td>
			<p style="font-size: 14px;font-weight: normal;margin-bottom: 10px;margin: 0; {{ (isset($bold) && $bold == true ? 'font-weight:bold;' : '') }}">
				{!! $text !!}
			</p>
		</td>
	</tr>
</table>