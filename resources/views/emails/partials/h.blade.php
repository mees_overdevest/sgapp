<?php
	switch($size) {
		case 1: 
			$fontSize = 34;
			break;
		case 2: 
			$fontSize = 26;
			break;
		case 3: 
			$fontSize = 20;
			break;
		default:
			$fontSize = 14;
			break;
	}
?>
<h{{ $size }} style="font-size: {{ $fontSize }}px;color: #EF7373 !important;font-family: 'Open Sans', sans-serif  !important;font-weight: 200 !important;line-height: 1.2em !important;margin: 40px 0 10px !important;">{{ $text }}</h{{ $size }}>