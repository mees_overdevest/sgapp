@extends('layouts.email')

@section('content')
	<p>{{ trans('emails/utility.greeting', ['name' => $name]) }},</p>
	<p>{{ trans('emails/account.new.activation') }}</p>

	@include('emails.partials.button', [
	'link'		=> route('auth.activate.account', ['code'=>$code, 'mail'=>$mail]),
	'text'		=> 'Activate account'])

	@include('emails.partials.blockquote_dark', [
		'text'			=> 'Does the button not work? Try to copy this link: <a href="'.route('auth.activate.account', ['code'=>$code, 'mail'=>$mail]).'">'.
		route('auth.activate.account', ['code'=>$code, 'mail'=>$mail]) . '</a>' 
	])
@endsection