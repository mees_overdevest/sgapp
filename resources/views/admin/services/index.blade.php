@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>{{  ucfirst(trans('services/service.services')) }}</h1>

        <a href="{{ route('admin.services.create') }}" class="btn btn-success">{{  ucfirst(trans('services/service.create.new')) }} <i class="fa fa-plus" aria-hidden="true"></i></a>

        <table id="tableServices" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
            <tr>
                <td>ID</td>
                <td>{{  ucfirst(trans('services/service.service')) }}</td>
                <td>{{  ucfirst(trans('general.actions.edit')) }}</td>
                <td>{{  ucfirst(trans('general.actions.delete')) }}</td>
                <td>Afbeeldingen</td>
            </tr>
            </thead>
            <tbody>
            @foreach($services as $service)
                <tr>
                    <td>{{ $service->id }}</td>
                    <td><a href="{{ route('admin.services.show', $service->id) }}">{{ $service->title }}</a></td>
                    <td><a href="{{ route('admin.services.edit', $service->id) }}"><i class="fa fa-pencil-square-o"></i></a></td>
                    <td>
                        {!! Form::open(array('route' => array('admin.services.destroy', $service->id), 'method' => 'DELETE')) !!}
                        <button class="btn btn-danger" data-confirm="Are you sure?">Verwijder <i class="fa fa-trash-o"></i></button>
                        {!! Form::close() !!}
                    </td>
                    <td>
                        <a href="{{route('admin.addPhotos',[$service->id, 'service'])}}" class="btn btn-warning">Voeg foto's toe aan service <i class="fa fa-plus" aria-hidden="true"></i></a>
                        <a href="{{route('admin.editPhotos',[$service->id, 'service'])}}" class="btn btn-warning">Pas foto's aan <i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@stop

@section('javascript')

@stop
