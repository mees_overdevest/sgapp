@extends('layouts.app')

@section('content')
    <div class="container">


        <h1>{{ trans('services/service.update.submit') }}</h1>
        {!! Form::model($service, ['method' => 'PATCH', 'route' => ['admin.services.update', $service->id]]) !!}
        <div class="form-group">
            {!! Form::label('title', trans('services/service.name')) !!}
            {!! Form::text('title', $service->title, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('description', trans('services/service.description')) !!}
            {!! Form::text('description', $service->description, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            @foreach ($categories as $category)
                @foreach ($category as $key => $value)
                    {!! Form::Label('category_id', ucfirst($value)) !!}
                    @if($service->hasCategory($key))
                        {!! Form::checkbox('category_id[]', $key, true, ['class' => 'form-control', 'checked' => 'true']) !!}
                    @else
                        {!! Form::checkbox('category_id[]', $key, false, ['class' => 'form-control']) !!}
                    @endif
                @endforeach
            @endforeach
        </div>

        <div class="form-group">
            {!! Form::submit(trans('services/service.update.submit'), ['class' => 'btn btn-primary form-control']) !!}
        </div>
        {!! Form::close() !!}
    </div>

@stop

@section('javascript')

@stop