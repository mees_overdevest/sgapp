@extends('layouts.app')

@section('content')
    <div class="container">


        <h1>{{ trans('services/service.create.new') }}</h1>
        <hr/>


        {!! Form::open(['route' => 'admin.services.store']) !!}
        @include('admin.services._form', ['submitButtonText'=> trans('services/service.create.submit') ])
        {!! Form::close() !!}

    </div>

@stop
