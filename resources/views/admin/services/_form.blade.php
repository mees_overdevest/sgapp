<div class="form-group">
    {!! Form::label('title', ucfirst(trans('services/service.name'))) !!}
    {!! Form::text('title', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('description', ucfirst(trans('services/service.description'))) !!}
    {!! Form::text('description', null, ['class' => 'form-control']) !!}
</div>

@foreach ($categories as $category)
    @foreach ($category as $key => $value)
        <div class="form-group">
            {{--{{ dd($value) }}--}}
            {!! Form::Label('category_id[]', ucfirst($value)) !!}
            {!! Form::checkbox('category_id[]', $key, false, ['class' => 'form-control']) !!}
        </div>
    @endforeach
@endforeach

<div class="form-group">
    {!! Form::submit($submitButtonText, ['class' => 'btn btn-primary form-control']) !!}
</div>