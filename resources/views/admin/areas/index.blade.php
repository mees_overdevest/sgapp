@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>Winkelgebieden</h1>

        <table id="tableShops" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
            <tr>
                <td>ID</td>
                <td>{{  ucfirst(trans('areas/area.area')) }}</td>
                <td>{{  ucfirst(trans('general.city')) }}</td>
                <td>{{  trans('general.actions.edit') }}</td>
                <td>{{  trans('general.actions.delete') }}</td>
                <td>Afbeeldingen</td>
            </tr>
            </thead>
            <tbody>
            @foreach($areas as $area)
                <tr>
                    <td>{{ $area->id }}</td>
                    <td><a href="{{ route('admin.areas.show', $area->id) }}">{{ $area->title }}</a></td>
                    <td>{{ $area->city }}</td>
                    <td><a href="{{ route('admin.areas.edit', $area->id) }}"><i class="fa fa-pencil-square-o"></i></a></td>
                    <td>
                        {!! Form::open(array('route' => array('admin.areas.destroy', $area->id), 'method' => 'DELETE')) !!}
                            <button class="btn btn-danger" data-confirm="Are you sure?">Delete <i class="fa fa-trash-o"></i></button>
                        {!! Form::close() !!}
                    </td>
                    <td>
                        <a href="{{route('admin.addPhotos',[$area->id, 'area'])}}" class="btn btn-warning">Voeg foto's toe aan gebied <i class="fa fa-plus" aria-hidden="true"></i></a>
                        <a href="{{route('admin.editPhotos',[$area->id, 'area'])}}" class="btn btn-warning">Pas foto's aan <i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

@stop

@section('footer')

@stop
