@extends('layouts.app')

@section('content')
    <div class="container">

        <h1>Winkelgebied {{ $area->title }} aanpassen</h1>
        {!! Form::model($area, ['method' => 'PATCH', 'route' => ['admin.areas.update', $area->id]]) !!}
        <div class="form-group">
            {!! Form::label('title', 'Naam van winkelgebied') !!}
            {!! Form::text('title',  $area->title,['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('description', 'Beschrijving') !!}
            {!! Form::text('description',$area->description ,['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::submit('Winkelgebied updaten', ['class' => 'btn btn-primary form-control']) !!}
        </div>
        {!! Form::close() !!}
    </div>
@stop

@section('footer')

@stop