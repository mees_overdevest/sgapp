@extends('layouts.app')

@section('content')
        {{--Voor de Google Maps voor winkelgebieden--}}
        {{--//http://codepen.io/jhawes/pen/ujdgK--}}
        {{--http://stackoverflow.com/questions/5072059/polygon-drawing-and-getting-coordinates-with-google-map-api-v3--}}
        <div class="container">

        <h1>{{ ucfirst(trans('areas/area.create.title')) }}</h1>

        {!! Form::open(['route' => 'admin.areas.store']) !!}

                <div class="form-group">
                        {!! Form::label('title', ucfirst(trans('areas/area.area'))) !!}
                        {!! Form::text('title', null, ['class' => 'form-control']) !!}
                </div>

                <div class="form-group">
                        {!! Form::label('city', ucfirst(trans('general.city'))) !!}
                        {!! Form::text('city', null, ['class' => 'form-control']) !!}
                </div>

                <div class="form-group">
                        {!! Form::label('address', ucfirst(trans('general.address'))) !!}
                        {!! Form::text('address', null, ['class' => 'form-control']) !!}
                </div>

                <div class="btn-group">
                        <button type="button" class="btn btn-default" onclick="initMap()">Open map</button>
                </div>

                <div id="map-canvas"></div>

                {!! Form::hidden('coordinates', '', ['class' => 'form-control', 'id' => 'coordinates']) !!}


                <div class="form-group">
                        {!! Form::submit('Save this Area', ['class' => 'btn btn-primary form-control']) !!}
                </div>

        {!! Form::close() !!}

                <button id="CoordsButton" onclick="showPoints()">Coordinates</button>



        <div class="lngLat"><span class="one">Lat</span><span class="two">,Lng</span></div>

        <button id="clipboard-btn" onclick="copyToClipboard(document.getElementById('info').innerHTML)">Copy to Clipboard</button>
        <textarea id="info"></textarea>
        <textarea id="firstinfo"></textarea>


        </div>

@stop

@section('javascript')
        {{--<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>--}}
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB8if5Pn4291xGseJe2OydAz35tf9xua_Y&libraries=drawing" async defer></script>
        <script>
                //var myPolygon;
                var points;
                var coordinates;
                function initMap(){
                        var map = new google.maps.Map(document.getElementById('map-canvas'), {
                                center: {lat: 52.370216, lng: 4.895168},
                                zoom: 8
                        });

                        var drawingManager = new google.maps.drawing.DrawingManager({
                                drawingMode: google.maps.drawing.OverlayType.POLYGON,
                                drawingControl: true,
                                drawingControlOptions: {
                                        position: google.maps.ControlPosition.TOP_CENTER,
                                        drawingModes: [
                                                google.maps.drawing.OverlayType.POLYGON,
                                        ]
                                },
                                polygonOptions: {
                                        editable: true,
                                }
                        });
                        drawingManager.setMap(map);

                        google.maps.event.addListener(drawingManager, 'overlaycomplete', function (event) {
                                if (event.type != google.maps.drawing.OverlayType.MARKER) {
                                        drawingManager.setDrawingMode(null);
                                        // assuming you want the points in a div with id="info"
//                                        overlayClickListener(event.overlay);
                                        var newShape = event.overlay;
                                        newShape.type = event.type;
                                        document.getElementById('info').innerHTML += "polygon points:" + "<br>";
                                        for (var i = 0; i < newShape.getPath().getLength(); i++) {
                                                document.getElementById('info').innerHTML += newShape.getPath().getAt(i).toUrlValue(6) + "<br>";
                                        }

                                }
                                overlayClickListener(event.overlay);
                                console.log("called complete");
                        });

                        function overlayClickListener(overlay) {
                                this.points = overlay.getPath().getArray();

                                document.getElementById('firstinfo').value = "";
                                document.getElementById('firstinfo').value = overlay.getPath().getArray();

                                google.maps.event.addListener(overlay, "mouseup", function(event){
                                        getPoints(overlay);

                                });
                        }

                        function getPoints(overlay) {
                                document.getElementById('firstinfo').value = "";
                                document.getElementById('firstinfo').value = overlay.getPath().getArray();
                                this.coordinates = overlay.getPath().getArray();
                                console.log(this.coordinates);
                        }
                }

                function showPoints(){
                        document.getElementById('coordinates').value = "";
                        document.getElementById('coordinates').value = this.points;
                }


        </script>
@stop