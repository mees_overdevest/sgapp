@extends('layouts.app')

@section('content')
    <div class="container">


        <h1>{{ trans('shops/category.actions.create') }}</h1>
        <hr/>

        {!! Form::open(['route' => 'admin.shopCategory.store']) !!}
        @include('admin.shopCategories._form', ['submitButtonText'=> trans('shops/category.actions.save')])
        {!! Form::close() !!}

    </div>

@stop



@section('javascript')
    <script>
    $(document).ready(function() {
        $("#show-me").css("display", "none");
        $("#subcat").prop("checked", false);

        $("#subcat").change(function(){
            if(this.checked){
                $("#show-me").css("display", "block");
            } else {
                $("#show-me").css("display", "none");
            }
        })
    });
    </script>
@stop