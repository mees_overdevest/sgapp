@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>Edit de categorie {{ $shopCategory->title }}</h1>
        <hr/>
        {!! Form::model($shopCategory, ['method' => 'PATCH', 'route' => ['admin.shopCategory.update', $shopCategory->id]]) !!}
        <div class="form-group">
            {!! Form::label('title', 'Titel:') !!}
            {!! Form::text('title', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('description', 'Beschrijving:') !!}
            {!! Form::text('description', null, ['class' => 'form-control']) !!}
        </div>

        @if($shopCategory->parent_id != null)
            <div class="form-group" id="show-me">
                {!! Form::label('parent_category', 'Maak subcategorie:') !!}
                {!! Form::select('parent_category', $shopCategories, $shopCategory->parent->id, ['class' => 'form-control']) !!}
            </div>
        @else
            <div class="form-group" id="show-me">
                {!! Form::label('parent_category', 'Hoofdcategorieën:') !!}
                {!! Form::select('parent_category', $shopCategories, null, ['class' => 'form-control']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('headcat', 'Maak subcategorie?') !!}
                {!! Form::checkbox('headcat', 1, ['class' => 'form-control']) !!}
            </div>
        @endif

        <div class="form-group">
            {!! Form::submit('Update categorie', ['class' => 'btn btn-primary form-control']) !!}
        </div>
        {!! Form::close() !!}
    </div>

@stop

@section('javascript')
    <script>
        $(document).ready(function() {
            $("#show-me").css("display", "none");
            $("#headcat").prop("checked", false);

            $("#headcat").change(function(){
                if(this.checked){
                    $("#show-me").css("display", "block");
                } else {
                    $("#show-me").css("display", "none");
                }
            })
        });
    </script>
@stop