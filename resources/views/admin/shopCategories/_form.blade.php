<div class="form-group">
    {!! Form::label('title', 'Titel:') !!}
    {!! Form::text('title', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('description', 'Beschrijving:') !!}
    {!! Form::text('description', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('subcat', 'Is dit een subcategorie?') !!}
    {!! Form::checkbox('subcat', 1, ['class' => 'form-control']) !!}
</div>

<div class="form-group" id="show-me">
    {!! Form::label('parent_category', 'Hoofdcategorieën:') !!}
    {!! Form::select('parent_category', $shopCategories, null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::submit($submitButtonText, ['class' => 'btn btn-primary form-control']) !!}
</div>