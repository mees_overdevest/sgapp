@extends('layouts.app')

@section('content')
    <div class="container">

        <h1>Edit de categorie {{ $PCategory->title }}</h1>
        <hr/>
        {!! Form::model($PCategory, ['method' => 'PATCH', 'route' => ['admin.PCategories.update', $PCategory->id]]) !!}
        <div class="form-group">
            {!! Form::label('title', 'Titel:') !!}
            {!! Form::text('title', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('description', 'Beschrijving:') !!}
            {!! Form::text('description', null, ['class' => 'form-control']) !!}
        </div>

        {{--@if($PCategory->parent_id == null)--}}
            {{--<div class="form-group" id="show-me">--}}
                {{--{!! Form::label('parent_category', 'Maak subcategorie:') !!}--}}
                {{--{!! Form::select('parent_category', $PCategories, null, ['class' => 'form-control']) !!}--}}
            {{--</div>--}}
        {{--@else--}}
            {{--<div class="form-group" id="show-me">--}}
                {{--{!! Form::label('parent_category', 'Hoofdcategorieën:') !!}--}}
                {{--{!! Form::select('parent_category', $PCategories, $PCategory->parent->id, ['class' => 'form-control']) !!}--}}
            {{--</div>--}}
            {{--<div class="form-group">--}}
                {{--{!! Form::label('headcat', 'Maak hoofdcategorie?') !!}--}}
                {{--{!! Form::checkbox('headcat', 1, ['class' => 'form-control']) !!}--}}
            {{--</div>--}}
        {{--@endif--}}

        <div class="form-group">
            {!! Form::submit('Update categorie', ['class' => 'btn btn-primary form-control']) !!}
        </div>
        {!! Form::close() !!}

    </div>

@stop

@section('javascript')
    <script>
//        $(document).ready(function() {
//            $("#show-me").css("display", "none");
            $("#headcat").prop("checked", false);
//
//            $("#subcat").change(function(){
//                if(this.checked){
//                    $("#show-me").css("display", "block");
//                } else {
//                    $("#show-me").css("display", "none");
//                }
//            })
//        });
    </script>
@stop