@extends('layouts.app')

@section('content')
    <h1>Aanmaken van nieuwe productcategorieën</h1>
    <hr/>

    {!! Form::open(['route' => 'admin.PCategories.store']) !!}
    @include('admin.PCategories._form', ['submitButtonText'=>'Maak productcategorie aan'])
    {!! Form::close() !!}



@stop



@section('javascript')
    <script>
    $(document).ready(function() {
        $("#show-me").css("display", "none");
        $("#subcat").prop("checked", false);

        $("#subcat").change(function(){
            if(this.checked){
                $("#show-me").css("display", "block");
            } else {
                $("#show-me").css("display", "none");
            }
        })
    });
    </script>
@stop