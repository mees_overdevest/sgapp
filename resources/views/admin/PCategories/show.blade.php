@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>Categorie {{ $PCategory->title }}</h1>
        <h3>Links die horen bij deze categorie</h3>
        <p>{{ $PCategory->description }}</p>
        <a href='{{ route('admin.PCategories.edit', [$PCategory->id]) }}' class="btn btn-success">Edit productcategorie</a>
    </div>

@stop

@section('footer')

@stop
