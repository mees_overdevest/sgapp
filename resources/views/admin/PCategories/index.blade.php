@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>Product Categorieën</h1>

        <div class="pull-right">
            {!! Form::open(['route' => 'admin.PCategories.uploadExcel', 'files' => true]) !!}

            {!! Form::label('excel', 'Excelsheet uploaden') !!}
            {!! Form::file('excel') !!}

            {!! Form::submit('Excel uploaden', ['class' => 'btn btn-primary']) !!}

            {!! Form::close() !!}
        </div>

        <a href="{{ route('admin.PCategories.create') }}" class="btn btn-success">{{  ucfirst(trans('PCategories/PCategory.create.new')) }} <i class="fa fa-plus" aria-hidden="true"></i>
        </a>

        <br>

        <table class="table table-bordered table-hover">
            <thead>
            <tr>
                <th>Categorieën</th>
                <th>Beschrijving</th>
                <th>Overkoepelende categorie</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($PCategories as $category)
                <tr>
                    <td><a href='{{ route('admin.PCategories.show', [$category->id]) }}'>{{ $category->title }}</a></td>
                    <td>{{ $category->description }}</td>
                    <td><p class="text-danger">Hoofdcategorie</p></td>
                    <td><a href='{{ route('admin.PCategories.edit', [$category->id]) }}' class="btn btn-success">Edit categorie <i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></td>
                @foreach ($category->children as $children)
                    <tr>
                        <td><a href='{{ route('admin.PCategories.show', [$children->id]) }}'>{{ $children->title }}</a></td>
                        <td>{{ $children->description }}</td>
                        <td>{{ $children->parent->title }}</td>
                        <td><a href='{{ route('admin.PCategories.edit', [$children->id]) }}' class="btn btn-success">Edit categorie <i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></td>
                    </tr>
                    @foreach ($children->children as $child)
                        <tr>
                            <td><a href='{{ route('admin.PCategories.show', [$child->id]) }}'>{{ $child->title }}</a></td>
                            <td>{{ $child->description }}</td>
                            <td>{{ $child->parent->title }}</td>
                            <td><a href='{{ route('admin.PCategories.edit', [$child->id]) }}' class="btn btn-success">Edit categorie <i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></td>
                        </tr>
                    @endforeach
                @endforeach
                    </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <!-- Test -->
@stop

@section('javascript')

@stop
