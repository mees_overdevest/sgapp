@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>Logs</h1>
        <p class="lead">Individuele logs worden op deze pagina opgesomd.</p>
        @include('admin.logs.partial._nav')

        <table id="tableUsers" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
            <tr>
                <td>Datum</td>
                <td>Type</td>
                <td>Gebruikers</td>
            </tr>
            </thead>
            <tbody>
            @foreach($logs as $log)
                <tr>
                    <td>{{ $log->created_at }}</td>
                    <td>{{ $log->type }}</td>
                    <td>{{ $log->user->fullName() }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection

