@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>{{ $title }} Logs</h1>
        <p class="lead">Individuele logs worden op deze pagina opgesomd.</p>
        @include('admin.logs.partial._nav')

        <table id="tableUsers" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
            <tr>
                <td>Titel</td>
                <td>Aangemaakt op</td>
                <td>Aantal keer bekeken</td>
                <td>Acties</td>
            </tr>
            </thead>
            <tbody>
            @foreach($logs as $log)
                <tr>
                    <td>{{ $log->title }}</td>
                    <td>{{ $log->created_at }}</td>
                    <td>{{ $log->getLog()->count() }}</td>
                    <td><a href="{{ route('admin.logs.objectTypeIndex', ['type' => $type, 'id' => $log->id]) }}">Bekijk logs</a></td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection

