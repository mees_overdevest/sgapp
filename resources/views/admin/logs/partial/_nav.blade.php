<a href="{{ route('admin.logs.typeIndex', 'shop') }}" class="btn btn-primary">Get Shop Logs</a>
<a href="{{ route('admin.logs.typeIndex', 'service') }}" class="btn btn-primary">Get Service Logs</a>
<a href="{{ route('admin.logs.typeIndex', 'product') }}" class="btn btn-primary">Get Product Logs</a>
<a href="{{ route('admin.logs.typeIndex', 'area') }}" class="btn btn-primary">Get Area Logs</a>
<a href="{{ route('admin.logs.typeIndex', 'brand') }}" class="btn btn-primary">Get Brand Logs</a>