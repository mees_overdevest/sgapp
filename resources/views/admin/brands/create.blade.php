@extends('layouts.app')

@section('content')
    <div class="container">


        <h1>{{ trans('brands/brand.create.new') }}</h1>
        <hr/>


        {!! Form::open(['route' => 'admin.brands.store']) !!}
        @include('admin.brands._form', ['submitButtonText'=> trans('brands/brand.create.submit') ])
        {!! Form::close() !!}

    </div>

@stop
