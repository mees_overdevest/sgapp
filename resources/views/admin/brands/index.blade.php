@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>{{  ucfirst(trans('brands/brand.brands')) }}</h1>

        <a href="{{ route('admin.brands.create') }}" class="btn btn-success">{{  ucfirst(trans('brands/brand.create.new')) }} <i class="fa fa-plus" aria-hidden="true"></i>
        </a>

        <table id="tableBrands" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
            <tr>
                <td>ID</td>
                <td>{{  ucfirst(trans('brands/brand.brand')) }}</td>
                <td>{{  trans('general.actions.edit') }}</td>
                <td>{{  trans('general.actions.delete') }}</td>
            </tr>
            </thead>
            <tbody>
            @foreach($brands as $brand)
                <tr>
                    <td>{{ $brand->id }}</td>
                    <td><a href="{{ route('admin.brands.show', $brand->id) }}">{{ $brand->title }}</a></td>
                    <td><a href="{{ route('admin.brands.edit', $brand->id) }}"><i class="fa fa-pencil-square-o"></i></a></td>
                    <td>
                        {{--<a href="{{ route('admin.brands.destroy', $brand->id) }}" data-method="delete" data-token="{{ csrf_token() }}" data-confirm="Are you sure?"><i class="fa fa-trash-o"></i></a>--}}
                        {!! Form::open(array('route' => array('admin.brands.destroy', $brand->id), 'method' => 'DELETE')) !!}
                            <button class="btn btn-danger">Delete <i class="fa fa-trash-o"></i></button>
                        {!! Form::close() !!}
                            <a href="{{route('admin.addPhotos',[$brand->id, 'brand'])}}" class="btn btn-warning">Voeg foto's toe aan merk</a>
                        <a href="{{route('admin.editPhotos',[$brand->id, 'brand'])}}" class="btn btn-warning">Pas foto's aan <i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@stop

@section('footer')

@stop
