@extends('layouts.app')

@section('styles')
    <link rel="stylesheet" type="text/css" href="{{ Request::root() }}/assets/plugins/owl-carousel/owl.carousel.css">
    <link rel="stylesheet" type="text/css" href="{{ Request::root() }}/assets/plugins/owl-carousel/owl.theme.css">
@stop

@section('content')
    <div class="container">
        <h1>Link: {{ $brand->title }}</h1>
        <h3>Informatie over dit merk staat op deze pagina</h3>
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
            <h3>Beschrijving</h3>
            <p>{{ $brand->description }}</p>
        </div>

        @if($brand->pictures)
            <div id="carrousel" class="owl-carousel owl-theme">

            @foreach($brand->pictures as $image)
                    <div class="item"><img src="{{ \App\Helpers\ImageManager::getMobilePath($brand->id, $image) }}" alt="{{ $image->image_name }}"></div>

            @endforeach
            </div>
        @endif
    </div>

@stop

@section('javascript')
    <script src="{{ Request::root() }}/assets/plugins/owl-carousel/owl.carousel.js"></script>
    <script>
        $("#carrousel").owlCarousel({
            autoPlay: true,
            singleItem: true,
            pagination: true
        });
    </script>
@stop
