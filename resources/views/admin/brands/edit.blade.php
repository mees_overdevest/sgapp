@extends('layouts.app')

@section('content')
    <div class="container">

        <h1>Merk {{ $brand->title }} aanpassen</h1>
        {!! Form::model($brand, ['method' => 'PATCH', 'route' => ['admin.brands.update', $brand->id]]) !!}
        <div class="form-group">
            {!! Form::label('title', 'Merknaam') !!}
            {!! Form::text('title',  $brand->title,['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('description', 'Beschrijving') !!}
            {!! Form::text('description',$brand->description ,['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::submit('Merk updaten', ['class' => 'btn btn-primary form-control']) !!}
        </div>
        {!! Form::close() !!}
    </div>
@stop

@section('footer')

@stop