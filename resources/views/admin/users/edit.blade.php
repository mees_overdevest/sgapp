@extends('layouts.app')

@section('content')
    <div class="container">
        {!! Form::open(['route' => ['admin.users.update', $user->id], 'method' => 'put']) !!}
        {!! Form::hidden('id', $user->id) !!}
        <div class="form-group">
            {!! Form::label('email', 'E-mail') !!}
            {!! Form::text('email', $user->email, ['class'=>'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('firstName', ucfirst(trans('admin/users.form.firstName'))) !!}
            {!! Form::text('firstName', $user->firstName, ['class'=>'form-control']) !!}
        </div>

         <div class="form-group">
            {!! Form::label('lastName', ucfirst(trans('admin/users.form.lastName'))) !!}
            {!! Form::text('lastName', $user->lastName, ['class'=>'form-control']) !!}
        </div>

        <div class="form-group">
            @foreach ($roles as $role)
                {!! Form::Label('name', ucfirst($role->name)) !!}
                    @if($user->hasRole($role->name))
                        {!! Form::checkbox('role[]', true, null, ['class' => 'form-control', 'checked' => 'true']) !!}
                    @else
                     {!! Form::checkbox('role[]', false, null, ['class' => 'form-control']) !!}
                    @endif
                @endforeach
        </div>


            <div class="form-group">
                {!! Form::label('language_id', ucfirst(trans('admin/users.form.language'))) !!}
                {!! Form::select('language_id', $languages, null, ['class'=>'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('shop_id', ucfirst(trans('shops/shop.name'))) !!}
                {!! Form::select('shop_id', $shops, null, ['class'=>'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('active', ucfirst(trans('admin/users.form.active'))) !!}
                {!! Form::select('active', $states, $user->active, ['class'=>'form-control']) !!}
            </div>

            {!! Form::submit(ucfirst(trans('admin/users.form.update'))) !!}

            {!! Form::close() !!}
        </div>

    @endsection