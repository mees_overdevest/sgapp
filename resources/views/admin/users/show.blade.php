@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>Gebruiker {{ $user->fullName() }}</h1>

        <table class="table">
            <tr>
                <td>{{ ucfirst('Email') }}</td>
                <td>{{ $user->email }} </td>
            </tr>
            <tr>
                <td>{{ ucfirst('Voornaam') }}</td>
                <td>{{ ucfirst($user->firstName) }} </td>
            </tr>

            <tr>
                <td>{{ ucfirst('Achternaam') }}</td>
                <td>{{ ucfirst($user->lastName) }} </td>
            </tr>

            <tr>
                <td>Acties</td>
                <td>
                    <a class="btn btn-info" href="{{ route('admin.users.edit', $user->id) }}">
                        <i class="fa fa-pencil-square-o"></i> Gebruiker aanpassen
                    </a>
                </td>
            </tr>
        </table>
    </div>

@endsection