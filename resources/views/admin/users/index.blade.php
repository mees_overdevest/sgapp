@extends('layouts.app')

@section('content')
    <div class="container">
     <table id="tableUsers" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
          <tr>
                <td>{{  ucfirst(trans('general.name')) }}</td>
                <td>{{  ucfirst(trans('general.email')) }}</td>
                <td>{{ ucfirst(trans('shops/shop.name')) }}</td>
                <td>{{  ucfirst(trans('general.actions.edit')) }}</td>
                <td>{{  ucfirst(trans('general.actions.delete')) }}</td>
                <td>{{  ucfirst(trans('general.actions.reset-password')) }}</td>
          </tr>
        </thead>
        <tbody>
          @foreach($users as $user)
              <tr>
                  <td><a href="{{ route('admin.users.show', $user->id) }}">{{ $user->fullname() }}</a></td>
                  <td>{{ $user->email }}</td>
                  <td>
                      @if($user->shops())
{{--                          {{ dd($user->owner()) }}--}}
                          @foreach($user->shops as $shop)
                              {{--{{ $shop->id }} | {{ $shop->name }}<br>--}}
                              {{ ucfirst(trans('general.actions.show')) }} : <a href="{{ route('admin.shops.show', $shop->id) }}">{{ $shop->title }}</a> in {{ $shop->city }}<br>
                          @endforeach
                      @endif
                  </td>
                  <td><a href="{{ route('admin.users.edit', $user->id) }}"><i class="fa fa-pencil-square-o"></i></a></td>
                  <td><a href="{{ route('admin.users.destroy', $user->id) }}" data-method="delete" data-token="{{ csrf_token() }}" data-confirm="Are you sure?"><i class="fa fa-trash-o"></i></a>
                  </td>
                  <td><a href="{{ route('admin.users.resetpassword', $user->id) }}"><i class="fa fa-envelope-o" aria-hidden="true"></i></a></td>
              </tr>
          @endforeach
        </tbody>
    </table>
</div>
@endsection

