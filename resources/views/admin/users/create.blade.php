@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    {!! Form::open(['route' => 'admin.users.store']) !!}

                    <div class="form-group">
                        {!! Form::label('email', 'E-mail') !!}
                        {!! Form::text('email', '', ['class'=>'form-control']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('firstName', ucfirst(trans('admin/users.form.firstName'))) !!}
                        {!! Form::text('firstName', '', ['class'=>'form-control']) !!}
                    </div>

                     <div class="form-group">
                        {!! Form::label('lastName', ucfirst(trans('admin/users.form.lastName'))) !!}
                        {!! Form::text('lastName', '', ['class'=>'form-control']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('role_id', ucfirst(trans('admin/users.form.role'))) !!}
                        {!! Form::select('role_id', $roles, '', ['class'=>'form-control']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('language_id', ucfirst(trans('admin/users.form.language'))) !!}
                        {!! Form::select('language_id', $languages, '', ['class'=>'form-control']) !!}
                    </div>


                    {!! Form::submit(ucfirst(trans('admin/users.form.save')), ['class'=>'btn btn-info']) !!}

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
       
    </div>
@endsection