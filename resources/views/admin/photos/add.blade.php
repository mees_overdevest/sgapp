@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="col-md-8" style="display:block">
            <h1>Foto's toevoegen {{ $object->getImageText('detail') }}</h1>
        </div>
        <div class="col-md-4">
            <a href="{{route('admin.editPhotos',[$object->id, $type])}}" class="btn btn-warning">Voeg foto's toe <i class="fa fa-plus" aria-hidden="true"></i>
            </a>
        </div>
        {!! Form::open(['route' => ['admin.addPhotosPost','id' => $object->id, 'type' => $type],'files' => true, 'method' => 'post']) !!}
        <div class="form-group">
            {!! Form::label('image name', 'Image name:') !!}
            {!! Form::text('image_name[]', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('image', 'Primary Image') !!}
            {!! Form::file('image[]', null, array('required', 'class'=>'form-control')) !!}
        </div>

        <div class="form-group">
            {!! Form::label('image_description', ucfirst(trans('pictures/picture.description'))) !!}
            {!! Form::text('image_description[]', null, ['class' => 'form-control']) !!}
        </div>

        <div class="afterthis"></div>

        <button id="b1" class="btn add-more" type="button">+</button>


        <div class="form-group">
            {!! Form::submit('Save Photos', ['class' => 'btn btn-primary form-control']) !!}
        </div>

        {!! Form::close() !!}
    </div>
@stop

@section('javascript')
    <script>
        $(document).ready(function(){
            var next = 1;
            $(".add-more").click(function(e){
                e.preventDefault();
                var addto = $('.afterthis');
//                var addRemove = "#field" + (next);
                next = next + 1;

                var newImgDescription = '<label for="image_description">Description for this image</label><input class="input form-control" type="text" name="image_description[]">';
                var newImgDescriptionInput = $(newImgDescription);

                var newImg = '<label for="image">Image</label><input type="file" name="image[]">';
                var newImgInput = $(newImg);

                var newImgName = '<label for="image_name">Image name</label><input class="input form-control" type="text" name="image_name[]">';
                var newImgNameInput = $(newImgName);

                $(addto).after(newImgNameInput);
                $(addto).after(newImgInput);
                $(addto).after(newImgDescriptionInput);
//                $(addRemove).after(removeButton);
//                $("#field" + next).attr('data-source',$(addto).attr('data-source'));
//                $("#count").val(next);

                $('.remove-me').click(function(e){
                    e.preventDefault();
                    var fieldNum = this.id.charAt(this.id.length-1);
                    var fieldID = "#field" + fieldNum;
                    $(this).remove();
                    $(fieldID).remove();
                });
            });



        });
    </script>
@stop
