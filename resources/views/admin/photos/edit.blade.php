@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="col-md-8" style="display:block">
            <h1>Aanpassen van foto's {{ $object->getImageText('edit') }}</h1>
        </div>
        <div class="col-md-4">
            <a href="{{route('admin.addPhotos',[$object->id, $type])}}" class="btn btn-warning">Voeg foto's toe <i class="fa fa-plus" aria-hidden="true"></i>
            </a>
        </div>

        {!! Form::open(['route' => ['admin.editPhotosPost', 'id' => $object->id, 'type' => $type],'files' => true, 'method' => 'post']) !!}

        @if($object->pictures)
            @foreach($object->pictures as $image)
                <div class="col-md-12 centered">
                    <h2><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                        Foto: {{ $image->image_name }} {{ $object->getImageText('detail') }}</h2>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-6">
                    <div class="carrousel" class="owl-carousel owl-theme">
                        <div class="item">
                            <div class="textoverlay">
                                <div class="col-md-6 col-sm-6 col-xs-6">
                                    <h3>{{ $image->image_name }}</h3>
                                    <p>{{ $image->description }}</p>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-6">
                                    <h4>Opties</h4>
                                    <a class="btn btn-danger" href="{{ route('admin.deletePhoto', [$object->id, $image->id, $type]) }}">Verwijder <i class="fa fa-trash" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>
                            <img src="{{ \App\Helpers\ImageManager::getMobilePath($object->id, $image) }}" alt="{{ $image->image_name }}">
                        </div>
                    </div>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-6">

                    <div class="form-group">
                        {!! Form::label('image name', 'Image name:') !!}
                        {!! Form::text('image_name['.$image->id.']', $image->image_name, ['class' => 'form-control']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('image_description', ucfirst(trans('pictures/picture.description'))) !!}
                        {!! Form::text('image_description['.$image->id.']', $image->description, ['class' => 'form-control']) !!}
                    </div>
                </div>
                <div class="clearfix"></div>
            @endforeach
        @endif


        <div class="form-group">
            {!! Form::submit('Update Photos', ['class' => 'btn btn-primary form-control']) !!}
        </div>

        {!! Form::close() !!}


    </div>

@stop

@section('javascript')
    <script src="{{ Request::root() }}/assets/plugins/owl-carousel/owl.carousel.js"></script>
    <script>
        $(".carrousel").owlCarousel({
            autoPlay: true,
            singleItem: true,
            pagination: true
        });
    </script>
@stop

