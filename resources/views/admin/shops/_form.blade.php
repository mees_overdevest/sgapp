<div class="form-group">
    {!! Form::label('title', ucfirst(trans('shops/shop.name'))) !!}
    {!! Form::text('title', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('description', ucfirst(trans('shops/shop.description'))) !!}
    {!! Form::text('description', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('since', ucfirst(trans('shops/shop.since'))) !!}
    {!! Form::text('since', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('city', ucfirst(trans('general.city'))) !!}
    {!! Form::text('city', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('address', ucfirst(trans('general.address'))) !!}
    {!! Form::text('address', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group" id="show-me">
    {!! Form::label('category_id', ucfirst(trans('PCategories/PCategory.PCategory'))) !!}
    {!! Form::select('category_id', $categories) !!}
</div>

<div class="form-group">
    {!! Form::submit($submitButtonText, ['class' => 'btn btn-primary form-control']) !!}
</div>