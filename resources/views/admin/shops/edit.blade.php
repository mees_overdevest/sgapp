@extends('app')

@section('content')
    <h1>Winkel aanpassen</h1>
    {!! Form::model($shop, ['method' => 'PATCH', 'route' => ['admin.shops.update', $shop->id]]) !!}
    <div class="form-group">
        {!! Form::label('title', 'Titel:') !!}
        {!! Form::text('title', $shop->title, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('description', 'Beschrijving:') !!}
        {!! Form::text('description', $shop->description, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('since', 'Startdatum:') !!}
        {!! Form::text('since', $shop->since, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('address', 'Adres:') !!}
        {!! Form::text('address', $shop->address, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('city', 'Stad:') !!}
        {!! Form::text('city', $shop->city, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group">
        {!! Form::submit('Update Winkel', ['class' => 'btn btn-primary form-control']) !!}
    </div>
    {!! Form::close() !!}

@stop

@section('footer')

@stop