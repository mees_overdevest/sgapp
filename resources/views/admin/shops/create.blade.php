@extends('layouts.app')

@section('content')
    <div class="container">

        <h1>{{ trans('shops/wizard.create.title') }}</h1>
        <hr/>


        {!! Form::open(['route' => 'admin.shops.store','files' => true]) !!}
        @include('admin.shops._form', ['submitButtonText'=> trans('shops/wizard.create.submit') ])
        {!! Form::close() !!}
    </div>



@stop
