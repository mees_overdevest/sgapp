@extends('layouts.app')

@section('styles')
    <link rel="stylesheet" type="text/css" href="{{ Request::root() }}/assets/plugins/owl-carousel/owl.carousel.css">
    <link rel="stylesheet" type="text/css" href="{{ Request::root() }}/assets/plugins/owl-carousel/owl.theme.css">
@stop

@section('content')
    <div class="container">
        <h1>Shop: {{ $shop->name }}</h1>
        @if(auth()->check() && auth()->user()->hasRole('admin'))
            <p><a href='{{ route('admin.shops.edit', [$shop->id]) }}' class="btn btn-success">Update deze Service!</a></p>
        @endif
        <?php $i = 1; ?>
        <div class="col-sm-6 col-md-6 col-lg-6">
            <h3>Beschrijving</h3>
            <p>{{ $shop->description }}</p>
        </div>

        @if($shop->pictures)
            <div class="col-sm-6 col-md-6 col-lg-6">
                <div class="carrousel" class="owl-carousel owl-theme">
                    @foreach($shop->pictures as $image)
                        <div class="item"><img src="{{ \App\Helpers\ImageManager::getMobilePath($shop->id, $image) }}" alt="{{ $image->image_name }}"></div>

                    @endforeach
                </div>
            </div>
            {{-- Update counter for div clearance --}}
            <?php $i++; ?>
        @endif

        @if($i === 2)
            <div class="clearfix"></div>
        @endif

        @if($shop->categories->count() > 0)
            <div class="col-sm-6 col-md-6 col-lg-6">
                <h3>Product Categorieën</h3>
                <table id="tableProducts" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <td>Titel</td>
                        <td>Beschrijving</td>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($shop->categories as $shopCategory)
                        <tr>
                            <td><a href="{{ route('auth.shopCategory.show',$shopCategory->id) }}">{{ $shopCategory->title }}</a></td>
                            <td>{{ $shopCategory->description }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            {{-- Update counter for div clearance --}}
            <?php $i++; ?>
        @endif

        @if($i === 2)
            <div class="clearfix"></div>
        @endif

        @if($shop->products->count() > 0)
            <div class="col-sm-6 col-md-6 col-lg-6">
                <h2>Producten</h2>
                <p>Alle producten die tot deze categorie toe behoren.</p>
                @foreach($shop->products as $product)
                    <h3><a href="{{ route('auth.products.show', $product->id) }}">{{ $product->title }}</a></h3>
                    <p>{{ $product->description }}</p>
                @endforeach
            </div>
            {{-- Update counter for div clearance --}}
            <?php $i++; ?>
        @endif

        @if($i === 4 || $i === 2)
            <div class="clearfix"></div>
        @endif

        @if($shop->services->count() > 0)
            <div class="col-sm-6 col-md-6 col-lg-6">
                <h2>Diensten</h2>
                <p>Alle diensten die tot deze categorie toe behoren.</p>

                @foreach($shop->services as $service)
                    <h3><a href="{{ route('auth.services.show', $service->id) }}">{{ $service->title }}</a></h3>
                    <p>{{ $service->description }}</p>
                @endforeach
            </div>
            {{-- Update counter for div clearance --}}
            <?php $i++; ?>
        @endif
    </div>
    {{--<component is="shop-owner-view">--}}
        {{--<autocomplete :suggestions="cities" :selection.sync="value"></autocomplete>--}}
        {{--</component>--}}

@stop

@section('javascript')
    <script src="{{ Request::root() }}/assets/plugins/owl-carousel/owl.carousel.js"></script>
    <script>
        $(".carrousel").owlCarousel({
            autoPlay: true,
            singleItem: true,
            pagination: true
        });
    </script>
@stop

