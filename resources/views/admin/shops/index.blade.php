@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>Winkels</h1>

        <table id="tableShops" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
            <tr>
                <td>ID</td>
                <td>{{  ucfirst(trans('shops/shop.name')) }}</td>
                <td>{{  ucfirst(trans('shops/shop.owner')) }}</td>
                <td>{{  ucfirst(trans('general.actions.edit')) }}</td>
                <td>{{  ucfirst(trans('general.actions.delete')) }}</td>
                <td>Afbeeldingen</td>
            </tr>
            </thead>
            <tbody>
            @foreach($shops as $shop)
                <tr>
                    <td>{{ $shop->id }}</td>
                    <td><a href="{{ route('admin.shops.show', $shop->id) }}">{{ $shop->title }}</a></td>
                    <td>
                        @if($shop->hasOwner() && $owner = $shop->getOwner())
                            <a href="{{ route('admin.users.show', $owner->id) }}">{{ $owner->fullName() }}</a>
                        @endif
                    </td>
                    <td><a href="{{ route('admin.shops.edit', $shop->id) }}"><i class="fa fa-pencil-square-o"></i></a></td>
                    <td>
                        {!! Form::open(array('route' => array('admin.shops.destroy', $shop->id), 'method' => 'DELETE')) !!}
                            <button class="btn btn-danger" data-confirm="Are you sure?">Verwijder <i class="fa fa-trash-o"></i></button>
                        {!! Form::close() !!}
                    </td>
                    <td>
                        <a href="{{route('admin.addPhotos',[$shop->id, 'shop'])}}" class="btn btn-warning">Voeg foto's toe aan winkel <i class="fa fa-plus" aria-hidden="true"></i></a>
                        <a href="{{route('admin.editPhotos',[$shop->id, 'shop'])}}" class="btn btn-warning">Pas foto's aan <i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

    </div>

@stop

@section('footer')

@stop
