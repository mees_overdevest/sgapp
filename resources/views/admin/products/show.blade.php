@extends('layouts.app')

@section('styles')
    <link rel="stylesheet" type="text/css" href="{{ Request::root() }}/assets/plugins/owl-carousel/owl.carousel.css">
    <link rel="stylesheet" type="text/css" href="{{ Request::root() }}/assets/plugins/owl-carousel/owl.theme.css">
@stop


@section('content')
    <div class="container">
        <h1>Product: {{ $product->title }}</h1>
        @if(auth()->check() && auth()->user()->hasRole('admin'))
            <p><a href='{{ route('admin.products.edit', [$product->id]) }}' class="btn btn-success">Update dit Product!</a></p>
        @endif

        <div class="col-sm-6 col-md-6 col-lg-6">
            <h3>{{ trans('products/product.description') }}</h3>
            <p>{{ $product->description }}</p>
        </div>

        @if($product->pictures)
            <div class="col-sm-6 col-md-6 col-lg-6">
                <div class="carrousel owl-carousel owl-theme">

                    @foreach($product->pictures as $image)
                        <div class="item"><img src="{{ \App\Helpers\ImageManager::getMobilePath($product->id, $image) }}" alt="{{ $image->image_name }}"></div>

                    @endforeach
                </div>
            </div>
        @endif


        @if($product->shops->count() > 0)
            <div class="col-sm-6 col-md-6 col-lg-6">
                <h3>Winkels</h3>
                <p>Alle winkels die dit product aanbieden</p>
                <table id="tableProducts" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <td>Titel</td>
                        <td>Beschrijving</td>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($product->shops as $shop)
                        <tr>
                            <td><a href="{{ route('admin.shops.show',$shop->id) }}">{{ $shop->name }} in {{ $shop->city }}</a></td>
                            <td>{{ $shop->description }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        @endif

        @if($product->brand->count() > 0)
            <div class="col-sm-6 col-md-6 col-lg-6">
                <h3>Merken</h3>
                <p>Alle merken die dit product aanbieden</p>
                <table id="tableProducts" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <td>Titel</td>
                        <td>Beschrijving</td>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($product->brand as $brand)
                        <tr>
                            <td><a href="{{ route('admin.brands.show',$brand->id) }}">{{ $brand->title }}</a></td>
                            <td>{{ $brand->description }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        @endif

        @if($product->categories->count() > 0)
            <div class="col-sm-6 col-md-6 col-lg-6">
                <h3>Categorieën</h3>
                <p>De categorieën die bij dit product horen</p>
                <table id="tableProducts" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <td>Titel</td>
                        <td>Beschrijving</td>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($product->categories as $category)
                        <tr>
                            <td><a href="{{ route('admin.PCategories.show',$category->id) }}">{{ $category->title }}</a></td>
                            <td>{{ $category->description }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        @endif

    </div>

@stop

@section('javascript')
    <script src="{{ Request::root() }}/assets/plugins/owl-carousel/owl.carousel.js"></script>
    <script>
        $("#carrousel").owlCarousel({
            autoPlay: true,
            singleItem: true,
            pagination: true
        });
    </script>
@stop
