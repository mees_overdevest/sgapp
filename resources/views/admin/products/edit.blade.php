@extends('layouts.app')

@section('content')
    <div class="container">

        <h1>{{ trans('products/product.update.submit') }}</h1>
        {!! Form::model($product, ['method' => 'PATCH', 'route' => ['admin.products.update', $product->id]]) !!}
        <div class="form-group">
            {!! Form::label('title', trans('products/product.name')) !!}
            {!! Form::text('title',  $product->title,['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('description', trans('products/product.description')) !!}
            {!! Form::text('description',$product->description ,['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            @foreach ($categories as $category)
                @foreach ($category as $key => $value)
                    {!! Form::Label('category_id', ucfirst($value)) !!}
                    @if($product->hasCategory($key))
                        {!! Form::checkbox('category_id[]', $key, true, ['class' => 'form-control', 'checked' => 'true']) !!}
                    @else
                        {!! Form::checkbox('category_id[]', $key, false, ['class' => 'form-control']) !!}
                    @endif
                @endforeach
            @endforeach
        </div>

        <div class="form-group">
            {!! Form::submit(trans('products/product.update.submit'), ['class' => 'btn btn-primary form-control']) !!}
        </div>
        {!! Form::close() !!}
    </div>
@stop

@section('footer')

@stop