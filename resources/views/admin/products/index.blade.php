@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>{{  ucfirst(trans('products/product.products')) }}</h1>

        <a href="{{ route('admin.products.create') }}" class="btn btn-success">{{  ucfirst(trans('products/product.create.new')) }}</a>

        <table id="tableProducts" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
            <tr>
                <td>ID</td>
                <td>{{  ucfirst(trans('products/product.product')) }}</td>
                <td>{{  ucfirst(trans('brands/brand.brand')) }}</td>
                <td>{{  ucfirst(trans('general.actions.edit')) }}</td>
                <td>{{  ucfirst(trans('general.actions.delete')) }}</td>
                <td>Afbeeldingen</td>
            </tr>
            </thead>
            <tbody>
            @foreach($products as $product)
                <tr>
                    <td>{{ $product->id }}</td>
                    <td><a href="{{ route('admin.products.show', $product->id) }}">{{ $product->title }}</a></td>
                    <td>
                        @if($product->getBrand())
                            <a href="{{ route('admin.brands.show', $product->getBrand()->id) }}">{{ $product->getBrand()->title }}</a>
                        @endif
                    </td>
                    <td><a href="{{ route('admin.products.edit', $product->id) }}"><i class="fa fa-pencil-square-o"></i></a></td>
                    <td>
                        {!! Form::open(array('route' => array('admin.products.destroy', $product->id), 'method' => 'DELETE')) !!}
                            <button class="btn btn-danger" data-confirm="Are you sure?">Verwijder <i class="fa fa-trash-o"></i></button>
                        {!! Form::close() !!}
                    </td>
                    <td>
                        <a href="{{route('admin.addPhotos',[$product->id, 'product'])}}" class="btn btn-warning">Voeg foto's toe aan product <i class="fa fa-plus" aria-hidden="true"></i></a>
                        <a href="{{route('admin.editPhotos',[$product->id, 'product'])}}" class="btn btn-warning">Pas foto's aan <i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@stop

@section('footer')

@stop
