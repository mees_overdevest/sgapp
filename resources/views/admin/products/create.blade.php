@extends('layouts.app')

@section('content')
    <div class="container">

        <h1>{{ trans('products/product.create.new') }}</h1>
        <hr/>

        {!! Form::open(['route' => 'admin.products.store']) !!}
        @include('admin.products._form', ['submitButtonText'=> trans('products/product.create.submit') ])
        {!! Form::close() !!}

    </div>

@stop
