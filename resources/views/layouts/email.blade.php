<!doctype html>
<html>
<head>
<meta name="viewport" content="width=device-width">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600" rel="stylesheet" type="text/css">
<title>4Staying Email Layout</title>
</head>

<body bgcolor="#f6f6f6" style="-webkit-font-smoothing: antialiased;height: 100%;-webkit-text-size-adjust: none;font-family: 'Open Sans', sans-serif;font-size: 100%;margin: 0;line-height: 1.6em;color: #7b7b7b;padding: 0;width: 100% !important; font-size: 14px !important;">

<!-- body -->
<table class="body-wrap" bgcolor="#f6f6f6" style="padding: 20px;width: 100%;">
	<tr>
		<td></td>
		<td  style="padding: 0 20px;border: 1px solid #f0f0f0;border-top: 0;background: #0fbad6;height: 7px;clear: both !important;display: block !important;margin: 0 auto !important;max-width: 600px !important;"></td>
		<td bgcolor="#FFFFFF" style="padding: 20px;border: 1px solid #f0f0f0;border-top: 0;clear: both !important;display: block !important;margin: 0 auto !important;max-width: 600px !important;">
			<table style="width: 100%;">
				 <tr>
					 <td>
						<div style="margin: 0 auto;max-width: 30%;">
							 <a target="_blank" href="{{ URL::to('/') }}" style="color: #ef7373;">
								  <img class="img-responsive" alt="logo" src="{{ URL::to('/') }}/assets/img/emails/logo.png" style="max-width: 600px;width: 100%;">
							 </a>
						</div>
						<br><br>
						@yield('content')
					</td>
				</tr>
			</table>	
			
		</td>
		<td></td>
	</tr>
</table>
<!-- /body -->

<!-- footer -->
<table class="footer-wrap" style="width: 100%;clear: both !important;">
	<tr>
		<td></td>
		<td class="container" style="clear: both !important;display: block !important;margin: 0 auto !important;max-width: 600px !important;">
			
			<!-- content -->
			<div class="content" style="display: block;margin: 0 auto;max-width: 600px;">
				<table style="width: 100%;">
					<tr>
						<td align="center">
							{{-- <p style="font-size: 12px;font-weight: normal;margin-bottom: 10px;color: #666666;">Don't like these annoying emails? <a href="#" style="color: #999999;"><unsubscribe>Unsubscribe</unsubscribe></a>.
							</p> --}}
						</td>
					</tr>
				</table>
			</div>
			<!-- /content -->
			
		</td>
		<td></td>
	</tr>
</table>
<!-- /footer -->

</body>
</html>
