<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="_token" content="{{ csrf_token() }}" id="token">

    <title>ShoppingGuide {{ $title or "" }}</title>

    <!-- Fonts -->

    <!-- Styles -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

    {{--<link rel="stylesheet" href="{{ Request::root() }}/assets/css/template.css">--}}

    <link rel="stylesheet" href="{{ Request::root() }}/css/app.css">
    <link rel="stylesheet" href="/assets/plugins/editable/css/bootstrap-editable.css" >
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">
    @yield('styles')
    {{-- <link href="{{ elixir('css/temp.css') }}" rel="stylesheet"> --}}
    {{-- <link rel="stylesheet" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css"> --}}
</head>
<body id="app-layout">
<div id="site-wrapper">

    <div id="site-canvas">

@include('includes.nav')
@include('includes.alerts')



    @yield('content')


        <!-- #site-canvas -->
    </div>
    <!-- #site-wrapper> -->
</div>
        <!-- JavaScripts -->
<script>
    var BASE = "{{ Request::root() }}";
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js" integrity="sha384-I6F5OKECLVtK/BL+8iSLDEHowSAfUo76ZL9+kGAgTRdiByINKJaqTPH/QVNS1VDb" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
<script src="/assets/plugins/editable/js/bootstrap-editable.min.js"></script>
<script src="/assets/js/nav.js"></script>
<script src="/assets/js/site.js"></script>
<!-- <script src="{{ url('/js/bundle.js') }}"></script -->
@yield('javascript')
<script src="/assets/js/form.js"></script>

</body>
</html>
