@extends('layouts.app')

@section('content')

   {!! Form::open(['route' => ['auth.activate.account', $code, $mail]]) !!}

    <div class="form-group">
        {!! Form::label('password', ucfirst(trans('admin/users.form.password'))) !!}
        {!! Form::password('password', '', ['class'=>'form-control']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('password_confirmation', ucfirst(trans('admin/users.form.passwordRepeat'))) !!}
        {!! Form::password('password_confirmation', '', ['class'=>'form-control']) !!}
    </div>

    {!! Form::submit(trans('general.actions.save')) !!}

    {!! Form::close() !!}

@endsection