@if(Auth::check() && auth()->user()->hasRole('user'))
    @if(!$object->isFavourite())
        <a class="btn btn-primary" href="{{ route('auth.'. $routeName .'.favourite', $object->id) }}">Maak favoriet</a>
    @else
        <a class="btn btn-primary" href="{{ route('auth.'. $routeName .'.unFavourite', $object->id) }}">Verwijder favoriet</a>
    @endif
@endif