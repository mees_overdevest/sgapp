@include('includes.navs.main')

<nav class="navbar navbar-default navbar-static-top">
    <div class="container">
        <div class="navbar-header">

            <!-- Collapsed Hamburger -->
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <!-- Branding Image -->
            <a class="navbar-brand toggle-nav" href="#"> ShoppingGuide </a>
        </div>

        <div class="collapse navbar-collapse" id="app-navbar-collapse">
            <!-- Left Side Of Navbar -->
            <ul class="nav navbar-nav">

                        <!-- Client -->
                @if(Auth::check())
                    <li class="dropdown">
                        <a href="#" class="toggle-nav">Menu</a>
                    </li>
                    @if(auth()->user()->hasRole('admin'))
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{{ ucfirst(trans('admin/users.nav.title')) }} <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="{{ route('admin.users.index') }}">{{ ucfirst(trans('admin/users.nav.title')) }}</a></li>
                                <li><a href="{{ route('admin.users.create') }}">{{ ucfirst(trans('admin/users.nav.create')) }}</a></li>
                            </ul>
                        </li>
                    @endif
                @endif

                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Zoeken <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        {!! Form::open(['route' => 'search']) !!}

                        <div class="form-group">
                            {!! Form::label('term', 'Uw zoekterm') !!}
                            {!! Form::text('term', '', ['class'=>'form-control']) !!}
                        </div>

                        {!! Form::submit("Zoek", ['class'=>'btn btn-info']) !!}
                        {!! Form::close() !!}
                    </ul>
                </li>

                <li><a href="{{ route('wizards.intro', ['step' => 0]) }}">Intro</a></li>
            </ul>



            <!-- Right Side Of Navbar -->
            <ul class="nav navbar-nav navbar-right">
                <!-- Authentication Links -->
                @if (Auth::guest())
                    <li><a href="{{ url('/login') }}">Login</a></li>
                    <li><a href="{{ url('/register') }}">Register</a></li>
                    <li><a href="{{ url('/login/facebook') }}">Login with Facebook</a></li>
                @else
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            {{ auth()->user()->fullname() }} <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu" role="menu">
                            @if(auth()->user()->facebook_id == null)
                            <li>
                                <a href="{{ route('user.resetpassword') }}">
                                    <i class="fa fa-envelope-o"></i> {{ trans('general.actions.resetPassword') }}
                                </a>
                            </li>
                            @endif
                            <li>
                                <a href="{{ route('auth.me') }}">
                                    <i class="fa fa-envelope-o"></i> {{ trans('account.actions.edit') }}
                                </a>
                            </li>
                            <hr/>
                            <li>
                                <a href="{{ url('/logout') }}">
                                    <i class="fa fa-btn fa-sign-out"></i>Logout
                                </a>
                            </li>
                        </ul>
                    </li>
                @endif
            </ul>
        </div>
    </div>
</nav>