<div class="homecarrousel owl-carousel owl-theme">

    @foreach($objects as $object)
        @if($object->pictures)
        <div class="item">
            <div class="textoverlay">
                <div class="col-md-6 col-sm-6 col-xs-6">
                    <h3>{{ $object->title }}</h3>
                    <p>{{ $object->description }}</p>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-6">
                    {{--<h4>Opties</h4>--}}
                    {{----}}
                    {{--</a>--}}
                    @include('includes.buttons.favourite', ['object' => $object, 'routeName' => $favouriteRoute])
                </div>
            </div>
            <div class="carrousel" class="owl-carousel owl-theme">
                @foreach($object->pictures as $image)
                    <div class="item">
                        <img src="{{ \App\Helpers\ImageManager::getMobilePath($object->id, $image) }}" alt="{{ $image->image_name }}">
                    </div>
                @endforeach
            </div>
        </div>
        @endif
    @endforeach
</div>