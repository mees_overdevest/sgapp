<div id="site-menu9">
    <a href="#" class="toggle-nav9"><i class="fa fa-times" aria-hidden="true"></i></a>

    @if(auth()->check() && auth()->user()->hasRole('admin'))
        <a href="{{ route('admin.logs.index') }}"><h2>Gebruikerslogs</h2></a>
        <p class="lead">Hieronder staan de opties, voor het bekijken van gebruikerslogs.</p>

        <div class="list-group">
            <a href="{{ route('admin.logs.index') }}" class="list-group-item">Gebruikerslogs</a>
        </div>

    @endif

</div>