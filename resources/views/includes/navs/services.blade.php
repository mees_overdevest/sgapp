<div id="site-menu8">
    <a href="#" class="toggle-nav8"><i class="fa fa-times" aria-hidden="true"></i></a>

    @if(auth()->check() && auth()->user()->hasRole('admin'))
    <a href="{{ route('admin.services.index') }}"><h2>{{  ucfirst(trans('services/service.services')) }}</h2></a>
    <p class="lead">Hieronder staan de opties, voor het bekijken en aanpassen van winkelgebieden.</p>

    <div class="list-group">
        <a href="{{ route('admin.services.index') }}" class="list-group-item">{{  ucfirst(trans('services/service.services')) }}</a>
        <a href="{{ route('admin.services.create') }}" class="list-group-item">{{  ucfirst(trans('services/service.create.new')) }}</a>
    </div>
    @elseif(auth()->check())
        <a href="{{ route('auth.services.index') }}"><h2>{{  ucfirst(trans('services/service.services')) }}</h2></a>
        <div class="list-group">
            <a href="{{ route('auth.services.index') }}" class="list-group-item">{{  ucfirst(trans('services/service.services')) }}</a>
        </div>
    @else

    @endif

</div>