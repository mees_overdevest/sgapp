<div id="site-menu7">
    <a href="#" class="toggle-nav7"><i class="fa fa-times" aria-hidden="true"></i></a>

    @if(auth()->check() && auth()->user()->hasRole('admin'))
    <a href="{{ route('admin.products.index') }}"><h2>{{  ucfirst(trans('products/product.products')) }}</h2></a>
    <p class="lead">Hieronder staan de opties, voor het bekijken en aanpassen van winkelgebieden.</p>

    <div class="list-group">
        <a href="{{ route('admin.products.index') }}" class="list-group-item">{{  ucfirst(trans('products/product.products')) }}</a>
        <a href="{{ route('admin.products.create') }}" class="list-group-item">{{  ucfirst(trans('products/product.create.new')) }}</a>
    </div>
    @elseif(auth()->check())
        <a href="{{ route('auth.products.index') }}"><h2>{{  ucfirst(trans('products/product.products')) }}</h2></a>
        <div class="list-group">
            <a href="{{ route('auth.products.index') }}" class="list-group-item">{{  ucfirst(trans('products/product.products')) }}</a>
        </div>
    @else

    @endif

</div>