<div id="site-menu">
    <a href="#" class="toggle-nav"><i class="fa fa-times" aria-hidden="true"></i></a>
    <h2>Menu</h2>
    <p class="lead">Doorzoek hieronder onze productcategorieën!</p>

    {{--@if(auth()->check() && auth()->user()->hasRole('admin'))--}}
        {{-- Admin extra Auth --}}
    {{--@else--}}
        <div class="list-group">
            @if(auth()->check() && auth()->user()->hasRole('user'))
                <a href="#" class="list-group-item toggle-nav10">Mijn account</a>
            @endif
            <a href="#" class="list-group-item toggle-nav1">{{ ucfirst(trans('nav.shops.title')) }}</a>
            <a href="#" class="list-group-item toggle-nav2">{{ ucfirst(trans('nav.brands.title')) }}</a>
            <a href="#" class="list-group-item toggle-nav3">{{ ucfirst(trans('nav.areas.title')) }}</a>
            <a href="#" class="list-group-item toggle-nav4">{{ ucfirst(trans('nav.PCategories.title')) }}</a>
            <a href="#" class="list-group-item toggle-nav5">{{ ucfirst(trans('nav.shopCategories.title')) }}</a>
            @if(auth()->check() && auth()->user()->hasRole('admin'))
                <a href="#" class="list-group-item toggle-nav6">{{ ucfirst(trans('nav.users.title')) }}</a>
                <a href="#" class="list-group-item toggle-nav9">Gebruikerslogs</a>
            @endif
            <a href="#" class="list-group-item toggle-nav7">{{ ucfirst(trans('nav.products.title')) }}</a>
            <a href="#" class="list-group-item toggle-nav8">{{ ucfirst(trans('nav.services.title')) }}</a>
        </div>
    {{--@endif--}}

</div>


    @include('includes.navs.shops')
    @include('includes.navs.areas')
    @include('includes.navs.users')
    @include('includes.navs.brands')
    @include('includes.navs.PCategories')
    @include('includes.navs.shopCategories')
    @include('includes.navs.products')
    @include('includes.navs.services')
    @include('includes.navs.logs')
    @include('includes.navs.account')

