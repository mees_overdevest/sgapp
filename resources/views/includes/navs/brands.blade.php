<div id="site-menu2">
    <a href="#" class="toggle-nav2"><i class="fa fa-times" aria-hidden="true"></i></a>

    @if(auth()->check() && auth()->user()->hasRole('admin'))
    <a href="{{ route('admin.brands.index') }}"><h2>{{  ucfirst(trans('brands/brand.brands')) }}</h2></a>
    <p class="lead">Hieronder staan de opties, voor het bekijken en aanpassen van winkelgebieden.</p>

    <div class="list-group">
        <a href="{{ route('admin.brands.index') }}" class="list-group-item">{{  ucfirst(trans('brands/brand.brands')) }}</a>
        <a href="{{ route('admin.brands.create') }}" class="list-group-item">{{  ucfirst(trans('brands/brand.create.new')) }}</a>
    </div>
    @elseif(auth()->check())
        <a href="{{ route('auth.brands.index') }}"><h2>{{  ucfirst(trans('brands/brand.brands')) }}</h2></a>
        <div class="list-group">
            <a href="{{ route('auth.brands.index') }}" class="list-group-item">{{  ucfirst(trans('brands/brand.brands')) }}</a>
        </div>
    @else

    @endif

</div>