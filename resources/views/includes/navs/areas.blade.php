<div id="site-menu3">
    <a href="#" class="toggle-nav3"><i class="fa fa-times" aria-hidden="true"></i></a>


    @if(auth()->check() && auth()->user()->hasRole('admin'))
    <a href="{{ route('admin.areas.index') }}"><h2>Winkelgebieden</h2></a>
    <p class="lead">Hieronder staan de opties, voor het bekijken en aanpassen van winkelgebieden.</p>

    <div class="list-group">
        <a href="{{ route('admin.areas.index') }}" class="list-group-item">Alle winkelgebieden</a>
        <a href="{{ route('admin.areas.create') }}" class="list-group-item">Nieuwe winkelgebied</a>
    </div>
    @elseif(auth()->check())
        <a href="{{ route('auth.areas.index') }}"><h2>Winkelgebieden</h2></a>
        <p class="lead">Hieronder staan de opties, voor het bekijken van winkelgebieden.</p>

        <div class="list-group">
            <a href="{{ route('auth.areas.index') }}" class="list-group-item">Alle winkelgebieden</a>
        </div>
    @else

    @endif

</div>