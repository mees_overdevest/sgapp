<div id="site-menu1">
    <a href="#" class="toggle-nav1"><i class="fa fa-times" aria-hidden="true"></i></a>

    @if(auth()->check() && auth()->user()->hasRole('admin'))
    <a href="{{ route('admin.shops.index') }}"><h2>Winkels</h2></a>
    <p class="lead">Hieronder staan de opties, voor het bekijken en aanpassen van winkels.</p>
    <p>Zij vormen de leden van ShoppingGuide.</p>

    <div class="list-group">
        <a href="{{ route('admin.shops.index') }}" class="list-group-item">Alle winkels</a>
        <a href="{{ route('admin.shops.create') }}" class="list-group-item">Nieuwe winkel</a>
    </div>
    @elseif(auth()->check() && auth()->user()->shops())
        <a href="{{ route('auth.shops.index') }}"><h2>Winkels</h2></a>
        <div class="list-group">
            <a href="{{ route('auth.me.shop') }}" class="list-group-item">Pas mijn winkel aan</a>
            <a href="{{ route('auth.me.shop.addPhotos') }}" class="list-group-item">Voeg foto's toe aan mijn winkel</a>
        </div>
    @else

    @endif

</div>