<div id="site-menu5">
    <a href="#" class="toggle-nav5"><i class="fa fa-times" aria-hidden="true"></i></a>

    @if(auth()->check() && auth()->user()->hasRole('admin'))
    <a href="{{ route('admin.shopCategory.index') }}"><h2>{{  ucfirst(trans('shops/category.categories')) }}</h2></a>
    <p class="lead">Hieronder staan de opties, voor het bekijken en aanpassen van winkelgebieden.</p>

    <div class="list-group">
        <a href="{{ route('admin.shopCategory.index') }}" class="list-group-item">{{  ucfirst(trans('shops/category.categories')) }}</a>
        <a href="{{ route('admin.shopCategory.create') }}" class="list-group-item">{{  ucfirst(trans('shops/category.actions.create')) }}</a>
    </div>
    @elseif(auth()->check())
        <a href="{{ route('auth.shopCategory.index') }}"><h2>{{  ucfirst(trans('shops/category.categories')) }}</h2></a>
        <div class="list-group">
            <a href="{{ route('auth.shopCategory.index') }}" class="list-group-item">{{  ucfirst(trans('shops/category.categories')) }}</a>
        </div>
    @else

    @endif

</div>