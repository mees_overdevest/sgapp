<div id="site-menu6">
    <a href="#" class="toggle-nav6"><i class="fa fa-times" aria-hidden="true"></i></a>

    @if(auth()->check() && auth()->user()->hasRole('admin'))
    <a href="{{ route('admin.users.index') }}"><h2>{{ ucfirst(trans('nav.users.title')) }}</h2></a>
    <p class="lead">Hieronder staan de opties, voor het bekijken en aanpassen van gebruikers.</p>

    <div class="list-group">
        <a href="{{ route('admin.users.create') }}" class="list-group-item">Nieuwe gebruiker</a>
    </div>
    @endif

</div>