<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShopsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shops', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('description');
            $table->string('since')->nullable();
            $table->string('city');
            $table->string('address');
            $table->string('places_id');
            $table->timestamps();
        });

        Schema::create('shop_product', function(Blueprint $table){
            $table->integer('shop_id')->unsigned();
            $table->integer('product_id')->unsigned();

            $table->foreign('shop_id')
                ->references('id')
                ->on('shops')
                ->onDelete('cascade');

            $table->foreign('product_id')
                ->references('id')
                ->on('products')
                ->onDelete('cascade');

            $table->primary(['product_id','shop_id']);
        });

        Schema::create('shop_service', function(Blueprint $table){
            $table->integer('shop_id')->unsigned();
            $table->integer('service_id')->unsigned();

            $table->foreign('shop_id')
                ->references('id')
                ->on('shops')
                ->onDelete('cascade');

            $table->foreign('service_id')
                ->references('id')
                ->on('services')
                ->onDelete('cascade');

            $table->primary(['service_id','shop_id']);
        });

        Schema::create('shop_owner', function(Blueprint $table){
            $table->integer('shop_id')->unsigned();
            $table->integer('user_id')->unsigned();

            $table->foreign('shop_id')
                ->references('id')
                ->on('shops')
                ->onDelete('cascade');

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->primary(['user_id','shop_id']);
        });

        Schema::create('shop_user', function(Blueprint $table){
            $table->integer('shop_id')->unsigned();
            $table->integer('user_id')->unsigned();

            $table->foreign('shop_id')
                ->references('id')
                ->on('shops')
                ->onDelete('cascade');

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->primary(['user_id','shop_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('shops');
        Schema::drop('shop_product');
        Schema::drop('shop_service');
        Schema::drop('shop_owner');
        Schema::drop('shop_user');
    }
}
