<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShopCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shopCategories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->text('description');
            $table->integer('parent_id')->nullable();
            $table->timestamps();
        });

        Schema::create('shopCategory_shop', function(Blueprint $table){
            $table->integer('shop_id')->unsigned();
            $table->integer('shopCategory_id')->unsigned();

            $table->foreign('shop_id')
                ->references('id')
                ->on('shops')
                ->onDelete('cascade');

            $table->foreign('shopCategory_id')
                ->references('id')
                ->on('shopCategories')
                ->onDelete('cascade');

            $table->primary(['shopCategory_id','shop_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('shopCategories');
        Schema::drop('shopCategory_shop');
    }
}
