<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePicturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pictures', function (Blueprint $table) {
            $table->increments('id');
            $table->string('image_name');
            $table->string('type');
            $table->string('image_extension', 10);
            $table->string('description');
            $table->timestamps();
        });

        Schema::create('picture_brand', function (Blueprint $table) {
            $table->integer('picture_id')->unsigned();
            $table->integer('brand_id')->unsigned();

            $table->foreign('brand_id')
                ->references('id')
                ->on('brands')
                ->onDelete('cascade');

            $table->foreign('picture_id')
                ->references('id')
                ->on('pictures')
                ->onDelete('cascade');

            $table->primary(['picture_id','brand_id']);
        });

        Schema::create('picture_shop', function (Blueprint $table) {
            $table->integer('picture_id')->unsigned();
            $table->integer('shop_id')->unsigned();

            $table->foreign('shop_id')
                ->references('id')
                ->on('shops')
                ->onDelete('cascade');

            $table->foreign('picture_id')
                ->references('id')
                ->on('pictures')
                ->onDelete('cascade');

            $table->primary(['picture_id','shop_id']);
        });

        Schema::create('picture_product', function (Blueprint $table) {
            $table->integer('picture_id')->unsigned();
            $table->integer('product_id')->unsigned();

            $table->foreign('product_id')
                ->references('id')
                ->on('products')
                ->onDelete('cascade');

            $table->foreign('picture_id')
                ->references('id')
                ->on('pictures')
                ->onDelete('cascade');

            $table->primary(['picture_id','product_id']);
        });

        Schema::create('picture_service', function (Blueprint $table) {
            $table->integer('picture_id')->unsigned();
            $table->integer('service_id')->unsigned();

            $table->foreign('service_id')
                ->references('id')
                ->on('services')
                ->onDelete('cascade');

            $table->foreign('picture_id')
                ->references('id')
                ->on('pictures')
                ->onDelete('cascade');

            $table->primary(['picture_id','service_id']);
        });

        Schema::create('picture_area', function (Blueprint $table) {
            $table->integer('picture_id')->unsigned();
            $table->integer('area_id')->unsigned();

            $table->foreign('area_id')
                ->references('id')
                ->on('areas')
                ->onDelete('cascade');

            $table->foreign('picture_id')
                ->references('id')
                ->on('pictures')
                ->onDelete('cascade');

            $table->primary(['picture_id','area_id']);
        });

        Schema::create('picture_user', function (Blueprint $table) {
            $table->integer('picture_id')->unsigned();
            $table->integer('user_id')->unsigned();

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->foreign('picture_id')
                ->references('id')
                ->on('pictures')
                ->onDelete('cascade');

            $table->primary(['picture_id','user_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pictures');
        Schema::drop('picture_brand');
        Schema::drop('picture_shop');
        Schema::drop('picture_product');
        Schema::drop('picture_service');
        Schema::drop('picture_area');
        Schema::drop('picture_user');
    }
}
