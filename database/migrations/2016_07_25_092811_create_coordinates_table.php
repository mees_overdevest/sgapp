<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoordinatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coordinates', function(Blueprint $table){
            $table->increments('id');
            $table->double('lat', 8, 5);
            $table->double('lng', 8, 5);
            $table->timestamps();
        });

        Schema::create('area_coordinates', function(Blueprint $table){
            $table->integer('coordinate_id')->unsigned();
            $table->integer('area_id')->unsigned();
            $table->integer('order');

            $table->foreign('coordinate_id')
                ->references('id')
                ->on('coordinates')
                ->onDelete('cascade');

            $table->foreign('area_id')
                ->references('id')
                ->on('areas')
                ->onDelete('cascade');

            $table->primary(['area_id','coordinate_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('coordinates');
        Schema::drop('area_coordinates');
    }
}
