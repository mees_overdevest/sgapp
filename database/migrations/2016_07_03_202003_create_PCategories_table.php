<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('PCategories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->text('description');
            $table->integer('parent_id')->nullable();
            $table->timestamps();
        });

        Schema::create('PCategory_product', function(Blueprint $table){
            $table->integer('product_id')->unsigned();
            $table->integer('PCategory_id')->unsigned();

            $table->foreign('product_id')
                ->references('id')
                ->on('products')
                ->onDelete('cascade');

            $table->foreign('PCategory_id')
                ->references('id')
                ->on('PCategories')
                ->onDelete('cascade');

            $table->primary(['PCategory_id','product_id']);
        });

        Schema::create('PCategory_service', function(Blueprint $table){
            $table->integer('service_id')->unsigned();
            $table->integer('PCategory_id')->unsigned();

            $table->foreign('service_id')
                ->references('id')
                ->on('services')
                ->onDelete('cascade');

            $table->foreign('PCategory_id')
                ->references('id')
                ->on('PCategories')
                ->onDelete('cascade');

            $table->primary(['PCategory_id','service_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('PCategories');
        Schema::drop('PCategory_product');
        Schema::drop('PCategory_service');
    }
}
