<?php
/**
 * Created by PhpStorm.
 * User: mees
 * Date: 16-10-16
 * Time: 17:40
 */

return [
    'log_types' => [
        'A0' => 'frontpages',
    ],
    'types'     => [
        'brand'         =>  'brand',
        'product'       =>  'product',
        'service'       =>  'service',
        'area'          =>  'area',
        'shop'          =>  'shop',
        'shopCategory'  =>  'shopCategory',
        'PCategory'     =>  'PCategory',
    ],
    'auth' => [
        'no-reply' => [
            'name' => 'No-reply Shopping Guide',
            'mail' => 'no-reply@shopping-guide.nl'
        ]
    ]
];