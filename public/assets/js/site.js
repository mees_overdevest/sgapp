/**
 * Created by mees on 13-07-16.
 */
var site = {
    post: function(url, type, data, handleData)
    {
        $.ajax({
            url: url,
            type: "POST",
            dataType: type,
            data: data,
            success: function(data)
            {
                handleData(data);
            },
            error: function(jqXhr)
            {
                handleData(jqXhr);
            }
        })
    },
    get: function(url, type, handleData)
    {

        type = type || "json";

        $.ajax({
            url: url,
            type: "GET",
            dataType: type,
            success: function(data)
            {
                handleData(data);
            },
            fail: function()
            {
                console.log('Something failed');
            }
        });
    },
    create: function(type, html, classes, href)
    {
        html = html || '';
        classes = classes || '';
        href = href || '';

        var create = document.createElement(type);
        create.innerHTML = html;
        create.className = classes;
        create.href = href;

        return create;

    }
};