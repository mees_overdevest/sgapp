/**
 * Created by mees on 17-07-16.
 */
/*====================================
 =            ON DOM READY            =
 ====================================*/

//var menuItem = '';



$(function() {
    $('.toggle-nav').click(function() {
        // Calling a function in case you want to expand upon this.
        //console.log(event.target.classList);
        //this.menuItem = event.target.classList[0];
        //console.log(this.menuItem);
        //[this.menuItem]();
        //window[menuItem]();
        toggleNav();
    });
    $('.toggle-nav1').click(function() {
        // Calling a function in case you want to expand upon this.
        toggleNav1();
    });
    $('.toggle-nav2').click(function() {
        // Calling a function in case you want to expand upon this.
        toggleNav2();
    });

    $('.toggle-nav3').click(function() {
        // Calling a function in case you want to expand upon this.
        toggleNav3();
    });

    $('.toggle-nav4').click(function() {
        // Calling a function in case you want to expand upon this.
        toggleNav4();
    });

    $('.toggle-nav5').click(function() {
        // Calling a function in case you want to expand upon this.
        toggleNav5();
    });

    $('.toggle-nav6').click(function() {
        // Calling a function in case you want to expand upon this.
        toggleNav6();
    });

    $('.toggle-nav7').click(function() {
        // Calling a function in case you want to expand upon this.
        toggleNav7();
    });

    $('.toggle-nav8').click(function() {
        // Calling a function in case you want to expand upon this.
        toggleNav8();
    });

    $('.toggle-nav9').click(function() {
        // Calling a function in case you want to expand upon this.
        toggleNav9();
    });

    $('.toggle-nav10').click(function() {
        // Calling a function in case you want to expand upon this.
        toggleNav10();
    });
});


/*========================================
 =            CUSTOM FUNCTIONS            =
 ========================================*/
function toggleNav() {
    // if ($('#site-wrapper[class=""]')) {
    if ($('#site-wrapper').hasClass('show-nav')) {

        var regexp = /show-nav/gi;
        var clList = document.getElementById('site-wrapper').classList;
        console.log(clList);

        var classes = ['show-nav','show-nav1','show-nav2', 'show-nav3', 'show-nav4', 'show-nav5', 'show-nav6','show-nav7', 'show-nav8', 'show-nav9'];
        var menus = ['#site-menu', '#site-menu1','#site-menu2', '#site-menu3', '#site-menu4', '#site-menu5', '#site-menu6', '#site-menu7', '#site-menu8', '#site-menu9'];

        for(var i = 0, j = classes.length; i < j; i++) {
            if($('#site-wrapper').hasClass(classes[i])) {
                console.log(classes[i]);
                $(menus[i]).removeClass('visible');
                $('#site-wrapper').removeClass(classes[i]);
                // break;
            }
        }

        // Do things on Nav Close
        // $('#site-wrapper').removeClass('show-nav');

        // if($('#site-wrapper'))

        // if($('#site-wrapper').hasClass('show-nav2')){
        // 	$('#site-wrapper').removeClass('show-nav2');
        // }
    } else {
        // Do things on Nav Open
        $('#site-wrapper').addClass('show-nav');
    }
}
function toggleNav1() {
    if ($('#site-wrapper').hasClass('show-nav1')) {
        // Do things on Nav Close
        $('#site-menu1').removeClass('visible');
        $('#site-wrapper').removeClass('show-nav1');
    } else {
        // Do things on Nav Open
        if ($('#site-wrapper').hasClass('show-nav')) {
        }else{
            $('#site-wrapper').addClass('show-nav');
        }


        $('#site-wrapper').addClass('show-nav1');
        $('#site-menu1').addClass('visible');
    }

//$('#site-wrapper').toggleClass('show-nav');
}

function toggleNav2() {
    if ($('#site-wrapper').hasClass('show-nav2')) {
        // Do things on Nav Close
        $('#site-menu2').removeClass('visible');
        $('#site-wrapper').removeClass('show-nav2');
    } else {
        // Do things on Nav Open
        if ($('#site-wrapper').hasClass('show-nav')) {
        }else{
            $('#site-wrapper').addClass('show-nav');
        }


        $('#site-wrapper').addClass('show-nav2');
        $('#site-menu2').addClass('visible');
    }

//$('#site-wrapper').toggleClass('show-nav');
}

function toggleNav3() {
    if ($('#site-wrapper').hasClass('show-nav3')) {
        // Do things on Nav Close
        $('#site-menu3').removeClass('visible');
        $('#site-wrapper').removeClass('show-nav3');
    } else {
        // Do things on Nav Open
        if ($('#site-wrapper').hasClass('show-nav')) {
        }else{
            $('#site-wrapper').addClass('show-nav');
        }
        $('#site-wrapper').addClass('show-nav3');
        $('#site-menu3').addClass('visible');
    }

//$('#site-wrapper').toggleClass('show-nav');
}

function toggleNav4() {
    if ($('#site-wrapper').hasClass('show-nav4')) {
        // Do things on Nav Close
        $('#site-menu4').removeClass('visible');
        $('#site-wrapper').removeClass('show-nav4');
    } else {
        // Do things on Nav Open
        if ($('#site-wrapper').hasClass('show-nav')) {
        }else{
            $('#site-wrapper').addClass('show-nav');
        }
        $('#site-wrapper').addClass('show-nav4');
        $('#site-menu4').addClass('visible');
    }

//$('#site-wrapper').toggleClass('show-nav');
}

function toggleNav5() {
    if ($('#site-wrapper').hasClass('show-nav5')) {
        // Do things on Nav Close
        $('#site-menu5').removeClass('visible');
        $('#site-wrapper').removeClass('show-nav5');
    } else {
        // Do things on Nav Open
        if ($('#site-wrapper').hasClass('show-nav')) {
        }else{
            $('#site-wrapper').addClass('show-nav');
        }
        $('#site-wrapper').addClass('show-nav5');
        $('#site-menu5').addClass('visible');
    }

//$('#site-wrapper').toggleClass('show-nav');
}

function toggleNav6() {
    if ($('#site-wrapper').hasClass('show-nav6')) {
        // Do things on Nav Close
        $('#site-menu6').removeClass('visible');
        $('#site-wrapper').removeClass('show-nav6');
    } else {
        // Do things on Nav Open
        if ($('#site-wrapper').hasClass('show-nav')) {
        }else{
            $('#site-wrapper').addClass('show-nav');
        }
        $('#site-wrapper').addClass('show-nav6');
        $('#site-menu6').addClass('visible');
    }

//$('#site-wrapper').toggleClass('show-nav');
}

function toggleNav7() {
    if ($('#site-wrapper').hasClass('show-nav7')) {
        // Do things on Nav Close
        $('#site-menu7').removeClass('visible');
        $('#site-wrapper').removeClass('show-nav7');
    } else {
        // Do things on Nav Open
        if ($('#site-wrapper').hasClass('show-nav')) {
        }else{
            $('#site-wrapper').addClass('show-nav');
        }
        $('#site-wrapper').addClass('show-nav7');
        $('#site-menu7').addClass('visible');
    }

//$('#site-wrapper').toggleClass('show-nav');
}

function toggleNav8() {
    if ($('#site-wrapper').hasClass('show-nav8')) {
        // Do things on Nav Close
        $('#site-menu8').removeClass('visible');
        $('#site-wrapper').removeClass('show-nav8');
    } else {
        // Do things on Nav Open
        if ($('#site-wrapper').hasClass('show-nav')) {
        }else{
            $('#site-wrapper').addClass('show-nav');
        }
        $('#site-wrapper').addClass('show-nav8');
        $('#site-menu8').addClass('visible');
    }

//$('#site-wrapper').toggleClass('show-nav');
}


function toggleNav9() {
    if ($('#site-wrapper').hasClass('show-nav9')) {
        // Do things on Nav Close
        $('#site-menu9').removeClass('visible');
        $('#site-wrapper').removeClass('show-nav9');
    } else {
        // Do things on Nav Open
        if ($('#site-wrapper').hasClass('show-nav')) {
        }else{
            $('#site-wrapper').addClass('show-nav');
        }
        $('#site-wrapper').addClass('show-nav9');
        $('#site-menu9').addClass('visible');
    }

//$('#site-wrapper').toggleClass('show-nav');
}

function toggleNav10() {
    if ($('#site-wrapper').hasClass('show-nav10')) {
        // Do things on Nav Close
        $('#site-menu10').removeClass('visible');
        $('#site-wrapper').removeClass('show-nav10');
    } else {
        // Do things on Nav Open
        if ($('#site-wrapper').hasClass('show-nav')) {
        }else{
            $('#site-wrapper').addClass('show-nav');
        }
        $('#site-wrapper').addClass('show-nav10');
        $('#site-menu10').addClass('visible');
    }

//$('#site-wrapper').toggleClass('show-nav');
}