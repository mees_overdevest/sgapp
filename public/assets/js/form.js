$(function() {
	submit();
});

var submit = function() {
	$(".ajaxForm").on('submit', function(e) {
		e.preventDefault();
		var route = $(this).attr('action');
		var method = $(this).attr('method');
		var data = $(this).serialize();

		site.post(route, 'json', data, function(output) {
			if(output.status == 422) error(output, this);
			else {
				console.log(typeof customSuccess === "function");
				(typeof customSuccess === "function") ? customSuccess(output, this) : success(output, this);
			}
		}.bind(this));
	});
}

var createErrors = function(errors) {
	$(".errors").remove();
	var div = document.createElement('div');
	div.className = "errors alert alert-danger";
	$.each(errors, function(key, index) {
		div.innerHTML += index + "<br>";
	});
	return div;
}

var createSuccess = function(message) {
	$(".success").remove();
	var div = document.createElement('div');
	div.className = "success alert alert-success";
	div.innerHTML = message;
	return div;
}

var error = function(output, form) {
	var errorDiv = createErrors(output.responseJSON);
	$(form).prepend(errorDiv);
}

var success = function(output, form) {
	console.log(output);
	var successDiv = createSuccess(output.Success);
	$(form).prepend(successDiv);
}